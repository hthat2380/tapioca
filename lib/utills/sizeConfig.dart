import 'package:get/get.dart';

class SizeConfig {
  static double width = Get.width/100;
  static double height = Get.height/100;
}