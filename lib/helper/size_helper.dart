import 'package:flutter/cupertino.dart';

class SizeConfig {
  static MediaQueryData? _mediaQueryData = MediaQueryData();
  static double screenWidth = 0.0;
  static double screenHeight = 0.0;
  static double blockSizeHorizontal = 0.0;
  static double blockSizeVertical = 0.0;

  static double _safeAreaHorizontal = 0.0;
  static double _safeAreaVertical = 0.0;
  static double safeBlockHorizontal = 0.0;
  static double safeBlockVertical = 0.0;

  static double size22 = SizeConfig.blockSizeHorizontal * 8;
  static double size20 = SizeConfig.blockSizeHorizontal * 6;

  static double size18 = SizeConfig.blockSizeHorizontal * 5;
  static double size10 = SizeConfig.blockSizeHorizontal * 2.3;
  static double size5 = SizeConfig.blockSizeHorizontal * 2;
  static double size2 = SizeConfig.blockSizeHorizontal * 2;

  static double size12 = SizeConfig.blockSizeHorizontal * 3;

  static double size15 = SizeConfig.blockSizeHorizontal * 4;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData!.size.width;
    screenHeight = _mediaQueryData!.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal =
        _mediaQueryData!.padding.left + _mediaQueryData!.padding.right;
    _safeAreaVertical =
        _mediaQueryData!.padding.top + _mediaQueryData!.padding.bottom;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;
  }
}
