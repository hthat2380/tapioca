class TopCategory {
  //modal class for Person object
  String id, name, image;
  TopCategory({required this.id, required this.name, required this.image});
}

class RestCategory {
  //modal class for Person object
  String id, name, subTitle, image, bgColor;
  RestCategory(
      {required this.id,
      required this.name,
      required this.subTitle,
      required this.image,
      required this.bgColor});
}

class ShopByBrandModel {
  //modal class for Person object
  String id, name, image;
  ShopByBrandModel({
    required this.id,
    required this.name,
    required this.image,
  });
}
class NearMeModel {
  //modal class for Person object
  String id, name, subTitle, image, bgColor;
  NearMeModel(
      {required this.id,
      required this.name,
      required this.subTitle,
      required this.image,
      required this.bgColor});
}


class NearMeFootModel {
  //modal class for Person object
  String id, name, subTitle, image, bgColor;
  NearMeFootModel(
      {required this.id,
      required this.name,
      required this.subTitle,
      required this.image,
      required this.bgColor});
}

class AmazingDealsModel {
  //modal class for Person object
  String id, name, image;
  AmazingDealsModel({
    required this.id,
    required this.name,
    required this.image,
  });
}
