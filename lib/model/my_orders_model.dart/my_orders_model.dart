class MyOrdersModel {
  //modal class for Person object
  String id,
      orderNo,
      orderDate,
      orderStatus,
      deliveryDate,
      qty,
      time,
      name,
      subTitle,
      subTitle2,
      image,
      price;
  MyOrdersModel({
    required this.id,
    required this.orderNo,
    required this.orderDate,
    required this.orderStatus,
    required this.deliveryDate,
    required this.qty,
    required this.time,
    required this.name,
    required this.subTitle,
    required this.subTitle2,
    required this.image,
    required this.price,
  });
}
