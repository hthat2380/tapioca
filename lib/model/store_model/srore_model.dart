class StoreCategory {
  //modal class for Person object
  String id, name, subTitle, subTitle2, image;
  StoreCategory({
    required this.id,
    required this.name,
    required this.subTitle,
    required this.subTitle2,
    required this.image,
  });
}

class StoreCategoryAll {
  //modal class for Person object
  String id, name, subTitle, subTitle2, image, qty, time;
  StoreCategoryAll({
    required this.id,
    required this.name,
    required this.subTitle,
    required this.subTitle2,
    required this.image,
    required this.qty,
    required this.time,
  });
}
