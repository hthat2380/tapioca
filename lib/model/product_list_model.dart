class Product {
  Product({
    this.productId,
    this.quantity,
    this.productName,
    this.brandName,
    this.type,
    this.description,
    this.image,
    this.price,
    this.specialPrice,
    this.uomValue,
    this.discountAmount,
    this.discountInPercentage,
    this.outOfStock,
  });

  String? productId;
  int? quantity;
  String? productName;
  String? brandName;
  String? type;
  String? description;
  String? image;
  String? price;
  double? specialPrice;
  String? uomValue;
  String? discountAmount;
  String? discountInPercentage;
  bool? outOfStock;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        productId: json["product_id"],
        quantity: json["quantity"],
        productName: json["productName"],
        brandName: json["brandName"],
        type: json["type"],
        description: json["description"],
        image: json["image"],
        price: json["price"],
        specialPrice: json["specialPrice"],
        uomValue: json["uomValue"],
        discountAmount: json["discountAmount"],
        discountInPercentage: json["discountInPercentage"],
        outOfStock: json["outOfStock"],
      );

  Map<String, dynamic> toJson() => {
        "product_id": productId,
        "quantity": quantity,
        "productName": productName,
        "brandName": brandName,
        "type": type,
        "description": description,
        "image": image,
        "price": price,
        "specialPrice": specialPrice,
        "uomValue": uomValue,
        "discountAmount": discountAmount,
        "discountInPercentage": discountInPercentage,
        "outOfStock": outOfStock,
      };
}
