import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';

class RegisterWithGoogle extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Container(
      height: SizeConfig.height * 5.4,
      width: SizeConfig.width * 100,
      decoration: BoxDecoration(
          border: Border.all(color: HexColor('#7A858B')),
          borderRadius: BorderRadius.circular(50)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: SizeConfig.width * 8,
          ),
          Image.asset(googleIcon,
              height: SizeConfig.height * 3.5),
          SizedBox(
            width: SizeConfig.width * 9,
          ),
          Text(
            'Register with Google',
            style: TextStyle(
                color: Colors.black,
                fontFamily: poppinsReg,
                fontSize: SizeConfig.height * 1.8),
          )
        ],
      ),
    );
  }
}
