import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';

class OfferProductItem extends StatelessWidget {
  const OfferProductItem({required this.image,required this.title});
  final String image ;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
            width: SizeConfig.width * 37,
            height: SizeConfig.width * 55,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(187, 187, 187, 0.51),
                      offset: Offset(0, 1),
                      blurRadius: 8),
                ]),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.width * 3,
                  horizontal: SizeConfig.width * 3),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      alignment: Alignment.center,
                      height: SizeConfig.width * 6,
                      width: SizeConfig.width * 6,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: HexColor('#B8B8B8')),
                      child: Icon(Icons.favorite,
                          color: Colors.white,
                          size: SizeConfig.width * 4.3),
                    ),
                  ),
                  Image.asset(
                   image,
                    width: SizeConfig.width * 25,
                  ),
                  Text(
                   title,
                    maxLines: 1,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: HexColor('#262626'),
                        fontFamily: robotoReg,
                        fontSize: SizeConfig.width * 3.3),
                  ),
                  SizedBox(height: SizeConfig.width * 1.5),
                  Row(
                    children: [
                      Text(
                        '₹ ' + '100',
                        style: TextStyle(
                            color: HexColor('#0C4492'),
                            fontFamily: robotoBold,
                            fontSize: SizeConfig.width * 3),
                      ),
                      SizedBox(width: SizeConfig.width * 1.2),
                      Text(
                        '199',
                        style: TextStyle(
                            fontSize: SizeConfig.width * 2.5),
                      ),
                      Spacer(),
                      Container(
                        decoration: BoxDecoration(
                            color: HexColor('#001C39'),
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.width * 2.3,
                              vertical: SizeConfig.width * 1.5),
                          child: Text(
                            'Add to cart',
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: robotoReg,
                                fontSize: SizeConfig.width * 2.4),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )),
        Positioned(
          top: 0,
          left: SizeConfig.width * 4,
          child: Stack(
            children: [
              Image.asset(
                comboOfferTag,
                width: SizeConfig.width * 8,
              ),
              Positioned.fill(
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '5%',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: robotoBold,
                            fontSize: SizeConfig.width * 3),
                      ),
                      Text('OFF',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: robotoBold,
                              fontSize: SizeConfig.width * 2.5))
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
