import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class StoreAppBar extends StatelessWidget {
  final _homeController = Get.put(HomePageController());

  List Lang = ["EN", "MA"];
  String eng = "EN";
  final box = GetStorage();

  get appBarBackArrowColor => null;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double notification = MediaQuery.of(context).padding.top;

    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      width: MediaQuery.of(context).size.width,
      height: Get.width * .3,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25)),
        boxShadow: [
          BoxShadow(
            color: HexColor("#EEEEEE"),
            blurRadius: 2.2, // soften the shadow
            spreadRadius: 1, //extend the shadow
            offset: Offset(
              2.0, // Move to right 10  horizontally
              2.0, // Move to bottom 10 Vertically
            ),
          ),
        ],
      ),
      child: Container(
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.arrow_back, color: appBarBackArrowColor),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Stack(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        height: 22,
                        width: 22,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(
                                    "assets/images/ic_notification.png"))),
                      ),
                      Positioned(
                        right: 0.0,
                        top: 0.0,
                        child: Container(
                          width: 17,
                          height: 10,
                          decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(4.0),
                          ),
                          child: Center(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "2",
                                  style: TextStyle(
                                      fontSize: 8, color: Colors.white),
                                ),
                                Text(
                                  "+",
                                  style: TextStyle(
                                      fontSize: 8, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: Get.height * .01),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 10),
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: Get.width * .13,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: HexColor("#EEEEEE"),
                      blurRadius: 2.2, // soften the shadow
                      spreadRadius: 1, //extend the shadow
                      offset: Offset(
                        2.0, // Move to right 10  horizontally
                        2.0, // Move to bottom 10 Vertically
                      ),
                    ),
                  ]),
              child: Row(
                children: [
                  IconButton(
                      icon: Icon(Icons.search, color: HexColor('#827D7D')),
                      onPressed: null),
                  Expanded(
                    child: TextField(
                      // controller: _editingController,
                      textAlignVertical: TextAlignVertical.center,
                      onChanged: (_) {},
                      decoration: InputDecoration(
                        hintText: 'Search for',
                        hintStyle: TextStyle(color: HexColor('#B0AAAA')),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                      ),
                    ),
                  ),

                  // IconButton(
                  //     highlightColor: Colors.transparent,
                  //     splashColor: Colors.transparent,
                  //     icon: Icon(Icons.clear, color: Theme.of(context).primaryColor.withOpacity(0.5)),
                  //     onPressed: () =>
                  //     setState(() {
                  //       _editingController.clear();
                  //     })
                  //     ),
                ],
              ),
            ),
          ],
        ),
        // Container(
        //   padding: EdgeInsets.only(left: 20, right: 20),
        //   width: MediaQuery.of(context).size.width,
        //   height: 67,
        //   decoration: BoxDecoration(
        //     color: Colors.white,
        //     borderRadius: BorderRadius.only(
        //         bottomLeft: Radius.circular(25),
        //         bottomRight: Radius.circular(25)),
        //     boxShadow: [
        //       BoxShadow(
        //         color: HexColor("#EEEEEE"),
        //         blurRadius: 2.2, // soften the shadow
        //         spreadRadius: 1, //extend the shadow
        //         offset: Offset(
        //           2.0, // Move to right 10  horizontally
        //           2.0, // Move to bottom 10 Vertically
        //         ),
        //       ),
        //     ],
        //   ),
        //   child: Row(
        //     children: [
        //       Container(
        //           height: 28,
        //           width: 32,
        //           child: Image.asset(
        //             "assets/images/ic_search_grey_base_line.png",
        //             width: 30,
        //             fit: BoxFit.fitHeight,
        //           ))
        //     ],
        //   ),
        // )
      ),
    );
  }
}
