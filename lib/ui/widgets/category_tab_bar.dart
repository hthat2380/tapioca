import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryTabBar extends StatelessWidget {
  const CategoryTabBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(bismiFashionImage,
            width: SizeConfig.width * 100,
            // height: SizeConfig.width * 50,
            fit: BoxFit.fitWidth),
        Positioned.fill(
          child: Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Lifestyle',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: SizeConfig.width * 5.5,
                      fontFamily: robotoMedium),
                ),
                SizedBox(height: SizeConfig.width * 2),
                Text('Fashion',
                    style: TextStyle(
                        color: Colors.white.withOpacity(0.5),
                        fontSize: SizeConfig.width * 4,
                        fontFamily: robotoMedium)),
              ],
            ),
          ),
        ),
        Positioned.fill(
            top: SizeConfig.width * 4,
            left: SizeConfig.width * 4,
            child: Align(
                alignment: Alignment.topLeft,
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                    size: SizeConfig.width * 6,
                  ),
                ))),
        Positioned.fill(
            top: SizeConfig.width * 4,
            right: SizeConfig.width * 4,
            child: Align(
                alignment: Alignment.topRight,
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                  size: SizeConfig.width * 7,
                )))
      ],
    );
  }
}
