import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserTextField extends StatelessWidget {
  final String? hint;
  final Color? color;
  final TextEditingController? textController;
  UserTextField({this.color, this.hint, this.textController});

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(LoginController());
    return TextFormField(
      controller: textController,
      validator: (val) {
        if (val!.isEmpty) return 'Empty';
        return null;
      },
      decoration: InputDecoration(
        hintText: hint,
        focusColor: color,
        hintStyle:
            TextStyle(color: color, fontFamily: poppinsExtra, fontSize: 15),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color!),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: color!),
        ),
      ),
    );
  }
}
