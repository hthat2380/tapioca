import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';

class ProductItem extends StatelessWidget {
  final String image;
  final String title;

  ProductItem({required this.title, required this.image});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.only(left: SizeConfig.width * 2),
          child: Container(
            height: SizeConfig.width * 62,
            // color: Colors.yellow,
            child: Container(
              alignment: Alignment.center,
              // height: SizeConfig.width * 35,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.16),
                        offset: Offset(0, 3),
                        blurRadius: 6)
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: Padding(
                padding: EdgeInsets.all(SizeConfig.width * 3),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                        alignment: Alignment.topRight,
                        child: Container(
                            padding: EdgeInsets.all(SizeConfig.width * 1.5),
                            decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.1),
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.favorite,
                              color: Colors.grey.withOpacity(0.8),
                            ))),
                    SizedBox(height: SizeConfig.width * 1.5),
                    Center(
                        child: Image.asset(
                      image,
                      width: SizeConfig.width * 30,
                      height: SizeConfig.width * 30,
                      // fit: BoxFit.fitWidth,
                    )),
                    Text(
                      title,
                      style: TextStyle(
                          color: HexColor('#262626'),
                          fontFamily: poppinsLight,
                          fontSize: SizeConfig.width * 3.3),
                    ),
                    SizedBox(height: SizeConfig.width * 1.5),
                    Text(
                      '₹ ' + '1,00,000',
                      style: TextStyle(
                          color: HexColor('#2E4D7F'),
                          fontFamily: robotoMedium,
                          fontSize: SizeConfig.width * 3.2),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          '1,19,900',
                          style: TextStyle(
                              color: HexColor('#050500').withOpacity(0.6),
                              fontFamily: robotoReg,
                              fontSize: SizeConfig.width * 2.9),
                        ),
                        Spacer(),
                        Container(
                          // height: SizeConfig.width * 5,
                          // width: SizeConfig.width *20,
                          decoration: BoxDecoration(
                              color: HexColor('#001C39'),
                              borderRadius: BorderRadius.circular(
                                  SizeConfig.width * 1.5),
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                    offset: Offset(0, 3),
                                    blurRadius: 6)
                              ]),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: SizeConfig.width * 3,
                                vertical: SizeConfig.width * 1),
                            child: Center(
                              child: Text('Add to cart',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: poppinsReg,
                                      fontSize: SizeConfig.width * 3.2)),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: SizeConfig.width * 3,
          left: 0,
          child: Container(
            width: SizeConfig.width * 20,
            height: SizeConfig.width * 10,
            decoration: BoxDecoration(
                // color: Colors.yellow,
                image: DecorationImage(image: AssetImage(productOfferTage))),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(left: SizeConfig.width * 1),
                child: Text(
                  '35% Discount',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: poppinsLight,
                      fontSize: SizeConfig.width * 2.3),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
