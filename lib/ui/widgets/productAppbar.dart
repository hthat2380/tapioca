import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../routes/route.dart';

class ProductAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;

  ProductAppbar({required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: HexColor('#001C39'),
      title: Text(
        title,
        style: TextStyle(fontSize: 17.sp, fontFamily: poppinsReg),
      ),
      actions: [
        topIcon('assets/images/product_seaech.png', ""),
        topIcon('assets/images/product_fav.png', wishListRoute),
        topIcon('assets/images/product_cart.png', ""),
        SizedBox(width: 6.w)
      ],
    );
  }

  IconButton topIcon(String image, String route) {
    return IconButton(
        padding: EdgeInsets.all(5.w),
        onPressed: () {
          Get.toNamed(route);
        },
        icon: Container(
            child: Container(
                padding: EdgeInsets.all(7.w),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: HexColor('#002449'),
                ),
                child: Image.asset(image))));
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
