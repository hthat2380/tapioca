import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ReviewItem extends StatelessWidget {
  const ReviewItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1.sw,
      // height: 1.sw,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8.w),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.1),
                offset: Offset(0.w, 0.w),
                blurRadius: 1.w)
          ]),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(7.w),
            child: Text(
              "I hate this story, but I just also learned of the things what the actress has done,I was so disappointed. Although she has done something wrong, I just wanna to say that the movie is pretty bad on its own and I hate the song especially Everything I Need. Also,the fight scenes are very boring, just like real people fighting in front of you.",
              style: TextStyle(
                  fontSize: 9.sp,
                  height: 1.5.w,
                  fontFamily: poppinsReg,
                  color: HexColor('#707070')),
            ),
          ),
          Container(
            height: 30.w,
            decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.1),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8.w),
                    bottomRight: Radius.circular(8.w))),
            child: Padding(
              padding:  EdgeInsets.symmetric(horizontal: 10.w),
              child: Row(
                children: [
                  Text(
                    'Ronald',
                    style: TextStyle(
                        fontSize: 12.sp,
                        fontFamily: poppinsReg,
                        color: Colors.black),
                  ),
                  Spacer(),
                  RatingBar(
                    initialRating: 4,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemSize: 18.w,
                    ratingWidget: RatingWidget(
                      full: Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      half: Icon(
                        Icons.star_half,
                        color: Colors.amber,
                      ),
                      empty: Icon(
                        Icons.star,
                        color: Colors.grey.withOpacity(0.5),
                      ),
                    ),
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
