import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';

class HeadingRow extends StatelessWidget {
  final String text;

  HeadingRow({required this.text});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          text,
          style:
          TextStyle(fontSize: SizeConfig.width * 4, fontFamily: robotoReg),
        ),
        Icon(
          Icons.keyboard_arrow_right,
          size: SizeConfig.width * 5,
        )
      ],
    );
  }
}