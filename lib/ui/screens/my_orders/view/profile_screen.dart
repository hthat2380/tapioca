import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../resource/shadows.dart';
import 'widget/store_list_All.dart';

class MyOrders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        centerTitle: true, // Don't show the leading button
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back, color: appBarBackArrowColor),
        ),
      ),
      body: Column(
        children: [
          SizedBox(
            height: Get.width * .05,
          ),
          MyOrdersList()
        ],
      ),
    );
  }
}
