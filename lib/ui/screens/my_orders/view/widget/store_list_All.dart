import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:ayush_buddy/ui/screens/store_screen.dart/view/widget/list/storeListCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../landing_page/view/widget/list/sliderMenu1.dart';

import '../../controller/my_orders_controller.dart';
import 'list_item.dart';

class MyOrdersList extends StatelessWidget {
  final _storeController = Get.put(MyOrdersDetailsController());

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 0),
        shrinkWrap: true,
        itemCount: _storeController.storeList2.length,
        itemBuilder: (context, index) {
          var data = _storeController.storeList2[index];
          return Padding(
            padding: const EdgeInsets.fromLTRB(2, 10, 2, 10),
            child: ItemListWidget(data: data),
          );
        },
      ),
    );
  }
}
