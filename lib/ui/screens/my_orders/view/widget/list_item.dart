import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/supermarketCategoryPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../resource/images.dart';

class ItemListWidget extends StatelessWidget {
  var data;

  ItemListWidget({
    this.data,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        data.orderStatus == 'Shipped'
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 13),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Order Number : ' + data.orderNo,
                          style: TextStyle(fontSize: 12),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Text('Order Date: ' + data.orderDate,
                            style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 13),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(data.orderStatus,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: HexColor('#3BA632'))),
                        data.orderStatus == 'Shipped'
                            ? Text("Arriving by 20 Jan. 2022",
                                style: TextStyle(
                                  fontSize: 10,
                                ))
                            : Text("Wxchange/Return window closed on 18 Jan",
                                style: TextStyle(
                                  fontSize: 10,
                                )),
                      ],
                    ),
                  )
                ],
              )
            : data.orderStatus == 'Cancelled'
                ? Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_refund.png',
                          fit: BoxFit.contain,
                          height: Get.width * .13,
                          width: Get.width * .13,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 13),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Refund Processed",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                )),
                            SizedBox(
                              width: Get.width * .77,
                              child: Text(
                                  "Your refund 899.00 for the return has been processed success fully on 2nd Feb 2021",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.black.withOpacity(.4))),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                : Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_exchange.png',
                          fit: BoxFit.contain,
                          height: Get.width * .13,
                          width: Get.width * .13,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 13),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Exchange Cancelled",
                                style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                )),
                            SizedBox(
                              width: Get.width * .77,
                              child: Text("On thu Jan 2021 as per your request",
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.black.withOpacity(.4))),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
        SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: () {
            Get.toNamed(myOrdersDetails);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Get.width * 0.02),
            child: Container(
              height: Get.width * .45,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.12),
                      blurRadius: 6, // soften the shadow
                      offset: Offset(2.5, 2.5),
                    ),
                  ]),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(6.0),
                            child: Container(
                              height: Get.width * .26,
                              width: Get.width * .18,
                              color: HexColor('##F7F6F6'),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Image.asset(
                                  data.image,
                                  fit: BoxFit.contain,
                                  height: Get.width * .13,
                                  width: Get.width * .13,
                                ),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(10, 2, 5, 0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 8,
                                ),
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        data.name,
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontFamily: "Poppins",
                                            fontWeight: FontWeight.w500),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                          top: 0,
                                          right: 19,
                                        ),
                                        child: Container(
                                          child: Padding(
                                            padding: const EdgeInsets.all(3.0),
                                            child: Text("\t\u20B9" + data.price,
                                                style: TextStyle(
                                                    fontFamily: "PoppinsReg",
                                                    fontWeight: FontWeight.w700,
                                                    fontSize: 16,
                                                    color: Colors.black)),
                                          ),
                                        ),
                                      )
                                    ]),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  data.subTitle,
                                  style: TextStyle(
                                      fontSize: 12, fontFamily: "Poppins"),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  '${data.qty}',
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontFamily: "Poppins",
                                      color:
                                          HexColor('#1B1D16').withOpacity(.6)),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  children: [
                                    Container(
                                        height: 7,
                                        width: 7,
                                        decoration: BoxDecoration(
                                          color: HexColor('#1B1D16')
                                              .withOpacity(.4),
                                          borderRadius:
                                              BorderRadius.circular(50.0),
                                        )),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Text(
                                      '${data.time}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: "Poppins",
                                          color: HexColor('#1B1D16')
                                              .withOpacity(.6)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]),
                  SizedBox(
                    height: Get.width * .02,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(15, 8, 0, 8),
                        child: Image.asset(
                          "assets/images/ic_star_yellow.png",
                          height: Get.width * .05,
                          width: Get.width * .05,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 25, 2),
                        child: Text("View Details",
                            style: TextStyle(
                                fontSize: 14,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w400,
                                color: HexColor('#679CEB'))),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
