import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

import '../../whish_list_screen/controller/whish_controller.dart';

class MyOrdersBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => WhishListController());
}
