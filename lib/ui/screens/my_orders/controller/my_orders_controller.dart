import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../../../../model/my_orders_model.dart/my_orders_model.dart';
import '../../../../model/store_model/srore_model.dart';

class MyOrdersController extends GetxController {
  static MyOrdersController get to => Get.find();

  List<MyOrdersModel> storeList2 = [
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Shipped',
        orderDate: '09-20-2022',
        price: "99"),
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Cancelled',
        orderDate: '09-20-2022',
        price: "99"),
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Pending',
        orderDate: '09-20-2022',
        price: "99"),
  ];
}
