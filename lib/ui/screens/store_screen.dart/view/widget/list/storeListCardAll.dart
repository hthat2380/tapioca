import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/supermarketCategoryPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../resource/images.dart';

class StoreListCardAll extends StatelessWidget {
  var data;

  StoreListCardAll({
    this.data,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Get.to(SupermarketCategoryPage());
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Get.width * 0.02),
            child: Container(
              height: Get.width * .35,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.12),
                      blurRadius: 6, // soften the shadow
                      offset: Offset(2.5, 2.5),
                    ),
                  ]),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(6.0),
                        child: Image.asset(
                          data.image,
                          height: Get.width * .3,
                          width: Get.width * .3,
                          fit: BoxFit.fitHeight,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(5, 2, 5, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              data.name,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              data.subTitle,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins"),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              data.subTitle2,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins"),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              '${data.qty + " | " + data.time}',
                              style: TextStyle(
                                  fontSize: 12,
                                  fontFamily: "Poppins",
                                  color: HexColor('#1B1D16').withOpacity(.6)),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                Container(
                                  height: Get.width * .06,
                                  width: Get.width * .14,
                                  decoration: BoxDecoration(
                                    color: HexColor("##DEDEDE"),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Container(
                                  height: Get.width * .06,
                                  width: Get.width * .14,
                                  decoration: BoxDecoration(
                                    color: HexColor("##DEDEDE"),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                                SizedBox(
                                  width: 2,
                                ),
                                Container(
                                  height: Get.width * .06,
                                  width: Get.width * .14,
                                  decoration: BoxDecoration(
                                    color: HexColor("##DEDEDE"),
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 13, right: 19),
                          child: Container(
                            height: Get.width * .05,
                            width: Get.width * .1,
                            decoration: BoxDecoration(
                              color: appColor,
                              borderRadius: BorderRadius.circular(4.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(3.0),
                              child: Image.asset(
                                ic_rating,
                                height: Get.width * .12,
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10, right: 10),
                          child: Text(
                            "More",
                            style: TextStyle(
                                fontFamily: "PoppinsReg",
                                fontSize: 10,
                                color: Colors.blueAccent),
                          ),
                        ),
                      ],
                    )
                  ]),
            ),
          ),
        ),
      ],
    );
  }
}
