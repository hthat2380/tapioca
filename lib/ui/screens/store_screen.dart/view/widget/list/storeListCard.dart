import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/supermarketCategoryPage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../../resource/images.dart';

class StoreListCard extends StatelessWidget {
  var data;

  StoreListCard({
    this.data,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Get.to(SupermarketCategoryPage());
          },
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: Get.width * 0.02, vertical: Get.width * 0.001),
            child: Container(
              height: Get.width * .44,
              width: Get.width * .335,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.12),
                      blurRadius: 6, // soften the shadow
                      spreadRadius: 1,
                      offset: Offset(2.5, 2.5),
                    ),
                  ]),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: Get.width * .04,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(6.0),
                      child: Image.asset(
                        data.image,
                        height: Get.width * .15,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 2, 5, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              data.name,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w500),
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            Text(
                              data.subTitle,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins"),
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            Text(
                              data.subTitle2,
                              style: TextStyle(
                                  fontSize: 12, fontFamily: "Poppins"),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Container(
                              height: Get.width * .05,
                              width: Get.width * .1,
                              decoration: BoxDecoration(
                                color: appColor,
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Image.asset(
                                  ic_rating,
                                  height: Get.width * .12,
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ]),
            ),
          ),
        ),
      ],
    );
  }
}
