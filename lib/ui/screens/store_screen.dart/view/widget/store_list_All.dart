import 'package:ayush_buddy/ui/screens/store_screen.dart/view/widget/list/storeListCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../landing_page/controller/home_page_controller.dart';
import '../../../landing_page/view/widget/list/sliderMenu1.dart';
import '../../controller/store_controller.dart';
import 'list/storeListCardAll.dart';

class StoreListAll extends StatelessWidget {
  final _storeController = Get.put(StoreController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          _storeController.storeList2.isNotEmpty
              ? Center(
                  child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shrinkWrap: true,
                    physics:
                        ScrollPhysics(parent: NeverScrollableScrollPhysics()),
                    itemCount: _storeController.storeList2.length,
                    itemBuilder: (context, index) {
                      var data = _storeController.storeList2[index];
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(2, 10, 2, 10),
                        child: StoreListCardAll(data: data),
                      );
                    },
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}
