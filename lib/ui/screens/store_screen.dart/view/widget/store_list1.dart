import 'package:ayush_buddy/ui/screens/store_screen.dart/view/widget/list/storeListCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../landing_page/controller/home_page_controller.dart';
import '../../../landing_page/view/widget/list/sliderMenu1.dart';
import '../../controller/store_controller.dart';

class StoreList extends StatelessWidget {
  final _storeController = Get.put(StoreController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _storeController.storeList.isNotEmpty
            ? Container(
                height: Get.width * .48,
                width: Get.width,
                margin: EdgeInsets.only(top: 5, bottom: 0, left: 8, right: 0),
                child: Center(
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shrinkWrap: true,
                    itemCount: _storeController.storeList.length,
                    itemBuilder: (context, index) {
                      var data = _storeController.storeList[index];
                      return StoreListCard(data: data);
                    },
                  ),
                ))
            : Container(),
      ],
    );
  }
}
