import 'package:ayush_buddy/model/store_model/srore_model.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/state_manager.dart';

import '../../../../model/home_screen_model.dart';

class StoreController extends GetxController {
  static StoreController get to => Get.find();
  final CarouselController sliderController = CarouselController();
  final advancedDrawerController = AdvancedDrawerController();
  PageController pageController =
      PageController(viewportFraction: 1, keepPage: true);

  var tabIndex = 0;
  RxInt brandSliderPos = 0.obs;
  CarouselController carouselController = new CarouselController();

  var shopByBrand = <ShopByBrandModel>[].obs;
  var shopByBrandProduct = <ShopByBrandModel>[].obs;

  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }

  @override
  void onInit() {
    pageController = PageController();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (pageController.hasClients) {
        pageController.animateToPage(tabIndex,
            duration: Duration(milliseconds: 1), curve: Curves.easeInOut);
      }
    });
    super.onInit();
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  int _current = 0;
  List imgList = [
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png'
  ];

  List<StoreCategory> storeList = [
    StoreCategory(
      id: "1",
      name: "Budget",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_shop_item.png',
    ),
    StoreCategory(
      id: "1",
      name: "V-One",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_shop_item.png',
    ),
    StoreCategory(
      id: "1",
      name: "Margin free",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_shop_item.png',
    ),
  ];

  List<StoreCategoryAll> storeList2 = [
    StoreCategoryAll(
      id: "1",
      name: "Budget",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_restaurant.png',
      qty: "6",
      time: "24 hour Available",
    ),
    StoreCategoryAll(
      id: "1",
      name: "Budget",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_restaurant2.png',
      qty: "6",
      time: "24 hour Available",
    ),
    StoreCategoryAll(
      id: "1",
      name: "Budget",
      subTitle: "Supermarket",
      subTitle2: "Manjeri",
      image: 'assets/images/ic_restaurant3.png',
      qty: "6",
      time: "24 hour Available",
    ),
  ];

  void onChangeBrandSlider(int index) {
    brandSliderPos.value = index;
    //List view value updated while moving carosel slider
    // brandProducts.value = shopByBrand.value[index].name!;
  }

  void handleMenuButtonPressed() {
    // NOTICE: Manage Advanced Drawer state through the Controller.
    // _advancedDrawerController.value = AdvancedDrawerValue.visible();
    advancedDrawerController.showDrawer();
  }
}
