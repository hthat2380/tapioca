import 'package:ayush_buddy/resource/api.dart';
import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../helper/DalogHeleper.dart';
import '../../../../resource/validation.dart';
import '../controller/settings_controller.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final controller = Get.put(SettingsController());

  bool switchControl = false;

  void toggleSwitch(bool value) {
    if (switchControl == false) {
      setState(() {
        switchControl = true;
      });
      print('Switch is ON');
      // Put your code here which you want to execute on Switch ON event.

    } else {
      setState(() {
        switchControl = false;
      });
      print('Switch is OFF');
      // Put your code here which you want to execute on Switch OFF event.
    }
  }

  @override
  Widget build(BuildContext context) {
    controller.userTypeData();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Settings',
          style: TextStyle(fontFamily: 'PoppinsReg'),
        ),
        backgroundColor: appColor,
      ),
      body: Padding(
        padding:
            const EdgeInsets.only(top: 20, right: 10, left: 10, bottom: 30),
        child: Column(
          children: [
            Obx(() => Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 15, left: 15),
                      child: Container(
                        height: 50,
                        child: Row(
                          children: [
                            Text(
                              'Notification',
                              style: TextStyle(
                                  fontSize: 13, fontFamily: 'PoppinsLight'),
                            ),
                            Spacer(),
                            Transform.scale(
                              scale: 1.1,
                              child: Switch(
                                onChanged: toggleSwitch,
                                value: switchControl,
                                activeColor: appColor,
                                activeTrackColor: HexColor('#D8D8D8'),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    settingsItem('Manage Address', Icons.arrow_forward_ios,
                        SelectAddressRoute),
                    Divider(),
                    settingsItem('Rate Us', Icons.arrow_forward_ios, ''),
                    Divider(),
                    settingsItem('Language', Icons.arrow_forward_ios, ''),
                    Divider(),
                    settingsItem('Share Feedback', Icons.arrow_forward_ios, ''),
                    Divider(),
                    settingsItem(
                        'Refund preferences', Icons.arrow_forward_ios, ''),
                    Divider(),
                    settingsItem(
                        controller.userType.value != SECURE_STORAGE_WHERE_LOGIN
                            ? "Login"
                            : "Logout",
                        Icons.arrow_forward_ios,
                        onBoardingRoute),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget getContent() {
    return Column(
      children: [
        Text("Logout"),
      ],
    );
  }

  Padding settingsItem(String text, IconData icon, String routeName) {
    return Padding(
      padding: const EdgeInsets.only(right: 16, left: 16),
      child: GestureDetector(
        onTap: () {
          if (text == "Logout") {
            var user_type = controller.userType.value.toString();

            print("usertype:" + controller.userType.value.toString());
            // controller.callLogout();
            if (user_type != SECURE_STORAGE_WHERE_LOGIN) {
              Get.offAllNamed(loginMainRoute);
            } else {
              var dialog = CustomAlertDialog(
                  title: "Logout",
                  message: logoutMessage,
                  onPostivePressed: () {
                    controller.callLogout();
                  },
                  positiveBtnText: 'Yes',
                  negativeBtnText: 'No');
              showDialog(
                  context: context, builder: (BuildContext context) => dialog);
            }
          } else {
            Get.toNamed(loginRoute);
          }
        },
        child: Container(
          color: Colors.white,
          height: 50,
          child: Row(
            children: [
              Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'PoppinsLight',
                  color: HexColor('#050500'),
                ),
              ),
              Spacer(),
              Icon(
                icon,
                color: HexColor('#B2B2B2'),
                size: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
