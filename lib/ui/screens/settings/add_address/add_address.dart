import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../helper/size_helper.dart';

class AddAddress extends StatefulWidget {
  @override
  _AddAddress2State createState() => _AddAddress2State();
}

class _AddAddress2State extends State<AddAddress> {
  List place = [
    {"label": "Home", "icon": "images/addhome.png"},
    {"label": "Office", "icon": "images/iconoffice.png"},
    {"label": "Other", "icon": "images/iconlocation2.png"},
  ];
  List<bool> _selections = List.generate(3, (index) => false);
  int? _selected;

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor('#338AFE'),
        elevation: 5,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Add Address",
          style: TextStyle(
              fontFamily: "PoppinsReg", color: Colors.white, fontSize: 18),
        ),
        centerTitle: true,
        actions: [
          IconButton(
              icon: Icon(
                Icons.more_vert,
                color: Colors.white,
              ),
              onPressed: () {})
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            textItem('Name'),
            textFieldItem('Enter your name'),
            textItem('Phone number'),
            textFieldItem('Enter your phone number'),
            textItem('Pincode'),
            Row(
              children: [
                Expanded(child: textFieldItem('Enter yor pin')),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5, right: 15, top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: HexColor('#338AFE'),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              "images/iconlocation.png",
                              height: 25,
                              width: 25,
                            ),
                            SizedBox(
                              width: width * .06,
                            ),
                            FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                'Current location',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "PoppinsReg",
                                    fontSize:
                                        SizeConfig.blockSizeHorizontal * 3),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            textItem('House number & Building name'),
            textFieldItem('Enter your details'),
            textItem('Road name, Area, Street'),
            textFieldItem('Enter your details'),
            textItem('Landmark'),
            textFieldItem('Enter your landmark'),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  color: HexColor('#F2F2F0'),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: ListView.builder(
                        physics: BouncingScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 3,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                _selected = index;
                              });
                            },
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: _selected == index
                                          ? HexColor('#338AFE')
                                          : null,
                                      borderRadius: BorderRadius.circular(20),
                                      border: Border.all(
                                          color: _selected == index
                                              ? HexColor('#338AFE')
                                              : HexColor('#338AFE'))),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(6),
                                        height: 35,
                                        width: 35,
                                        decoration: BoxDecoration(
                                            color: _selected == index
                                                ? Colors.white
                                                : null,
                                            shape: BoxShape.circle),
                                        child: Padding(
                                          padding: const EdgeInsets.all(1.0),
                                          child:
                                              Image.asset(place[index]['icon']),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        place[index]['label'],
                                        style: TextStyle(
                                            color: _selected == index
                                                ? Colors.white
                                                : null),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      )
                                    ],
                                  )),
                            ),
                          );
                        }),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: HexColor('#338AFE'),
                      borderRadius: BorderRadius.circular(30)),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 15),
                    child: Text(
                      'Save & Continue',
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "PoppinsReg",
                          fontSize: 15),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }

  Padding textFieldItem(String hint) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 10),
      child: TextField(
          decoration: InputDecoration(
              isDense: true,
              hintText: hint,
              hintStyle: TextStyle(color: Colors.grey[400]),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: HexColor('#E5E5E5'),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                  color: HexColor('#E5E5E5'),
                ),
              ))),
    );
  }

  Padding textItem(String title) {
    return Padding(
      padding: const EdgeInsets.only(right: 15, left: 15, top: 10),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 13,
            fontFamily: "PoppinsReg",
            fontWeight: FontWeight.bold),
      ),
    );
  }
}
