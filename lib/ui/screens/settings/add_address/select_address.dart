import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro_nullsafety/carousel_pro_nullsafety.dart';

import 'add_address.dart';

class SelectAddress extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<SelectAddress> {
  int ListSelectIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor('#F2F2F0'),
      appBar: AppBar(
        title: Text(
          'Address',
          style: TextStyle(
            fontFamily: "PoppinsReg",
          ),
        ),
        backgroundColor: HexColor('#338AFE'),
        centerTitle: true,
        actions: [
          Icon(Icons.more_vert),
          SizedBox(
            width: 10,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AddAddress();
                }));
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20, left: 20, top: 20),
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: HexColor('#338AFE'))),
                  child: Center(
                      child: Text(
                    'Add new address',
                    style: TextStyle(
                      color: HexColor('#338AFE'),
                      fontWeight: FontWeight.bold,
                      fontFamily: "PoppinsReg",
                    ),
                  )),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: SizedBox(
                  height: 180.0,
                  child: Carousel(
                    dotIncreaseSize: 1.1,
                    dotIncreasedColor: HexColor("#9E9E9E"),
                    dotColor: HexColor("#717C71"),
                    dotBgColor: Colors.transparent,
                    images: [
                      Image.asset("images/partner.png"),
                      Image.asset("images/partner.png"),
                      Image.asset("images/partner.png")
                    ],
                  )),
            ),
            ListView(
              physics: ScrollPhysics(),
              shrinkWrap: true,
              children: [
                addressItem(
                    1,
                    Icons.home_outlined,
                    'Home',
                    '1st Streat, 2nd main road',
                    'Bangaluru 695625',
                    '9856255255'),
                addressItem(
                    2,
                    Icons.home_work_outlined,
                    'Office',
                    '1st Streat, 2nd main road',
                    'Bangaluru 695625',
                    '9856255255'),
                SizedBox(
                  height: 30,
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Padding addressItem(
    int index,
    IconData icon,
    String place,
    String address1,
    String address2,
    String phone,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, right: 15, left: 15),
      child: Container(
        decoration: BoxDecoration(
            color: HexColor('#FFFFFF'),
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: HexColor('#0000001A'),
                offset: Offset(0, 0),
                blurRadius: 9,
              )
            ]),
        child: Padding(
          padding:
              const EdgeInsets.only(right: 20, left: 20, top: 20, bottom: 20),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    'Aswin vinod',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      fontFamily: "PoppinsReg",
                    ),
                  ),
                  Spacer(),
                  Padding(
                    padding: const EdgeInsets.only(right: 8, bottom: 10),
                    child: GestureDetector(
                      onTap: () {
                        setState(() {
                          ListSelectIndex = index;
                        });
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.grey[200],
                        child: CircleAvatar(
                          radius: 12,
                          backgroundColor: ListSelectIndex == index
                              ? Color.fromRGBO(18, 196, 18, 1)
                              : Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        icon,
                        color: HexColor('#EFCD07'),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            place,
                            style: TextStyle(
                              fontSize: 18,
                              fontFamily: "PoppinsLight",
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            address1,
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: "PoppinsLight",
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            address2,
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: "PoppinsLight",
                            ),
                          ),
                          SizedBox(
                            height: 3,
                          ),
                          Text(
                            phone,
                            style: TextStyle(
                              fontSize: 12,
                              fontFamily: "PoppinsLight",
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Spacer(),
                    Container(
                        child: Row(
                      children: [
                        Icon(
                          Icons.edit_outlined,
                          size: 14,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          'Edit address',
                          style: TextStyle(
                              fontFamily: "PoppinsLight",
                              fontSize: 8,
                              color: Colors.black87),
                        ),
                      ],
                    )),
                    SizedBox(
                      width: 13,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(
                            Icons.delete,
                            size: 14,
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            'Delete',
                            style: TextStyle(
                                fontFamily: "PoppinsLight",
                                fontSize: 8,
                                color: Colors.black87),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
