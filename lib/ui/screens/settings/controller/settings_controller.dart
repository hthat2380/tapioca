import 'package:ayush_buddy/resource/api.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/login_page.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../shared/repository/local_auth_repository.dart';

class SettingsController extends GetxController {
  // SettingsController({this.localAuthRepository});
  final _storage = GetStorage();

  final userType = "".obs;

  /// inject repo abstraction dependency
  // final LocalAuthRepository localAuthRepository;

  @override
  void onInit() {
    userTypeData();
    print("type:" + userType.value.toString());
    super.onInit();
  }

  void userTypeData() async {
    final box = GetStorage();
    userType.value = box.read(SECURE_STORAGE_WHERE_LOGIN);
    // print("typeTEst" + box.read(SECURE_STORAGE_IS_LOGIN));
  }

  callLogout() async {
    // showLoaderDialog(Get.overlayContext);

    await Future.delayed(const Duration(seconds: 1), () {
      print("logout");
    });
    clearSession();
    // ProfileViewController.to.clearData();
    // signOutWithGoogle();
    // logoutFacebook();
    // await FirebaseMessaging.instance.deleteToken();

    Get.offAll(loginRoute);
  }

  cancel() async {
    Get.back();
  }

  Future<Null> signOutWithGoogle() async {
    // Sign out with google
    // await googleSignIn.signOut();
    print("User Sign Out");
  }

  void logoutFacebook() async {
    // Future<void> logOut() async => channel.invokeMethod('logOut');
    // await facebookLogIn.logOut();
    print("User Sign Out");
  }

  clearSession() {
    _storage.remove(SECURE_STORAGE_USERNAME);
    _storage.remove(SECURE_STORAGE_EMAIL);
    _storage.remove(SECURE_STORAGE_MOBILE);
    _storage.remove(SECURE_STORAGE_PROFILE_URL);
    _storage.remove(SECURE_STORAGE_USER_ID);
    _storage.remove(SECURE_STORAGE_TOKEN);
    _storage.remove(SECURE_STORAGE_WHERE_LOGIN);
    // _storage.remove(SECURE_STORAGE_FCM_ID);
    // _storage.remove(SECURE_STORAGE_IS_LOGIN);
    update();
  }
}
