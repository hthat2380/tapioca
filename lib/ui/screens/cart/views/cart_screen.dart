import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/cart/views/widget/cart_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../helper/size_helper.dart';
import '../../../../resource/colors.dart';
import '../../../../resource/hex_color.dart';
import '../../../../resource/images.dart';
import '../../../../resource/shadows.dart';
import '../../../widgets/productAppbar.dart';
import '../controller/cart_controller.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double notification = MediaQuery.of(context).padding.top;
    print(notification);
    return Scaffold(
      appBar: ProductAppbar(title: "Cart"),
      body: Stack(
        children: [
          SingleChildScrollView(
              child: Column(
            children: [
              CartItem(),
              addMore(width),
              coupon(),
              address(width, context, height),
              cartDetails(height, width, context),
              SizedBox(
                height: 40,
              )
            ],
          )),
          confirmBlock(width, context)
        ],
      ),
    );
  }

  Positioned confirmBlock(double width, BuildContext context) {
    return Positioned(
      bottom: 0.5,
      left: 0.0,
      right: 0.0,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Row(
          children: [
            Container(
              height: 60,
              width: width,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: listBoxShadow,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 40,
                    width: 150,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        GetBuilder<CartController>(
                          init: CartController(),
                          builder: (_dx) => Column(
                            children: [
                              RichText(
                                text: TextSpan(
                                    text: "\u20B9\t",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                    children: [
                                      TextSpan(
                                          text:
                                              _dx.totalPrice.toString() + '\n',
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black),
                                          children: [
                                            TextSpan(
                                                text: "Expected amount to paid",
                                                style: TextStyle(
                                                    color: HexColor("#12C412"),
                                                    fontSize: 10))
                                          ])
                                    ]),
                              ),
                            ],
                          ),
                        ),
                        // Text("Expected amount to paid",
                        //     style: TextStyle(
                        //         color: HexColor("#12C412"), fontSize: 10))
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        Get.toNamed(payment);
                      },
                      child: Container(
                        height: 36,
                        width: 160,
                        decoration: BoxDecoration(
                            color: appColor,
                            boxShadow: cartShadowLight,
                            borderRadius: BorderRadius.circular(6.0)),
                        child: Center(
                          child: Text(
                            "Continue",
                            style: TextStyle(
                                color: Colors.white, fontFamily: "Poppins"),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Padding coupon() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 15),
      child: Container(
        height: 60,
        color: HexColor("#F8F8F8"),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image.asset(
              ic_coupon,
              height: 30,
            ),
            Text(
              "Apply Promo Code Applied",
              style: TextStyle(
                  fontSize: 15, fontFamily: "Poppins-Regular", color: textGrey),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: HexColor('#B2B2B2'),
              size: 20,
            )
          ],
        ),
      ),
    );
  }

  Container cartDetails(double height, double width, BuildContext context) {
    return Container(
      height: SizeConfig.blockSizeHorizontal * 62,
      width: width,
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          SizedBox(
            height: SizeConfig.screenHeight * .03,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 8.0, top: 8.0),
            child: Row(
              children: [
                Text(
                  "PRICE DETAILS",
                  style: TextStyle(
                      fontFamily: 'PopinsReg',
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              child: Container(
                  child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(7.0),
                      child: Container(
                        width: width * 0.6,
                        child: Column(
                          children: [
                            SizedBox(
                              height: SizeConfig.screenHeight * .0,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Price (1 item)",
                                  style: TextStyle(
                                      fontFamily: 'PopinsReg', fontSize: 12),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Row(
                              children: [
                                Text("Discount",
                                    style: TextStyle(
                                        fontFamily: 'PopinsReg', fontSize: 12)),
                              ],
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.02,
                            ),
                            Row(
                              children: [
                                Text("Delivery Charge",
                                    style: TextStyle(
                                        fontFamily: 'PopinsReg', fontSize: 12)),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 5.0),
                      child: Container(
                        width: width * 0.19,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SizedBox(
                              height: SizeConfig.screenHeight * .0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                    text: TextSpan(
                                        text: "\u20B9",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                        children: [TextSpan(text: '199')])),
                              ],
                            ),
                            SizedBox(
                              height: SizeConfig.blockSizeHorizontal * 4,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                    text: TextSpan(
                                        text: "-\u20B9",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                        children: [TextSpan(text: '99')])),
                              ],
                            ),
                            SizedBox(
                              height: SizeConfig.blockSizeHorizontal * 4,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                    text: TextSpan(
                                        text: "\u20B9",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                        children: [TextSpan(text: '40')])),
                              ],
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: SizeConfig.blockSizeHorizontal * 4,
                ),
                Container(
                  color: HexColor("#70707040"),
                  height: .3,
                ),
                SizedBox(
                  height: SizeConfig.blockSizeHorizontal * 4,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(7.0),
                      child: Container(
                        width: width * 0.6,
                        child: Column(
                          children: [
                            SizedBox(
                              height: SizeConfig.screenHeight * .0,
                            ),
                            Row(
                              children: [
                                Text(
                                  "Grand Total",
                                  style: TextStyle(fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8.0, right: 8.0, top: 5.0),
                      child: Container(
                        width: width * 0.19,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            SizedBox(
                              height: SizeConfig.screenHeight * .0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                RichText(
                                    text: TextSpan(
                                        text: "\u20B9",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                        children: [TextSpan(text: '140')])),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ]))),
        ],
      ),
    );
  }

  Container address(double width, BuildContext context, double height) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 80,
                child: Row(
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                            padding: const EdgeInsets.fromLTRB(10, 5, 5, 5),
                            child: Image.asset(
                              ic_home,
                              height: 30,
                            ))
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Container(
                              height: 60,
                              width: width * .55,
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        "Delivery to : ",
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.grey,
                                            fontFamily: "Poppins-Light"),
                                      ),
                                      Text(
                                        'Home',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: HexColor("#050500")
                                                .withOpacity(.7),
                                            fontWeight: FontWeight.bold,
                                            fontFamily: "Poppins-Light"),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      SizedBox(
                                        width:
                                            SizeConfig.safeBlockHorizontal * 50,
                                        child: Text(
                                          '1 st street, second road mathikere Bangalore 686598',
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          maxLines: 2,
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              height: 1.6,
                                              letterSpacing: .5,
                                              fontSize: 12,
                                              fontFamily: "Poppins-Light"),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: width * .05),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          Get.toNamed(
                                            SelectAddressRoute,
                                            arguments: [
                                              "cartRoute",
                                            ],
                                          );
                                        },
                                        child: Text(
                                          "Change",
                                          style: TextStyle(
                                              color: HexColor("#53A4F7")),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Icon(Icons.arrow_forward_ios,
                                          size: 15, color: HexColor("#12C412")),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: height * .03,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.fromLTRB(width * .03,
                                          0, height * .02, width * .03),
                                      child: Container(
                                        height: 20,
                                        width: 82,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Center(
                                            child: RichText(
                                          text: TextSpan(
                                              text: "Delivery in ",
                                              style: TextStyle(
                                                  fontSize: 8,
                                                  color: Colors.grey),
                                              children: [
                                                TextSpan(
                                                    text: '20 Minute',
                                                    style: TextStyle(
                                                        fontSize: 8,
                                                        color: Colors.black))
                                              ]),
                                        )),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Container addMore(double width) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Get.toNamed(homeRoute);
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          child: Container(
            height: 50,
            width: width,
            padding: EdgeInsets.only(left: 12.0, right: 12.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: listBoxShadowLight),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "Add more",
                    style:
                        TextStyle(fontSize: 15, fontFamily: "Poppins-Regular"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: Image.asset(
                    ic_add,
                    height: 25,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
