import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../../helper/size_helper.dart';
import '../../controller/cart_controller.dart';
import 'caritemWidget.dart';

class CartItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final categoryController = Get.find<CartController>();

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
        child: Padding(
      padding: const EdgeInsets.only(left: 10.0, top: 8.0, right: 10.0),
      child: Container(
        width: width,
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Container(
              width: width,
              child: GetBuilder<CartController>(
                  init: CartController(),
                  builder: (_dx) {
                    if (_dx.products.value.length == 0) {
                      return Center(
                        child: Column(
                          children: [
                            SizedBox(
                              height: SizeConfig.blockSizeHorizontal * 8,
                            ),
                            Text("No item found"),
                            SizedBox(
                              height: SizeConfig.blockSizeHorizontal * 15,
                            )
                          ],
                        ),
                      );
                    }
                    return ListView.builder(
                      itemCount: _dx.products.value.length,
                      shrinkWrap: true,
                      padding: EdgeInsets.symmetric(vertical: 5),
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        return cartItem(
                            productList: _dx.products.value, index: index);
                      },
                    );
                  }),
            )
          ],
        ),
      ),
    ));
  }
}
