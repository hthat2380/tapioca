import 'package:ayush_buddy/resource/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../helper/size_helper.dart';
import '../../../../../model/product_list_model.dart';
import '../../../../../resource/hex_color.dart';
import '../../../../../resource/images.dart';
import '../../../../../resource/shadows.dart';
import '../../controller/cart_controller.dart';

class cartItem extends StatelessWidget {
  List<Product>? productList;
  int? index;

  cartItem({this.productList, required this.index});

  @override
  Widget build(BuildContext context) {
    final productId = productList![index!].productId!;
    final quantity = productList![index!].quantity;
    final productName = productList![index!].productName;
    final brandName = productList![index!].brandName;
    final type = productList![index!].type;
    final image = productList![index!].image;
    final description = productList![index!].description;
    final price = double.parse(productList![index!].price!) * quantity!;
    final specialPrice = productList![index!].specialPrice! * quantity;
    final uomValue = productList![index!].uomValue;
    final discountAmount = productList![index!].discountAmount;
    bool outOfStock = productList![index!].outOfStock!;
    final discountInPercentage = productList![index!].discountInPercentage;

    SizeConfig().init(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 15.0,
      ),
      child: Stack(
        children: [
          Container(
            height: SizeConfig.blockSizeHorizontal * 36,
            width: width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10.0),
                boxShadow: listBoxShadowLight),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 30),
                  child: Column(
                    children: [
                      Stack(children: [
                        GestureDetector(
                          onTap: () {
                            // Get.to(() => ProductDetails(title: productName!),
                            //     arguments: productId);
                          },
                          child: Container(
                              height: SizeConfig.blockSizeHorizontal * 20,
                              width: SizeConfig.blockSizeHorizontal * 25,
                              child: Image.asset(image!)),
                        ),
                        Positioned.fill(
                            child: Align(
                                alignment: Alignment.center,
                                child: outOfStock
                                    ? Container(
                                        width:
                                            SizeConfig.safeBlockHorizontal * 18,
                                        height:
                                            SizeConfig.safeBlockHorizontal * 6,
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.black
                                                      .withOpacity(0.11),
                                                  offset: Offset(0, 3),
                                                  blurRadius: 6)
                                            ]),
                                        child: Center(
                                            child: Text(
                                          'Out of stock',
                                          style: TextStyle(
                                              color: HexColor('#FF0000'),
                                              fontFamily: "PoppinsReg",
                                              fontSize: SizeConfig
                                                      .safeBlockHorizontal *
                                                  2.2),
                                        )),
                                      )
                                    : Container()))
                      ]),
                    ],
                  ),
                ),
                Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: SizeConfig.blockSizeHorizontal * 9),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: SizeConfig.safeBlockHorizontal * 35,
                                child: Text(productName!,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontFamily: "Poppins-Regular",
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: HexColor("#050500"),
                                        height: 1)),
                              ),
                              SizedBox(height: height * .005),
                              SizedBox(
                                width: SizeConfig.safeBlockHorizontal * 30,
                                child: Text(brandName!,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    maxLines: 1,
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontFamily: "PoppinsReg",
                                        color: HexColor("#050500")
                                            .withOpacity(.7))),
                              ),
                              SizedBox(height: height * .005),
                              Text(
                                '${uomValue}',
                                style: TextStyle(
                                    fontSize: 10,
                                    fontFamily: "PoppinsReg",
                                    color: Colors.grey),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: SizeConfig.blockSizeHorizontal * 25),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10),
                                          child: price != specialPrice
                                              ? Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    Stack(
                                                      children: [
                                                        Text(
                                                          "\t\u20B9"
                                                          '${price.toString()}',
                                                          textAlign:
                                                              TextAlign.right,
                                                          style: TextStyle(
                                                              fontSize: 11,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Positioned(
                                                          left: 0.0,
                                                          right: 0.0,
                                                          bottom: 0.0,
                                                          top: 0.0,
                                                          child:
                                                              new RotationTransition(
                                                            turns:
                                                                new AlwaysStoppedAnimation(
                                                                    30 / 360),
                                                            child: Container(
                                                              width: 4,
                                                              child: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .fromLTRB(
                                                                        10,
                                                                        0,
                                                                        10,
                                                                        0),
                                                                child: Divider(
                                                                  color: Colors
                                                                      .grey,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    )
                                                  ],
                                                )
                                              : Container(),
                                        ),
                                        Row(
                                          children: [
                                            Text(
                                              "\t\u20B9" +
                                                  specialPrice.toString(),
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: width * .042,
                                                  fontWeight: FontWeight.bold),
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ]),
                      )
                    ],
                  ),
                ]),
                Spacer(),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          SizeConfig.blockSizeHorizontal * 16,
                          10,
                          SizeConfig.blockSizeHorizontal * 2,
                          0),
                      child: Row(
                        children: [
                          Container(),
                          SizedBox(
                            width: 10,
                          ),
                          InkWell(
                            onTap: () {
                              CartController.to.removeFromCart(index!);

                              // var dialog = CustomAlertDialog(
                              //     title: "Alert",
                              //     message: removeMessage,
                              //     onPostivePressed: () {
                              //       CartController.to.removeFromCart(cartId!);
                              //       Navigator.pop(context);
                              //     },
                              //     positiveBtnText: 'Yes',
                              //     negativeBtnText: 'No');
                              // showDialog(
                              //     context: context,
                              //     builder: (BuildContext context) => dialog);
                            },
                            child: Image.asset(
                              ic_delete,
                              height: 16,
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.fromLTRB(
                          0,
                          0,
                          SizeConfig.blockSizeHorizontal * 5,
                          SizeConfig.blockSizeHorizontal * 5),
                      child: outOfStock != 'Out of Stock'
                          ? GetBuilder<CartController>(
                              init: CartController(),
                              builder: (_dx) {
                                return Container(
                                  height: height * .03,
                                  width: width * .18,
                                  decoration: BoxDecoration(
                                    color: appColor,
                                    boxShadow: listBoxShadowLight,
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                  child: Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        GestureDetector(
                                          child: quantity > 1
                                              ? Icon(
                                                  Icons.remove,
                                                  size: 20,
                                                  color: Colors.white,
                                                )
                                              : Container(
                                                  width: 20, height: 20),
                                          onTap: () {
                                            CartController.to
                                                .decreaseQuantity(index!);
                                          },
                                        ),
                                        Text(
                                          _dx.products[index!].quantity
                                              .toString(),
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                          ),
                                        ),
                                        GestureDetector(
                                          child: Icon(
                                            Icons.add,
                                            size: 20,
                                            color: Colors.white,
                                          ),
                                          onTap: () {
                                            if (quantity.isLowerThan(20)) {
                                              CartController.to
                                                  .increaseQuantity(index!);
                                            }
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              })
                          : Container(),
                    ),

                    // Container(
                    //   height: height * .04,
                    //   width: width * .18,
                    //   decoration: BoxDecoration(
                    //     color: HexColor("#12C412"),
                    //     boxShadow: listBoxShadowLight,
                    //     borderRadius: BorderRadius.circular(20.0),
                    //   ),
                    //   child: Center(
                    //       child: Padding(
                    //     padding: const EdgeInsets.all(8.0),
                    //     child: DropdownButton(
                    //       iconDisabledColor: Colors.white,
                    //       iconEnabledColor: Colors.white,
                    //       isExpanded: true,
                    //       underline: SizedBox(),
                    //       hint: quantityValue == null
                    //           ? Text('Qty')
                    //           : Text(
                    //               "Qty " + quantityValue,
                    //               style: TextStyle(
                    //                   color: Colors.white,
                    //                   fontSize: 10,
                    //                   fontFamily: "Poppins-Regular"),
                    //             ),
                    //       iconSize: 15.0,
                    //       style: TextStyle(color: Colors.black),
                    //       // items: _qty.map(
                    //       //   (val) {
                    //       //     return DropdownMenuItem<String>(
                    //       //       value: val,
                    //       //       child: Padding(
                    //       //         padding: const EdgeInsets.all(8.0),
                    //       //         child: Text(val),
                    //       //       ),
                    //       //     );
                    //       //   },
                    //       // ).toList(),
                    //       onChanged: (val) {
                    //         // setState(
                    //         //   () {
                    //         //     quantityValue = val;
                    //         //   },
                    //         // );
                    //       },
                    //       items: [],
                    //     ),
                    //   )),
                    // ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            right: 0.0,
            top: height * .06,
            child: discountInPercentage == "0"
                ? Container()
                : Container(
                    height: 28,
                    width: 70,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.fill, image: AssetImage(ic_badge)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0, 2, 4, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            '${discountInPercentage}' '%',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 10, color: Colors.white),
                          ),
                          Text(
                            " Discount",
                            style: TextStyle(
                                fontSize: 6,
                                color: Colors.white.withOpacity(.8)),
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
