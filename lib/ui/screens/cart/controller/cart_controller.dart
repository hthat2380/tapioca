import 'dart:ffi';

import 'package:get/get.dart';

import '../../../../model/product_list_model.dart';
import '../../../../resource/images.dart';

class CartController extends GetxController with SingleGetTickerProviderMixin {
  static CartController get to => Get.find();

  var products = <Product>[].obs;

  @override
  void onInit() {
    getCartData();
    totalPrices();
    super.onInit();
  }

  double totalPrice = 0;

  getCartData() async {
    products.value = [
      Product(
          productId: "1",
          quantity: 1,
          productName: "Green Broccoli",
          brandName: "Vegetable",
          type: "",
          description: "test",
          image: ic_fruits,
          price: "100",
          specialPrice: 120.0,
          uomValue: "2kg",
          discountAmount: "100",
          discountInPercentage: "4",
          outOfStock: false),
      Product(
          productId: "2",
          quantity: 1,
          productName: "test",
          brandName: "test",
          type: "",
          description: "test",
          image: ic_fruits,
          price: "100",
          specialPrice: 120.0,
          uomValue: "2kg",
          discountAmount: "100",
          discountInPercentage: "4",
          outOfStock: false),
    ];
  }

  void totalPrices() {
    products.value.forEach((item) {
      totalPrice += ((item.specialPrice!) * item.quantity!);
    });
    print("totlapricetext" + totalPrice.toString());
  }

  void removeFromCart(int index) {
    products.value.removeAt(index);
    totalPrices();
    update();
  }

  void increaseQuantity(int index) {
    products[index].quantity = (products[index].quantity)! + 1;
    totalPrices();
    update();
  }

  void decreaseQuantity(int index) {
    products[index].quantity = (products[index].quantity)! - 1;
    totalPrices();
    update();
  }
}
