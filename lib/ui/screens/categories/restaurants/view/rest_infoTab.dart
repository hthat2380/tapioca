import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RestInfoTab extends StatelessWidget {
  const RestInfoTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 15.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text(
                'Lifestyle',
                style: TextStyle(
                    fontSize: 20.sp,
                    fontFamily: poppinsSemiBold,
                    color: Colors.black.withOpacity(0.97)),
              ),
              Spacer(),
              RatingBar(
                initialRating: 4,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemSize: 18.w,
                ratingWidget: RatingWidget(
                  full: Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  half: Icon(
                    Icons.star_half,
                    color: Colors.amber,
                  ),
                  empty: Icon(
                    Icons.star,
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ),
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                onRatingUpdate: (rating) {
                  print(rating);
                },
              ),
            ],
          ),
          SizedBox(
            height: 5.w,
          ),
          Row(
            children: [
              Text(
                'Indian Mall, Manjeri',
                style: TextStyle(
                    fontSize: 12.sp,
                    fontFamily: poppinsReg,
                    color: HexColor('#424242').withOpacity(0.97)),
              ),
              Spacer(),
              Text(
                'Open Now',
                style: TextStyle(
                    fontSize: 10.sp,
                    fontFamily: robotoMedium,
                    color: HexColor('#589A12').withOpacity(0.97)),
              ),
            ],
          ),
          SizedBox(
            height: 25.w,
          ),
          Text(
            'About',
            style: TextStyle(
                fontSize: 16.sp,
                fontFamily: poppinsMed,
                color: HexColor('#424242').withOpacity(0.97)),
          ),
          SizedBox(
            height: 15.w,
          ),
          Text(
            "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anythingem barrassing hidden in the middle of text.",
            style: TextStyle(
                fontSize: 10.sp,
                height: 1.5.w,
                fontFamily: poppinsLight,
                color: HexColor('#424242')),
          ),
        ],
      ),
    );
  }
}
