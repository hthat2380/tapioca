import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/widgets/review_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RestReviewTab extends StatelessWidget {
  const RestReviewTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 15.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Lifestyle',
                      style: TextStyle(
                          fontSize: 20.sp,
                          fontFamily: poppinsSemiBold,
                          color: Colors.black.withOpacity(0.97)),
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    Text(
                      'Indian Mall, Manjeri',
                      style: TextStyle(
                          fontSize: 12.sp,
                          fontFamily: poppinsReg,
                          color: HexColor('#424242').withOpacity(0.97)),
                    ),
                  ],
                ),
                Spacer(),
                Text(
                  'Rate Shop',
                  style: TextStyle(
                      fontSize: 10.sp,
                      fontFamily: robotoReg,
                      color: Colors.black),
                ),
                SizedBox(
                  width: 10.w,
                ),
                Image.asset(ratingShopIcon, height: 14.w)
              ],
            ),
            SizedBox(
              height: 15.w,
            ),
            Container(
                width: 1.sw,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.w),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.13),
                          offset: Offset(0.w, 3.w),
                          blurRadius: 6.w)
                    ]),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 13.w),
                  child: Image.asset(
                    ratingProgressImage,
                    width: 1.sw,
                    fit: BoxFit.fitWidth,
                  ),
                )),
            SizedBox(
              height: 25.w,
            ),
            Text(
              'User Reviews',
              style: TextStyle(
                  fontSize: 13.sp, fontFamily: poppinsReg, color: Colors.black),
            ),
            SizedBox(
              height: 25.w,
            ),
            ListView.separated(
              shrinkWrap: true,
              itemCount: 6,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return  ReviewItem();
              },

              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 25.w,
                );
              },
            ),

          ],
        ),
      ),
    );
  }
}
