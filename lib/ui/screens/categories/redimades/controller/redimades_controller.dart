import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';

class ReadyMadeController extends GetxController with GetSingleTickerProviderStateMixin{
  late TabController tabController;

  @override
  void onInit() {
    tabController = TabController(
        vsync: this, initialIndex: 0, length: 3);
    super.onInit();
  }


  final categoryList=[
    {'image':'assets/images/chopping-board.png' , 'text':'Burgger'},
    {'image':'assets/images/erik-odiin.png' , 'text':'Pizza'},
    {'image':'assets/images/mahbod-akhzami.png' , 'text':'Fish Fried'},
    {'image':'assets/images/chopping-board.png' , 'text':'Soop'},
    {'image':'assets/images/erik-odiin.png' , 'text':'Seeds'},
    {'image':'assets/images/mahbod-akhzami.png' , 'text':'Chicken'},
  ];

  final comboOffers=[
    {'image':'assets/images/anthony-duran.png' , 'text':'Chicken'},
    {'image':'assets/images/baked-chicken.png' , 'text':'Chicken'},
    {'image':'assets/images/mahbod-akhzam.png' , 'text':'Chicken'},
    {'image':'assets/images/anthony-duran.png' , 'text':'Chicken'},
    {'image':'assets/images/baked-chicken.png' , 'text':'Chicken'},
    {'image':'assets/images/mahbod-akhzam.png' , 'text':'Chicken'},
  ];

  final partyDeals=[
    {'image':'assets/images/dabur_image.png' , 'text':'Dabur Chyawanprash'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Dabur Chyawanprash'},
  ];
}