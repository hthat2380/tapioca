import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/controller/redimades_controller.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/view/redimade_info_tab.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/view/redimade_product_tab.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/view/redimade_review_tab.dart';
import 'package:ayush_buddy/ui/widgets/category_tab_bar.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReadyMadeCategory extends StatelessWidget {
   ReadyMadeCategory({Key? key}) : super(key: key);
  final ReadyMadeController categoryController = Get.put(ReadyMadeController());

  @override
  Widget build(BuildContext context) {
    TextStyle tabTextStyle =
    TextStyle(fontFamily: robotoReg, fontSize: SizeConfig.width * 4);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: SizeConfig.width * 100,
            child: Column(
              children: [
                CategoryTabBar(),
                Container(
                  height: SizeConfig.width * 13,
                  // color: HexColor('#12C412'),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(114, 124, 142, 0.2),
                        offset: Offset(0, 8),
                        blurRadius: 15)
                  ]),
                  child: TabBar(
                    controller: categoryController.tabController,
                    indicatorWeight: SizeConfig.width * 0.7,
                    labelColor: Colors.black,
                    unselectedLabelColor: HexColor('#707070'),
                    // indicator: MD2IndicatorRounded(
                    //   indicatorSize: RounedIndicator.full,
                    //   indicatorHeight: 5.0,
                    //   indicatorColor: Colors.white,
                    // ),
                    labelStyle: tabTextStyle,
                    unselectedLabelStyle: tabTextStyle,
                    indicatorColor: HexColor('#0C4492'),
                    tabs: [
                      Text("Products", style: tabTextStyle),
                      Text("Info", style: tabTextStyle),
                      Text(
                        "Reviews",
                        style: tabTextStyle,
                      ),
                    ],
                  ),
                ),
                // SizedBox(height: SizeConfig.width*10),
                Container(
                  height: SizeConfig.height * 198,
                  child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: categoryController.tabController,
                    children: [ReadyMadeProductTab(), ReadyMadeInfoTab(), ReadyMadeReviewTab()],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
