import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/controller/redimades_controller.dart';
import 'package:ayush_buddy/ui/widgets/heading_row.dart';
import 'package:ayush_buddy/ui/widgets/offerProductItem.dart';
import 'package:ayush_buddy/ui/widgets/productItem.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

class ReadyMadeProductTab extends StatelessWidget {
   ReadyMadeProductTab({Key? key}) : super(key: key);

  final categoryController = Get.find<ReadyMadeController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 5),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 4),
            child: HeadingRow(text: 'Explore Categories'),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: SizeConfig.width * 4,
            ),
            child: CategoryList(categoryController: categoryController),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 4),
            child: HeadingRow(text: 'Party Deals'),
          ),
          ComboOffersList(),
          SizedBox(height: SizeConfig.width * 3),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 4),
            child: HeadingRow(text: 'Combo Offers'),
          ),
          BestProductsList(categoryController: categoryController),
        ],
      ),
    );
  }
}
class CategoryList extends StatelessWidget {
  const CategoryList({
    Key? key,
    required this.categoryController,
  }) : super(key: key);

  final ReadyMadeController categoryController;

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 4),
        itemCount: categoryController.categoryList.length,
        crossAxisCount: 3,
        crossAxisSpacing: SizeConfig.width * 4.5,
        mainAxisSpacing: SizeConfig.width * 4.5,
        itemBuilder: (context, index) {
          var categories = categoryController.categoryList[index];
          return Container(
            height: SizeConfig.width * 32,
            // color: Colors.yellow,
            child: Column(
              children: [
                GestureDetector(
                  onTap: (){
                    //Get.to(()=>SupermarketCategoryInner(title:  categories['text'] ?? ''));
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: SizeConfig.width * 26,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.16),
                              offset: Offset(0, 3),
                              blurRadius: 6)
                        ],
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: EdgeInsets.all(SizeConfig.width * 2),
                      child: Image.asset(categories['image'] ?? ''),
                    ),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 2,
                ),
                Text(
                  categories['text'] ?? '',
                  style: TextStyle(
                      color: HexColor('#858484'),
                      fontFamily: poppinsReg,
                      fontSize: SizeConfig.width * 3.3),
                )
              ],
            ),
          );
        },
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1));
  }
}

class BestProductsList extends StatelessWidget {
  const BestProductsList({
    Key? key,
    required this.categoryController,
  }) : super(key: key);

  final ReadyMadeController categoryController;

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.countBuilder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 4,horizontal: SizeConfig.width * 4),
        itemCount: categoryController.comboOffers.length,
        crossAxisCount: 2,
        crossAxisSpacing: SizeConfig.width * 2.5,
        mainAxisSpacing: SizeConfig.width * 4.5,
        itemBuilder: (context, index) {
          var categories = categoryController.comboOffers[index];
          return InkWell(
              onTap: () {
                // Get.to(() => ProductDetailPage(title: categories['text'] ?? ''));
              },
              child: ProductItem(image:  categories['image'] ?? '',title:  categories['text'] ?? ''));
        },
        staggeredTileBuilder: (int index) => StaggeredTile.fit(1));
  }
}

class ComboOffersList extends StatelessWidget {
  final categoryController = Get.find<ReadyMadeController>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 3),
      child:
      Container(
        height: SizeConfig.width * 52.5,
        width: SizeConfig.width * 100,
        alignment: Alignment.center,
        // color: Colors.yellow,
        child: ListView.separated(
            itemCount: categoryController.partyDeals.length,
            shrinkWrap: true,
            physics: BouncingScrollPhysics(),
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig.width * 2,
                horizontal: SizeConfig.width * 2),
            itemBuilder: (context, index) {
              var items = categoryController.partyDeals[index];
              return OfferProductItem(title:  items['text'] ?? '',image:  items['image'] ?? '',);
            },
            separatorBuilder: (context, index) =>
                SizedBox(width: SizeConfig.width * 5)),
      ),
    );
  }
}