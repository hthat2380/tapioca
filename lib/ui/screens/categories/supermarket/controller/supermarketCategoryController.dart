import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';

class SupermarketCategoryController extends GetxController with GetSingleTickerProviderStateMixin{
  late TabController tabController;

  @override
  void onInit() {
    tabController = TabController(
        vsync: this, initialIndex: 0, length: 3);
    super.onInit();
  }


  final categoryList=[
    {'image':'assets/images/vegitables.png' , 'text':'Vegetables'},
    {'image':'assets/images/fruits.png' , 'text':'Fruits'},
    {'image':'assets/images/dairy.png' , 'text':'Dairy'},
    {'image':'assets/images/dry_fruits.png' , 'text':'Dry Fruits'},
    {'image':'assets/images/seeds.png' , 'text':'Seeds'},
    {'image':'assets/images/chicken.png' , 'text':'Chicken'},
  ];

  final bestProducts=[
    {'image':'assets/images/avocado.png' , 'text':'Avocado'},
    {'image':'assets/images/lettuse.png' , 'text':'Lettuce'},
    {'image':'assets/images/tomatos.png' , 'text':'Tomato'},
    {'image':'assets/images/green_broccoli.png' , 'text':'Green Broccoli'},
    {'image':'assets/images/mushroom.png' , 'text':'Mushroom'},
    {'image':'assets/images/lettuse.png' , 'text':'Lettuce'},
  ];

  final comboList=[
    {'image':'assets/images/dabur_image.png' , 'text':'Dabur Chyawanprash'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Pepsi 3L'},
    {'image':'assets/images/dabur_image.png' , 'text':'Dabur Chyawanprash'},
  ];
}