import 'package:get/get.dart';

class SupermarketCategoryInnerController extends GetxController{
  var selectedCategory = ''.obs;
  var categoryList = [
    'Kurthas',
    'Top',
    'Churidhar',
    'Inner Waers',
    'other'
  ];

  final items=[
    {'image':'assets/images/avocado.png' , 'text':'Avocado'},
    {'image':'assets/images/lettuse.png' , 'text':'Lettuce'},
    {'image':'assets/images/tomatos.png' , 'text':'Tomato'},
    {'image':'assets/images/green_broccoli.png' , 'text':'Green Broccoli'},
    {'image':'assets/images/mushroom.png' , 'text':'Mushroom'},
    {'image':'assets/images/lettuse.png' , 'text':'Lettuce'},
    {'image':'assets/images/tomatos.png' , 'text':'Tomato'},
    {'image':'assets/images/green_broccoli.png' , 'text':'Green Broccoli'},
    {'image':'assets/images/mushroom.png' , 'text':'Mushroom'},
  ];

  @override
  void onInit() {
    selectedCategory.value = categoryList[0];
    super.onInit();
  }
}