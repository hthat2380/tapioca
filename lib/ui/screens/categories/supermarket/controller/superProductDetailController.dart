import 'package:carousel_slider/carousel_controller.dart';
import 'package:get/get.dart';

class ProductDetailController extends GetxController{
  var current = 0.obs;
  final CarouselController controller = CarouselController();
  final List<String> imgList = [
    'assets/images/avocado.png',
    'assets/images/lettuse.png',
    'assets/images/tomatos.png'
  ];

}