import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/controller/supermarketCategoryController.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/superProductsTab.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/super_reviews_tab.dart';
import 'package:ayush_buddy/ui/widgets/category_tab_bar.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'super _info_tab.dart';

class SupermarketCategoryPage extends StatelessWidget {
  final SupermarketCategoryController categoryController = Get.put(SupermarketCategoryController());

  @override
  Widget build(BuildContext context) {
    TextStyle tabTextStyle =
        TextStyle(fontFamily: robotoReg, fontSize: SizeConfig.width * 4);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            width: SizeConfig.width * 100,
            child: Column(
              children: [
                CategoryTabBar(),
                Container(
                  height: SizeConfig.width * 13,
                  // color: HexColor('#12C412'),
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(114, 124, 142, 0.2),
                        offset: Offset(0, 8),
                        blurRadius: 15)
                  ]),
                  child: TabBar(
                    controller: categoryController.tabController,
                    indicatorWeight: SizeConfig.width * 0.7,
                    labelColor: Colors.black,
                    unselectedLabelColor: HexColor('#707070'),
                    // indicator: MD2IndicatorRounded(
                    //   indicatorSize: RounedIndicator.full,
                    //   indicatorHeight: 5.0,
                    //   indicatorColor: Colors.white,
                    // ),
                    labelStyle: tabTextStyle,
                    unselectedLabelStyle: tabTextStyle,
                    indicatorColor: HexColor('#0C4492'),
                    tabs: [
                      Text("Products", style: tabTextStyle),
                      Text("Info", style: tabTextStyle),
                      Text(
                        "Reviews",
                        style: tabTextStyle,
                      ),
                    ],
                  ),
                ),
                // SizedBox(height: SizeConfig.width*10),
                Container(
                  height: SizeConfig.height * 198,
                  child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: categoryController.tabController,
                    children: [SuperProductTab(), SuperInfoTab(), SuperReviewTab()],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
