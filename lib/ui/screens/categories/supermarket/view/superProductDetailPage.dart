import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/controller/superProductDetailController.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/controller/supermarketCategoryController.dart';
import 'package:ayush_buddy/ui/widgets/heading_row.dart';
import 'package:ayush_buddy/ui/widgets/offerProductItem.dart';
import 'package:ayush_buddy/ui/widgets/review_item.dart';
import 'package:ayush_buddy/ui/widgets/productAppbar.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class ProductDetailPage extends StatelessWidget {
  final String title;

  ProductDetailPage({required this.title});

  final ProductDetailController productController =
      Get.put(ProductDetailController());
  final categoryController = Get.find<SupermarketCategoryController>();

  @override
  Widget build(BuildContext context) {
    final List<Widget> imageSliders = productController.imgList
        .map((item) => Container(
              child: Container(
                // margin: EdgeInsets.all(5.0),
                child: Image.asset(item, fit: BoxFit.fitWidth, width: 0.8.sw),
              ),
            ))
        .toList();

    return Scaffold(
      appBar: ProductAppbar(title: title),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20.w),
                Padding(
                  padding: EdgeInsets.only(left: 17.w),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Ratings',
                              style: TextStyle(
                                  color: HexColor('#050500'),
                                  fontFamily: robotoReg,
                                  fontSize: 13.sp)),
                          SizedBox(height: 5.w),
                          RatingBar(
                            initialRating: 4,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemSize: 12.w,
                            ratingWidget: RatingWidget(
                              full: Icon(
                                Icons.star,
                                color: Colors.amber,
                              ),
                              half: Icon(
                                Icons.star_half,
                                color: Colors.amber,
                              ),
                              empty: Icon(
                                Icons.star,
                                color: Colors.grey.withOpacity(0.5),
                              ),
                            ),
                            itemPadding: EdgeInsets.only(right: 8.0),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                        ],
                      ),
                      // Spacer(),
                    ],
                  ),
                ),
                SizedBox(height: 10.w),
                CarouselSlider(
                  items: imageSliders,
                  carouselController: productController.controller,
                  options: CarouselOptions(
                      autoPlay: true,
                      enlargeCenterPage: true,
                      viewportFraction: 1,
                      // aspectRatio: 2.0,
                      onPageChanged: (index, reason) {
                        productController.current.value = index;
                      }),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children:
                      productController.imgList.asMap().entries.map((entry) {
                    return GestureDetector(
                      onTap: () =>
                          productController.controller.animateToPage(entry.key),
                      child: Container(
                        width: 8.w,
                        height: 8.w,
                        margin: EdgeInsets.symmetric(
                            vertical: 15.w, horizontal: 3.w),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color:
                                (Theme.of(context).brightness == Brightness.dark
                                        ? Colors.white
                                        : Colors.grey)
                                    .withOpacity(
                                        productController.current == entry.key
                                            ? 0.9
                                            : 0.4)),
                      ),
                    );
                  }).toList(),
                ),
                Container(
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 17.w, vertical: 10.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Green Broccoli',
                                  style: TextStyle(
                                      color: HexColor('#050500'),
                                      fontFamily: poppinsReg,
                                      fontSize: 15.sp),
                                ),
                                SizedBox(height: 10.w),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      '₹ 100',
                                      style: TextStyle(
                                          color: HexColor('#050500'),
                                          fontFamily: robotoReg,
                                          fontSize: 20.sp),
                                    ),
                                    SizedBox(
                                      width: 10.w,
                                    ),
                                    Text(
                                      '199',
                                      style: TextStyle(
                                          color: HexColor('#050500')
                                              .withOpacity(0.6),
                                          fontFamily: robotoReg,
                                          fontSize: 12.sp),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 10.w),
                                Container(
                                  padding: EdgeInsets.all(6.w),
                                  decoration: BoxDecoration(
                                      color: HexColor('#FCEBB2'),
                                      borderRadius: BorderRadius.circular(4.w)),
                                  child: Text(
                                    'Limited Stock',
                                    style: TextStyle(
                                        color: HexColor('#956942'),
                                        fontFamily: poppinsReg,
                                        fontSize: 12.sp),
                                  ),
                                )
                              ],
                            ),
                            Spacer(),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 7.w, horizontal: 18.w),
                              decoration: BoxDecoration(
                                  color: HexColor('#1B2B3B'),
                                  borderRadius: BorderRadius.circular(32.w),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.2),
                                        offset: Offset(3, 3),
                                        blurRadius: 9)
                                  ]),
                              child: Text(
                                'Add to Cart',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14.sp,
                                    fontFamily: poppinsReg),
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 10.w),
                        Text(
                          'Select KG',
                          style: TextStyle(
                              color: HexColor('#050500').withOpacity(0.5),
                              fontFamily: robotoReg,
                              fontSize: 13.sp),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 32.w,
                  child: ListView.separated(
                    itemCount: 5,
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(horizontal: 16.w),
                    itemBuilder: (context, index) {
                      return Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.symmetric(horizontal: 22.w),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.w),
                            border: Border.all(color: HexColor('#707070'))),
                        child: Text(
                          '1 KG',
                          style: TextStyle(
                              color: HexColor('#2A2A28').withOpacity(0.8),
                              fontSize: 13.sp,
                              fontFamily: poppinsReg),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => SizedBox(width: 16.w),
                  ),
                ),
                SizedBox(height: 20.w),
                Container(
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.16),
                        offset: Offset(0, 0),
                        blurRadius: 6)
                  ]),
                  child: Padding(
                    padding: EdgeInsets.all(17.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Product Details',
                          style: TextStyle(
                              fontSize: 14.sp,
                              fontFamily: robotoMedium,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 17.w),
                        detailItem('Product Name', 'Green Broccoli'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Calories', '31g'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Protein', '2.5g'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Carbs', '06g'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Fiber', '06g'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Fat', '06g'),
                        SizedBox(
                          height: 8.w,
                        ),
                        detailItem('Details',
                            'Lorem ipsum dolor sit amet, conse adipiscing elit. Pellentesque eget ipsum id ex bibendum blandit.Duis Pellentesque eget'),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 5.w),
                Container(
                  decoration: BoxDecoration(color: Colors.white, boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.16),
                        offset: Offset(0, 3),
                        blurRadius: 4),
                  ]),
                  child: Padding(
                    padding: EdgeInsets.all(17.w),
                    child: Row(
                      children: [
                        Text(
                          'All Details',
                          style: TextStyle(
                              fontSize: 14.sp, fontFamily: robotoMedium),
                        ),
                        Spacer(),
                        Icon(Icons.keyboard_arrow_right)
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20.w),
                Container(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 17.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Seller : Budget supper market',
                          style: TextStyle(
                              fontSize: 16.sp,
                              fontFamily: poppinsMed,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 5.w),
                        Text(
                          'Hurry, Only 5 left!',
                          style: TextStyle(
                            fontSize: 13.sp,
                            color: HexColor('#FF0000'),
                            fontFamily: poppinsReg,
                          ),
                        ),
                        SizedBox(height: 10.w),
                        Text('Delivery within 20 Minutes'),
                        SizedBox(height: 20.w),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 17.w),
                  child: HeadingRow(text: 'Similar products'),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 3),
                  child: Container(
                    height: SizeConfig.width * 52.5,
                    width: SizeConfig.width * 100,
                    alignment: Alignment.center,
                    // color: Colors.yellow,
                    child: ListView.separated(
                        itemCount: categoryController.comboList.length,
                        shrinkWrap: true,
                        physics: BouncingScrollPhysics(),
                        scrollDirection: Axis.horizontal,
                        padding: EdgeInsets.symmetric(
                            vertical: SizeConfig.width * 2,
                            horizontal: SizeConfig.width * 2),
                        itemBuilder: (context, index) {
                          var items = categoryController.comboList[index];
                          return OfferProductItem(
                            title: items['text'] ?? '',
                            image: items['image'] ?? '',
                          );
                        },
                        separatorBuilder: (context, index) =>
                            SizedBox(width: SizeConfig.width * 5)),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: EdgeInsets.all(17.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Reviews & Ratings',
                              style: TextStyle(
                                  fontFamily: robotoMedium,
                                  fontSize: 14.sp,
                                  color: Colors.black),
                            ),
                            Spacer(),
                            Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 7.w, horizontal: 11.w),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(6.w),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(0.15),
                                        offset: Offset(0, 3),
                                        blurRadius: 4)
                                  ]),
                              child: Text(
                                'Rate Product',
                                style: TextStyle(color: HexColor('#001C39')),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20.w,
                        ),
                        Container(
                            width: 1.sw,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(8.w),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black.withOpacity(0.13),
                                      offset: Offset(0.w, 3.w),
                                      blurRadius: 6.w)
                                ]),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15.w, vertical: 13.w),
                              child: Image.asset(
                                ratingProgressImage,
                                width: 1.sw,
                                fit: BoxFit.fitWidth,
                              ),
                            )),
                        SizedBox(
                          height: 25.w,
                        ),
                        Text(
                          'User Reviews',
                          style: TextStyle(
                              fontSize: 13.sp,
                              fontFamily: poppinsReg,
                              color: Colors.black),
                        ),
                        SizedBox(
                          height: 25.w,
                        ),
                        ListView.separated(
                          shrinkWrap: true,
                          itemCount: 6,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return ReviewItem();
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return SizedBox(
                              height: 25.w,
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            Positioned(
              top: 20.w,
              right: 13.w,
              child: Column(
                children: [
                  rightIcon(fav_grey_icon),
                  SizedBox(
                    height: 20.w,
                  ),
                  rightIcon(share_grey_icon),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget detailItem(String head, String sub) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 0.46.sw,
          child: Text(
            head,
            style: TextStyle(
                color: HexColor('#333333'),
                fontSize: 12.sp,
                fontFamily: poppinsLight),
          ),
        ),
        // Spacer(),
        Flexible(
          child: Text(
            sub,
            style: TextStyle(
                color: HexColor('#333333'),
                fontSize: 12.sp,
                fontFamily: poppinsLight),
          ),
        ),
      ],
    );
  }

  Container rightIcon(String image) {
    return Container(
        padding: EdgeInsets.all(8.w),
        height: 35.w,
        width: 35.w,
        decoration: BoxDecoration(
            color: HexColor('#F4F4F4'),
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  offset: Offset(0, 0),
                  blurRadius: 3)
            ]),
        child: Image.asset(image));
  }
}
