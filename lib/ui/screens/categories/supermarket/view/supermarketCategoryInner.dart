import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/controller/supermarketCategoryInnerController.dart';
import 'package:ayush_buddy/ui/widgets/productAppbar.dart';
import 'package:ayush_buddy/ui/widgets/productItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

class SupermarketCategoryInner extends StatelessWidget {
  final String title;

  SupermarketCategoryInner({required this.title});

  final SupermarketCategoryInnerController categoryInnerController =
      Get.put(SupermarketCategoryInnerController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ProductAppbar(title: title),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 15.w),
            Image.asset(freshVegitableBanner,
                width: 1.sw, fit: BoxFit.fitWidth),
            SizedBox(height: 15.w),
            Container(
              height: 54.w,
              width: 1.sw,
              // color: Colors.yellow,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                physics: BouncingScrollPhysics(),
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                itemCount: categoryInnerController.categoryList.length,
                itemBuilder: (context, index) {
                  var categories = categoryInnerController.categoryList[index];
                  return Obx(() {
                    return GestureDetector(
                      onTap: () {
                        categoryInnerController.selectedCategory.value =
                            categories;
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10.w),
                        child: categoryInnerController.selectedCategory.value ==
                                categories
                            ? Container(
                                // width: 100.w,
                                decoration: BoxDecoration(
                                    color: HexColor('#1B2B3B'),
                                    boxShadow: [
                                      BoxShadow(
                                          color:
                                              Color.fromRGBO(27, 43, 59, 0.27),
                                          offset: Offset(0, 4.w),
                                          blurRadius: 6.w)
                                    ],
                                    borderRadius: BorderRadius.circular(19.w)),
                                child: Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 30.w),
                                  child: Center(
                                    child: Text(categories,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontFamily: poppinsReg,
                                            fontSize: 10.sp)),
                                  ),
                                ),
                              )
                            : Container(
                                // width: 100.w,
                                decoration: BoxDecoration(
                                    color: HexColor('#FBFBFB'),
                                    border: Border.all(
                                        color: HexColor('#E8E8E8'),
                                        width: 0.5.w),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color.fromRGBO(
                                              196, 196, 196, 0.1),
                                          offset: Offset(0, 4.w),
                                          blurRadius: 6.w)
                                    ],
                                    borderRadius: BorderRadius.circular(19.w)),
                                child: Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 30.w),
                                  child: Center(
                                    child: Text(categories,
                                        style: TextStyle(
                                            color: HexColor('#747474'),
                                            fontFamily: poppinsReg,
                                            fontSize: 10.sp)),
                                  ),
                                ),
                              ),
                      ),
                    );
                  });
                },
                separatorBuilder: (context, index) {
                  return SizedBox(width: 11.w);
                },
              ),
            ),
            SizedBox(height: 15.w),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Vegetables & Fruits',
                    style: TextStyle(
                        color: HexColor('#262626'),
                        fontFamily: poppinsReg,
                        fontSize: 15.sp),
                  ),
                  Spacer(),
                  Image.asset(
                    sortIcon,
                    height: 27.w,
                  ),
                  SizedBox(width: 10.w),
                  Image.asset(
                    filterIcon,
                    height: 28.w,
                  )
                ],
              ),
            ),
            StaggeredGridView.countBuilder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                padding: EdgeInsets.symmetric(vertical: 20.w, horizontal: 10.w),
                itemCount: categoryInnerController.items.length,
                crossAxisCount: 2,
                crossAxisSpacing: 10.w,
                mainAxisSpacing: 15.w,
                itemBuilder: (context, index) {
                  var categories = categoryInnerController.items[index];
                  return ProductItem(
                      image: categories['image'] ?? '',
                      title: categories['text'] ?? '');
                },
                staggeredTileBuilder: (int index) => StaggeredTile.fit(1))
          ],
        ),
      ),
    );
  }
}
