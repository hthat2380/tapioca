import 'package:ayush_buddy/resource/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../../../resource/hex_color.dart';

class PaymentMethod extends StatefulWidget {
  @override
  _PaymentMethodState createState() => _PaymentMethodState();
}

class _PaymentMethodState extends State<PaymentMethod> {
  @override
  List IconList = [
    {
      "image": "assets/images/CreditCard.png",
      "Text": "Credit card / debit card"
    },
    {"image": "assets/images/Netbanking.png", "Text": "Net banking"},
    {"image": "assets/images/Upi.png", "Text": "UPI"},
    {"image": "assets/images/WalletIcon.png", "Text": "Wallet"},
    {"image": "assets/images/GiftCard.png", "Text": "Wallet"}
  ];
  List CardList = [
    Container(
      height: 150,
      width: 75,
      decoration: BoxDecoration(
          color: HexColor("#F7F7F7"),
          borderRadius: BorderRadius.circular(26),
          boxShadow: [
            BoxShadow(
              color: HexColor('#E0E0E0'),
              blurRadius: 2.0, // soften the shadow
              spreadRadius: .1, //extend the shadow
              offset: Offset(
                .5, // Move to right 10  horizontally
                .5, // Move to bottom 10 Vertically
              ),
            ),
          ]),
      child: Icon(
        Icons.add,
        size: 50,
        color: HexColor("#A8A8A8"),
      ),
    ),
    Image.asset("assets/images/AtmCard.png"),
    Image.asset("assets/images/AtmCard.png"),
    Image.asset("assets/images/AtmCard.png")
  ];

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color.fromRGBO(27, 20, 100, 1),
              Color.fromRGBO(28, 27, 109, 1),
              Color.fromRGBO(32, 48, 136, 1),
              Color.fromRGBO(39, 81, 180, 1),
              Color.fromRGBO(48, 127, 240, 1),
              Color.fromRGBO(51, 138, 254, 1),
            ], transform: GradientRotation(.7))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  height: 45,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: Colors.white,
                          ),
                          iconSize: 30,
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      SizedBox(
                        width: 80,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Center(
                            child: Text(
                              "Payment Method",
                              style: TextStyle(
                                  color: HexColor("#99CAFF"),
                                  fontSize: 16,
                                  fontFamily: "PoppinsReg"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Expanded(child: SizedBox()),
                Stack(
                  overflow: Overflow.visible,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(50),
                            topLeft: Radius.circular(50),
                          )),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 60.0, bottom: 20, left: 20),
                              child: Text(
                                "Other payment method",
                                style: TextStyle(
                                    fontFamily: "PoppinsReg",
                                    color: HexColor("#6D6D6D"),
                                    fontSize: 15),
                              ),
                            ),
                            ListView.builder(
                                padding: EdgeInsets.zero,
                                shrinkWrap: true,
                                itemCount: IconList.length,
                                itemBuilder: (context, index) => Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 8.0),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: HexColor("#F7F7F7"),
                                            boxShadow: [
                                              BoxShadow(
                                                color: textGrey,
                                                blurRadius: 7.0,
                                                // soften the shadow
                                                spreadRadius: .1,
                                                //extend the shadow
                                                offset: Offset(
                                                  .5,
                                                  // Move to right 10  horizontally
                                                  .5, // Move to bottom 10 Vertically
                                                ),
                                              ),
                                            ]),
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 16.0),
                                          child: Row(
                                            children: [
                                              Container(
                                                  height: 55,
                                                  width: 55,
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 5),
                                                  child: Image.asset(
                                                      IconList[index]
                                                          ["image"])),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 20.0),
                                                child: Text(
                                                  IconList[index]["Text"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w600,
                                                      fontSize: 13),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    )),
                            SizedBox(
                              height: 35,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(30),
                                      topLeft: Radius.circular(30)),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(27, 20, 100, 1),
                                    Color.fromRGBO(28, 27, 109, 1),
                                    Color.fromRGBO(32, 48, 136, 1),
                                    Color.fromRGBO(39, 81, 180, 1),
                                    Color.fromRGBO(48, 127, 240, 1),
                                    Color.fromRGBO(51, 138, 254, 1),
                                  ], transform: GradientRotation(.7))),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      children: [
                                        Text(
                                          "\u20B9 250",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 25,
                                          ),
                                        ),
                                        Text("Grand total",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontFamily: "PoppinsReg"))
                                      ],
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        paymentPopup(context);
                                      },
                                      child: Container(
                                        width: 160,
                                        height: 50,
                                        decoration: BoxDecoration(
                                            color: HexColor("#338AFE"),
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.black12,
                                                  spreadRadius: .021,
                                                  blurRadius: 1),
                                            ]),
                                        child: Center(
                                            child: Text(
                                          "Pay Now",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontFamily: "PoppinsReg"),
                                        )),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ]),
                    ),
                    Positioned(
                      top: -90,
                      child: SizedBox(
                        height: 120,
                        width: 500,
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            itemCount: CardList.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0),
                                child: CardList[index],
                              );
                            }),
                      ),
                    )
                  ],
                )
              ],
            )),
      ),
    );
  }

  Future<dynamic> paymentPopup(BuildContext context) {
    return showDialog(
      context: context,
      builder: (_) => WillPopScope(
        onWillPop: () async {
          Navigator.pop(context);
          return true;
        },
        child: AlertDialog(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.zero,
          content: Container(
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.only(bottom: 30),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 45.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Image.asset(
                    "images/PopUpTick.png",
                    height: 100,
                  ),
                  Text(
                    "Order confirmed",
                    style: TextStyle(fontFamily: "PoppinsReg", fontSize: 20),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Your order confirmed successfully Estimated delivery date : 10 dec 2020",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: "PoppinsReg",
                        fontSize:
                            MediaQuery.of(context).size.width < 380 ? 9 : 10,
                        color: Colors.black54),
                  ),
                ],
              ),
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(13), color: Colors.white),
            width: 150,
            height: 300,
          ),
        ),
      ),
    );
  }
}
