import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../resource/shadows.dart';

class ProfileScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        centerTitle: true, // Don't show the leading button
        title: Text(
          "Profile",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back, color: appBarBackArrowColor),
        ),
      ),
      body: Column(
        children: [
          profileHeader(),
          SizedBox(
            height: Get.width * .05,
          ),
          profileDetailsRow("First Name", "vivek"),
          profileDetailsRow("Last Name", "Kelly"),
          bioDetailsRow("Bio", ""),
          genderDetailsRow("Gender", "Male", "Female"),
          buttonUpdate(),
        ],
      ),
    );
  }

  Padding buttonUpdate() {
    return Padding(
      padding: EdgeInsets.only(right: 20.0, top: Get.width * .2),
      child: GestureDetector(
        onTap: () {},
        child: Container(
          height: Get.width * .1,
          width: 160,
          decoration: BoxDecoration(
              color: appColor,
              boxShadow: cartShadowLight,
              borderRadius: BorderRadius.circular(6.0)),
          child: Center(
            child: Text(
              "Update",
              style: TextStyle(color: Colors.white, fontFamily: "RobotoReg"),
            ),
          ),
        ),
      ),
    );
  }

  Container profileDetailsRow(String text1, String text2) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
        child: Column(
          children: [
            SizedBox(
              height: Get.width * .05,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  text1,
                  style: TextStyle(
                      color: profileTitleColor,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'RobotoReg',
                      fontSize: 16),
                ),
                Text(
                  text2,
                  style: TextStyle(fontFamily: 'RobotoLite', fontSize: 16),
                ),
                SizedBox(
                  width: Get.width * .05,
                ),
                Text("Edit",
                    style: TextStyle(
                        color: profileTitleGreen,
                        fontFamily: 'RobotoLite',
                        fontSize: 16))
              ],
            ),
            SizedBox(
              height: Get.width * .05,
            ),
            new Divider(
              color: textGrey,
            ),
          ],
        ),
      ),
    );
  }

  Container genderDetailsRow(String text1, String text2, String text3) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
        child: Column(
          children: [
            SizedBox(
              height: Get.width * .05,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  text1,
                  style: TextStyle(
                      color: profileTitleColor,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'RobotoReg',
                      fontSize: 16),
                ),
                Container(
                  width: Get.width * .15,
                  height: Get.width * .08,
                  decoration: BoxDecoration(
                      color: profileGenderBgColor,
                      borderRadius: BorderRadiusDirectional.circular(16)),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        text2,
                        style: TextStyle(
                            fontFamily: 'RobotoLite',
                            fontSize: 12,
                            color: profileGenderText),
                      ),
                    ),
                  ),
                ),
                Container(
                  width: Get.width * .15,
                  height: Get.width * .08,
                  decoration: BoxDecoration(
                      color: profileGenderBgColor,
                      borderRadius: BorderRadiusDirectional.circular(16)),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        text3,
                        style: TextStyle(
                            fontFamily: 'RobotoLite',
                            fontSize: 12,
                            color: profileGenderText),
                      ),
                    ),
                  ),
                ),
                Text("Edit",
                    style: TextStyle(
                        color: profileTitleGreen,
                        fontFamily: 'RobotoLite',
                        fontSize: 16))
              ],
            ),
            new Divider(
              color: textGrey,
            ),
          ],
        ),
      ),
    );
  }

  Container bioDetailsRow(String text1, String text2) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(15, 0, 15, 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: Get.width * .01,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  text1,
                  style: TextStyle(
                      color: profileTitleColor,
                      fontWeight: FontWeight.w500,
                      fontFamily: 'RobotoReg',
                      fontSize: 16),
                ),
                Text("Edit",
                    style: TextStyle(
                        color: profileTitleGreen,
                        fontFamily: 'RobotoLite',
                        fontSize: 16))
              ],
            ),
            SizedBox(
              height: Get.width * .05,
            ),
            Text(
              "A brief description of who you are",
              style: TextStyle(
                  color: HexColor('#B7BECD'),
                  fontFamily: 'RobotoReg',
                  fontSize: 16),
            ),
            SizedBox(
              height: Get.width * .01,
            ),
            new Divider(
              color: textGrey,
            ),
          ],
        ),
      ),
    );
  }

  Container profileHeader() {
    return Container(
      height: 200,
      width: double.infinity,
      color: appColor,
      child: Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ClipOval(
              child: Image.asset(
                "assets/images/ic_profile_avathar.png",
                fit: BoxFit.cover,
                width: 90.0,
                height: 90.0,
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Robert R. Walker",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w700,
                  fontFamily: "PoppinsLight"),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "rayjo@gmail.com",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                  fontWeight: FontWeight.w500,
                  fontFamily: "PoppinsLight"),
            ),
          ],
        ),
      ),
    );
  }
}
