import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../../../../model/my_orders_model.dart/my_orders_model.dart';
import '../../../../model/store_model/srore_model.dart';

class MyOrdersDetailsController extends GetxController {
  static MyOrdersDetailsController get to => Get.find();

  var confirmClicked = false.obs;
  final formKey = GlobalKey<FormState>(); // var validate = false.obs;
  List cancelReasons = [
    "Expected delivery time is too long",
    "Purchased the product elsewhere",
    "Price for the product has decreased",
    "Changed my mind",
    "Changes in address and phone number",
    "Order placed by mistake",
    "Other",
  ];

  final selectedRefund = 'bank'.obs;
  var bankSelected = true.obs;
  var medcoinSelected = false.obs;

  final selectedReason = [].obs;
  final text = ''.obs;

  List<MyOrdersModel> storeList2 = [
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Shipped',
        orderDate: '09-20-2022',
        price: "99"),
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Cancelled',
        orderDate: '09-20-2022',
        price: "99"),
    MyOrdersModel(
        id: "1",
        name: "Green Broccoli",
        subTitle: "Vegetable",
        subTitle2: "Manjeri",
        image: 'assets/images/image_veg.png',
        qty: "1KG",
        time: "Exchange/Return window closed on 18 Jan",
        deliveryDate: '',
        orderNo: '0000001',
        orderStatus: 'Pending',
        orderDate: '09-20-2022',
        price: "99"),
  ];

  List returnreason = [
    {"check": false, "reason": "The product is not the same i ordered"},
    {"check": false, "reason": "The package is defective/ open"},
    {"check": false, "reason": "I have purchased the product elsewhere"},
    {"check": false, "reason": "Order arrived too late"},
    {"check": false, "reason": "Product is expired"},
    {"check": false, "reason": "Other"},
  ];

  List price = [
    {
      "val": "Sub Total",
      "rs": "250",
      "bold": true,
    },
    {"val": "Coupon Discount", "rs": "(-)50", "bold": false},
    {"val": "Medi cash used", "rs": "(-)50", "bold": false},
    {"val": "Delivery Charge", "rs": "(-)10", "bold": false},
    {"val": "Paid Online", "rs": "(-)100", "bold": false},
  ];

  List ordercancel = [
    {"img": "assets/images/cancel2.png", "val": "Cancel\n"},
    {"img": "assets/images/exchange.png", "val": "Exchange\n"},
    {"img": "assets/images/return2.png", "val": "Return\n"},
    {"img": "assets/images/down.png", "val": "Download\nInvoice"},
  ];
}
