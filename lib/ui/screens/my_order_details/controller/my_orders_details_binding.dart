import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

import '../../whish_list_screen/controller/whish_controller.dart';

class MyOrdersDetailsBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => MyOrdersDetailsController());
}
