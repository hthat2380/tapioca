import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:ayush_buddy/ui/screens/store_screen.dart/view/widget/list/storeListCard.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../../routes/route.dart';
import '../../../landing_page/controller/home_page_controller.dart';
import '../../../landing_page/view/widget/list/sliderMenu1.dart';

import 'list_item.dart';

class MyOrdersDetailsList extends StatelessWidget {
  final _storeController = Get.put(MyOrdersDetailsController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.width * .7,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 0),
        shrinkWrap: true,
        itemCount: 1,
        itemBuilder: (context, index) {
          var data = _storeController.storeList2[index];
          return Padding(
            padding: const EdgeInsets.fromLTRB(2, 10, 2, 10),
            child: GestureDetector(
                onTap: () {
                  Get.toNamed(myOrdersDetails);
                },
                child: ItemListDetailsWidget(data: data)),
          );
        },
      ),
    );
  }
}
