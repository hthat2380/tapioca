import 'dart:ui';

import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../../resource/hex_color.dart';

class CancelDialoge extends StatelessWidget {
  final String? amount;
  final String? proId;
  CancelDialoge({this.amount, this.proId});
  final MyOrdersDetailsController cancelController =
      Get.put(MyOrdersDetailsController());

  @override
  Widget build(BuildContext context) {
    Future<bool> _willPopCallback() async {
      Get.back();
      return true;
    }

    return WillPopScope(
      onWillPop: _willPopCallback,
      child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                // height: MediaQuery.of(context).size.height * .725,
                child: Form(
                  key: cancelController.formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        // height: MediaQuery.of(context).size.height * .63,
                        width: MediaQuery.of(context).size.width - 10,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(20),
                            topLeft: Radius.circular(20),
                          ),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left: 20, top: 20, bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Why cancelling order ?",
                                    style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "PoppinsReg"),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Please choose the correct reason for cancelling",
                                    style: TextStyle(
                                        fontSize: 13,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey,
                                        fontFamily: "PoppinsLight"),
                                  ),
                                ],
                              ),
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            Container(
                                padding: EdgeInsets.only(
                                    left: 20, top: 10, bottom: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Select Reason",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400,
                                            color: Colors.grey,
                                            fontFamily: "PoppinsLight")),
                                    Obx(() {
                                      return Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: cancelController
                                              .cancelReasons
                                              .map(
                                                (e) => Container(
                                                  height: 33,
                                                  child: Row(
                                                    children: [
                                                      Checkbox(
                                                        value: cancelController
                                                            .selectedReason
                                                            .contains(e),
                                                        onChanged: (value) {
                                                          cancelController
                                                              .selectedReason
                                                              .clear();
                                                          // if (value) {
                                                          //   cancelController
                                                          //       .selectedReason
                                                          //       .add(e);
                                                          // } else {
                                                          //   cancelController
                                                          //       .selectedReason
                                                          //       .remove(e);
                                                          // }
                                                          // setState(() {
                                                          //   e["check"] = !e["check"];
                                                          // });
                                                        },
                                                        checkColor:
                                                            Colors.blue[600],
                                                        hoverColor:
                                                            Colors.blue[600],
                                                      ),
                                                      Expanded(
                                                          child: Text(
                                                        e,
                                                        style: TextStyle(
                                                            fontFamily:
                                                                "PoppinsLight",
                                                            fontSize: 12),
                                                      )),
                                                    ],
                                                  ),
                                                ),
                                              )
                                              .toList());
                                    }),
                                  ],
                                )),
                            Divider(
                              thickness: 1,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        bottom: 5, right: 5, left: 35),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(1),
                                      // border: Border.all(color: Colors.grey[400])
                                    ),
                                    child: TextFormField(
                                      style: TextStyle(fontSize: 17),
                                      decoration: InputDecoration(
                                        hintText: "Additional Comments",
                                        hintStyle: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w200),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey
                                                    .withOpacity(.3)),
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey
                                                    .withOpacity(.3)),
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                      ),
                                      onChanged: (value) {
                                        cancelController.text.value = value;
                                      },
                                      validator: (text) {
                                        if (text == null || text.isEmpty) {
                                          return 'Additional Comments Can\'t Be Empty';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: Row(
                                children: [
                                  Obx(() {
                                    return Checkbox(
                                      value:
                                          cancelController.confirmClicked.value,
                                      onChanged: (value) {
                                        cancelController.confirmClicked.value =
                                            !cancelController
                                                .confirmClicked.value;
                                      },
                                    );
                                  }),
                                  Expanded(
                                      child: Text(
                                    "I am confirm that cancel this order",
                                    style: TextStyle(
                                        fontSize: 11,
                                        fontFamily: "PoppinsLight",
                                        color: Colors.grey),
                                  )),
                                ],
                              ),
                            ),
                            // SizedBox(
                            //   height: MediaQuery.of(context).viewInsets
                            // )
                          ],
                        ),
                      ),
                      Container(
                        decoration:
                            BoxDecoration(color: Colors.white, boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(.3),
                            blurRadius: 2.0, // soften the shadow
                            spreadRadius: .1, //extend the shadow
                            offset: Offset(
                              .5, // Move to right 10  horizontally
                              .5, // Move to bottom 10 Vertically
                            ),
                          ),
                        ]),
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        height: 70,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Refund amount",
                                  style: TextStyle(
                                      fontFamily: "PoppinsReg",
                                      color: HexColor('#338AFE')),
                                ),
                                Text(
                                  "₹" + "960",
                                  style: TextStyle(
                                    fontSize: 19,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            Obx(() {
                              return GestureDetector(
                                onTap: cancelController.confirmClicked.value
                                    ? () {
                                        if (cancelController
                                            .formKey.currentState!
                                            .validate()) {
                                          // Get.find<MyOrderDetailsController>().cartDetails.value.cashOnDelivery == true?
                                          // cancelController.cancelOrder(proId,'cod'):
                                          showMaterialModalBottomSheet(
                                              context: context,
                                              backgroundColor:
                                                  Colors.transparent,
                                              builder: (context) =>
                                                  RefundModelPopup(
                                                    amount: amount,
                                                    prodId: proId,
                                                  ));
                                        }
                                      }
                                    : () {},
                                child: Container(
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color:
                                          cancelController.confirmClicked.value
                                              ? HexColor('#338AFE')
                                              : Colors.grey.withOpacity(0.7),
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: cancelController
                                              .confirmClicked.value
                                          ? [
                                              BoxShadow(
                                                color:
                                                    Colors.grey.withOpacity(.2),
                                                blurRadius:
                                                    2.0, // soften the shadow
                                                spreadRadius:
                                                    .1, //extend the shadow
                                                offset: Offset(
                                                  .5, // Move to right 10  horizontally
                                                  .5, // Move to bottom 10 Vertically
                                                ),
                                              ),
                                            ]
                                          : null),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 5),
                                    child: Text(
                                      "Cancel Order",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 18),
                                    ),
                                  ),
                                ),
                              );
                            }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )),
    );
  }
}

class RefundModelPopup extends StatelessWidget {
  final String? amount;
  final String? prodId;

  RefundModelPopup({this.amount, this.prodId});
  final MyOrdersDetailsController cancelController =
      Get.put(MyOrdersDetailsController());

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
        child: Container(
          // height: MediaQuery.of(context).size.height * .545,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 10,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(20),
                          topLeft: Radius.circular(20),
                        ),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding:
                                EdgeInsets.only(left: 20, top: 20, bottom: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Choose a refund mode",
                                  style: TextStyle(
                                      fontSize: 19,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "PoppinsLight"),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  "choose the refund mode",
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.grey,
                                      fontFamily: "PoppinsLight"),
                                ),
                              ],
                            ),
                          ),
                          Divider(
                            thickness: 1,
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Obx(() {
                            return Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                margin: EdgeInsets.only(bottom: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        if (!cancelController
                                            .bankSelected.value) {
                                          cancelController
                                              .medcoinSelected(false);
                                          cancelController.bankSelected(true);
                                          cancelController
                                              .selectedRefund.value = 'bank';
                                        } else {}
                                      },
                                      child: Column(
                                        children: [
                                          Container(
                                            decoration: BoxDecoration(
                                                color: HexColor('#F7F7F7'),
                                                borderRadius:
                                                    BorderRadius.circular(8),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.5),
                                                    blurRadius: 2.0,
                                                    // soften the shadow
                                                    spreadRadius: .5,
                                                    //extend the shadow
                                                    offset: Offset(
                                                      0,
                                                      // Move to right 10  horizontally
                                                      0, // Move to bottom 10 Vertically
                                                    ),
                                                  ),
                                                ],
                                                border: cancelController
                                                        .bankSelected.value
                                                    ? Border.all(
                                                        color:
                                                            HexColor('#338AFE'))
                                                    : null),
                                            padding: EdgeInsets.all(8),
                                            height: 120,
                                            width: 120,
                                            child: Stack(
                                              children: [
                                                Positioned(
                                                    top: 25,
                                                    left: 15,
                                                    child: Image.asset(
                                                      "images/card1.png",
                                                      width: 70,
                                                      height: 70,
                                                    )),
                                                Positioned(
                                                    top: 10,
                                                    left: 25,
                                                    child: Image.asset(
                                                      "images/card2.png",
                                                      width: 70,
                                                      height: 70,
                                                    )),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(left: 6),
                                            child: Text(
                                              "Bank to source A/C",
                                              style: TextStyle(
                                                  fontFamily: "PoppinsLight",
                                                  fontSize: 13),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        if (!cancelController
                                            .medcoinSelected.value) {
                                          cancelController.bankSelected(false);
                                          cancelController
                                              .medcoinSelected(true);
                                          cancelController
                                              .selectedRefund.value = 'medcoin';
                                        } else {}
                                      },
                                      child: Stack(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: Color.fromRGBO(
                                                        247, 247, 247, 1),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: Colors.grey
                                                            .withOpacity(0.5),
                                                        blurRadius: 2.0,
                                                        // soften the shadow
                                                        spreadRadius: .5,
                                                        //extend the shadow
                                                        offset: Offset(
                                                          0,
                                                          // Move to right 10  horizontally
                                                          0, // Move to bottom 10 Vertically
                                                        ),
                                                      ),
                                                    ],
                                                    border: cancelController
                                                            .medcoinSelected
                                                            .value
                                                        ? Border.all(
                                                            color: HexColor(
                                                                '#338AFE'))
                                                        : null),
                                                padding: EdgeInsets.only(
                                                    left: 15, top: 10),
                                                height: 120,
                                                width: 120,
                                                child: Image.asset(
                                                  "assets/images/wallet.png",
                                                  height: 110,
                                                  width: 110,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 6),
                                                child: Text(
                                                  "Wallet",
                                                  style: TextStyle(
                                                      fontFamily:
                                                          "PoppinsLight"),
                                                ),
                                              )
                                            ],
                                          ),
                                          Positioned(
                                            top: 10,
                                            right: 0,
                                            child: Container(
                                              alignment: Alignment.center,
                                              width: 60,
                                              height: 15,
                                              color: HexColor('#338AFE'),
                                              child: Text(
                                                "Instant",
                                                style: TextStyle(
                                                    fontSize: 9,
                                                    color: Colors.white,
                                                    fontFamily: "PoppinsReg"),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ));
                          }),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    color: Colors.white,
                    blurRadius: 2.0, // soften the shadow
                    spreadRadius: .1, //extend the shadow
                    offset: Offset(
                      .5, // Move to right 10  horizontally
                      .5, // Move to bottom 10 Vertically
                    ),
                  ),
                ]),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                height: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Refund amount",
                          style: TextStyle(
                              fontFamily: "PoppinsReg",
                              color: HexColor('#338AFE')),
                        ),
                        Text(
                          "₹" + '950',
                          style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {
                        // cancelController.cancelOrder(prodId,cancelController.selectedRefund.value);
                      },
                      child: Container(
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            color: HexColor('#338AFE'),
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(.2),
                                blurRadius: 2.0, // soften the shadow
                                spreadRadius: .1, //extend the shadow
                                offset: Offset(
                                  .5, // Move to right 10  horizontally
                                  .5, // Move to bottom 10 Vertically
                                ),
                              ),
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 5),
                          child: Text(
                            "Confirm",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontSize: 18),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
