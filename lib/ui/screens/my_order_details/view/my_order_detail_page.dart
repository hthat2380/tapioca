import 'dart:ui';

import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:ayush_buddy/ui/screens/my_order_details/view/widget/cancel_bottom_sheet.dart';
import 'package:ayush_buddy/ui/screens/my_order_details/view/widget/store_list_All.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../../resource/hex_color.dart';
import '../../my_orders/view/widget/store_list_All.dart';

class OrderDetails extends GetView<MyOrdersDetailsController> {
  double height = Get.height / 100;
  double width = Get.width / 100;

  @override
  Widget build(BuildContext context) {
    TextStyle detailStyle = TextStyle(
      fontSize: width * 3.5,
      color: HexColor('#050500'),
      fontFamily: "PoppinsReg",
    );

    return Scaffold(
        backgroundColor: Colors.white,
        appBar:
            AppBar(backgroundColor: appColor, title: Text("MyOrderDetails")),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: width * 3.5, vertical: width * 4),
            child: Column(
              children: [
                MyOrdersDetailsList(),
                orderStatus(),
                RatingBar(),
                OrderCategory(),
                priceDetails(),
                SizedBox(height: width * 3)
              ],
            ),
          ),
        ));
  }

  Widget priceDetails() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: width * 2.5, top: width * 6),
          child: Text(
            "Price Details",
            style: TextStyle(
                fontFamily: "PoppinsReg",
                fontSize: width * 4.3,
                color: HexColor('#050500').withOpacity(0.7)),
          ),
        ),
        SizedBox(height: width * 3),
        Container(
          width: width * 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: HexColor('#F7F7F7'),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.15),
                  blurRadius: 5.0, // soften the shadow
                  offset: Offset(
                    2, // Move to right 10  horizontally
                    2, // Move to bottom 10 Vertically
                  ),
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * 3.5),
                child: Column(
                  children: [
                    SizedBox(height: width * 3),
                    Padding(
                      padding: EdgeInsets.only(bottom: width * 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Sub Total',
                            style: TextStyle(
                              fontFamily: "PoppinsReg",
                              fontSize: width * 3.7,
                              color: HexColor('#050500'),
                            ),
                          ),
                          Text(
                            '1000',
                            style: TextStyle(
                              fontFamily: "PoppinsReg",
                              fontSize: width * 3.7,
                              color: HexColor('#050500'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    priceRow(
                      text1: "Coupon discount",
                      operator: '(-)',
                      text2: "20",
                    ),
                    priceRow(
                        text1: "Med coin used", operator: '(-)', text2: '10'),
                    priceRow(
                        text1: "Member discount", operator: '(-)', text2: '12'),
                    priceRow(
                        text1: "Delivery charge", operator: '(+)', text2: '50'),
                    priceRow(
                        text1: "Donation amount", operator: '(+)', text2: '20')
                  ],
                ),
              ),
              Container(
                height: width * 10,
                padding: EdgeInsets.symmetric(horizontal: width * 3.5),
                decoration: BoxDecoration(
                  color: HexColor('#E6E6E6').withOpacity(0.52),
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Total Payable',
                      style: TextStyle(
                        fontFamily: "PoppinsReg",
                        fontSize: width * 4,
                        color: HexColor('#050500'),
                      ),
                    ),
                    Text(
                      "950",
                      style: TextStyle(
                          fontFamily: "PoppinsReg",
                          fontSize: width * 4,
                          color: HexColor('#050500')),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget RatingBar() {
    return Padding(
      padding:
          EdgeInsets.only(top: width * 5, right: width * .5, left: width * .5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 8, 0, 4),
            child: SvgPicture.asset(
              "assets/images/ic_rating_bar.svg",
              height: Get.width * .05,
              width: Get.width * .05,
              fit: BoxFit.fitHeight,
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 25, 2),
            child: Text("View Details",
                style: TextStyle(
                    fontSize: 14,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w400,
                    color: HexColor('#679CEB'))),
          )
        ],
      ),
    );
  }

  Widget OrderCategory() {
    return Padding(
      padding:
          EdgeInsets.only(top: width * 5, right: width * .5, left: width * .5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: controller.ordercancel.map((e) {
          int index = controller.ordercancel.indexOf(e);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  if (index == 0) {
                    Get.bottomSheet(
                        CancelDialoge(amount: "950", proId: '12345'),
                        isScrollControlled: true,
                        backgroundColor: Colors.transparent);
                    // : ScaffoldMessenger.of(Get.overlayContext!)
                    //     .showSnackBar(SnackBar(
                    //     content: Text('Cancel not available on this order'),
                    //     backgroundColor: Colors.blueAccent,
                    //   ));
                  } else if (index == 1) {
                    // controller.data.value.eligibleForReturn
                    //     ? Get.to(
                    //         () => ReturnPage(
                    //             amount: controller
                    //                     .cartDetails.value.totalAmountToBePaid
                    //                     .toString() ??
                    //                 ""),
                    //         arguments: controller.argument ?? '')
                    //     : ScaffoldMessenger.of(Get.overlayContext)
                    //         .showSnackBar(SnackBar(
                    //         content: Text('Return not available on this order'),
                    //         backgroundColor: Colors.blueAccent,
                    //       ));
                  } else {
                    // Get.to(() => TaxInvoice());
                  }
                },
                child: Container(
                  height: width * 23,
                  width: width * 23,
                  padding: EdgeInsets.all(width * 5.5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(13),
                    color: Colors.white,
                    // border: index == 0 && true
                    //     ? Border.all(color: Colors.black.withOpacity(0.13))
                    //     : index == 1 && true
                    //         ? Border.all(color: Colors.black.withOpacity(0.13))
                    //         : null,
                    boxShadow: index == 0 && true
                        ? []
                        : index == 1 && true
                            ? []
                            : [
                                // BoxShadow(
                                //   color: Colors.black.withOpacity(0.13),
                                //   blurRadius: 8, // soften the shadow
                                //   offset: Offset(
                                //       3, 3 // Move to bottom 10 Vertically
                                //       ),
                                // ),
                              ],
                  ),
                  child: Image.asset(
                    e['img'],
                  ),
                ),
              ),
              SizedBox(
                height: width * 1.5,
              ),
              Text(
                e['val'],
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: "PoppinsReg",
                    fontSize: width * 3.2,
                    color: HexColor('#050500')),
              ),
            ],
          );
        }).toList(),
      ),
    );
  }

  Widget orderStatus() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: width * 2.5, top: width * 6),
          child: Text(
            "Order Status",
            style: TextStyle(
                fontFamily: "PoppinsReg",
                fontSize: width * 4.3,
                color: HexColor('#050500').withOpacity(0.7)),
          ),
        ),
        SizedBox(height: width * 3),
        Container(
          padding:
              EdgeInsets.symmetric(vertical: width * 4, horizontal: width * 4),
          width: width * 100,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: HexColor('#F7F7F7'),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.15),
                  blurRadius: 5.0, // soften the shadow
                  offset: Offset(
                    2, // Move to right 10  horizontally
                    2, // Move to bottom 10 Vertically
                  ),
                )
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  (true == 'cancelled' ||
                          true == 'doctor rejected' ||
                          true == 'pharmacy rejected')
                      ? Image.asset("assets/images/error.png",
                          height: width * 5)
                      : Image.asset("assets/images/tick.png",
                          height: width * 7),
                  SizedBox(
                    width: width * 1.5,
                  ),
                  Text(
                    'Order Confirmed',
                    style: TextStyle(
                      color: HexColor('#050500'),
                      fontFamily: "PoppinsReg",
                      fontSize: width * 4.2,
                    ),
                  ),
                ],
              ),
              SizedBox(height: width * 2.5),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: width * 8.5),
                  Container(
                    width: 10,
                    // height: 260,
                    child: Stack(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 5),
                          alignment: Alignment.center,
                          child: StepProgressIndicator(
                            fallbackLength: 320,
                            size: 1.5,
                            padding: 1,
                            totalSteps: 6,
                            direction: Axis.vertical,
                            currentStep: 1,
                            roundedEdges: Radius.circular(10),
                            unselectedColor:
                                HexColor('#B5B5B5').withOpacity(0.51),
                            selectedColor: HexColor('#3F3F3F').withOpacity(0.6),
                          ),
                        ),
                        Positioned(
                          top: 4.5,
                          right: 0,
                          child: Container(
                            height: 320,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                statusDot(0),
                                statusDot(1),
                                statusDot(2),
                                statusDot(3),
                                statusDot(4),
                                statusDot(5),
                                statusDot(6),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 52,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order placed"),
                              timeText("asdsadas"),
                            ],
                          ),
                        ),
                        Container(
                          height: 52,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order confirmed"),
                              timeText(true == 'doctor rejected' ? "true" : ''),
                            ],
                          ),
                        ),
                        Container(
                          height: 51,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order under review"),
                              timeText('asdasdasdasdasdasd'),
                            ],
                          ),
                        ),
                        Container(
                          height: 52,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order packed"),
                              timeText(true == 'pharmacy rejected' ? '' : ''),
                            ],
                          ),
                        ),
                        Container(
                          height: 52,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order shipped"),
                              timeText(''),
                            ],
                          ),
                        ),
                        Container(
                          height: 52,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Out for delivery"),
                              timeText('gfhgfhf'),
                            ],
                          ),
                        ),
                        Container(
                          height: 49,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              statusText("Order delivered"),
                              timeText('cfcgc'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget timeText(String text) {
    return Padding(
      padding: EdgeInsets.only(left: 2),
      child: Text(
        text,
        style: TextStyle(
            fontFamily: "PoppinsReg",
            fontSize: 10,
            color: HexColor('#050500').withOpacity(0.5)),
      ),
    );
  }

  Text statusText(String text) {
    return Text(
      text,
      style: TextStyle(
          fontFamily: "PoppinsReg", fontSize: 13, color: HexColor('#050500')),
    );
  }

  Widget statusDot(int index) {
    return CircleAvatar(radius: 5, backgroundColor: HexColor('#3F3F3F'));
  }
}

class priceRow extends StatelessWidget {
  final String? text1;
  final String? text2;
  final String? operator;

  priceRow({this.text1, this.text2, this.operator});

  double height = Get.height / 100;
  double width = Get.width / 100;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(
          bottom: width * 2,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              text1!,
              style: TextStyle(
                fontFamily: "PoppinsReg",
                fontSize: width * 3.2,
                color: HexColor('#050500').withOpacity(0.6),
              ),
            ),
            Container(
              child: Row(
                children: [
                  Text(
                    operator!,
                    style: TextStyle(
                      fontFamily: "PoppinsReg",
                      fontSize: width * 2.5,
                      color: HexColor('#050500').withOpacity(0.6),
                    ),
                  ),
                  Text(
                    text2!,
                    style: TextStyle(
                      fontFamily: "PoppinsReg",
                      fontSize: width * 3.2,
                      color: HexColor('#050500').withOpacity(0.6),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
