import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/login_page.dart';
import 'package:ayush_buddy/ui/widgets/user_text_field.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig.width * 2,
                horizontal: SizeConfig.width * 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: SizeConfig.width * 7,
                ),
                Padding(
                  padding:  EdgeInsets.symmetric(vertical: SizeConfig.width * 9),
                  child: Center(
                    child: Image.asset(
                      resetPasswordImage,
                      width: SizeConfig.width*50,
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Reset Password',
                    style: TextStyle(
                        fontFamily: poppinsReg,
                        fontSize: SizeConfig.width * 5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 3,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 10),
                  child: Text(
                    'Set the new password for your account so you can log in and access all the features',
                    style: TextStyle(
                        fontFamily: poppinsLight,
                        color: Colors.black.withOpacity(0.6),
                        fontSize: SizeConfig.width * 3.5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 5,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 11),
                  child: Column(
                    children: [
                      UserTextField(
                          color: HexColor('#978E8E'), hint: 'Password'),
                      SizedBox(
                        height: SizeConfig.height * 2.5,
                      ),
                      UserTextField(
                          color: HexColor('#978E8E'),
                          hint: 'Re enter Password'),
                      SizedBox(
                        height: SizeConfig.height * 1.5,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Center(
                  child: GestureDetector(
                    onTap: (){
                      Get.to(()=>LoginPage());
                    },
                    child: Container(
                      height: SizeConfig.height * 5.5,
                      width: SizeConfig.width * 75,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: HexColor('#002449'),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(84, 193, 251, 0.29),
                                offset: Offset(0, 10),
                                blurRadius: 13)
                          ]),
                      child: Text(
                        'Continue',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: poppinsReg,
                            fontSize: SizeConfig.height * 2.1),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
