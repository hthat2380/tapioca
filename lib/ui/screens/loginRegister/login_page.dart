import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/categories/supermarket/view/supermarketCategoryPage.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_controller.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/register_page.dart';
import 'package:ayush_buddy/ui/widgets/register_with_google.dart';
import 'package:ayush_buddy/ui/widgets/user_text_field.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/user_text_field_password.dart';
import '../landing_page/view/landing_page.dart';
import 'forgotPasswordPage.dart';

class LoginPage extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: SizeConfig.height * 100,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: SizeConfig.height * 15),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.width * 3),
                  child: Text('Welcome Back',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: SizeConfig.height * 4.5,
                          fontFamily: robotoBold)),
                ),
                SizedBox(
                  height: SizeConfig.height * 0.5,
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.width * 3),
                  child: Text('Login back into your account',
                      style: TextStyle(
                          color: HexColor('#858484'),
                          fontSize: SizeConfig.height * 1.8,
                          fontFamily: robotoReg)),
                ),
                SizedBox(
                  height: SizeConfig.height * 3,
                ),
                credinentalSection(),
                SizedBox(
                  height: SizeConfig.height * 3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Create account? ',
                      style: TextStyle(
                          color: HexColor('#7A858B'),
                          fontFamily: robotoReg,
                          fontSize: SizeConfig.height * 1.9),
                    ),
                    GestureDetector(
                        onTap: () => Get.to(() => RegisterPage()),
                        child: Text('Sign up',
                            style: TextStyle(
                                color: HexColor('#54C1FB'),
                                fontFamily: robotoReg,
                                fontSize: SizeConfig.height * 1.9))),
                  ],
                ),
                Spacer(),
                Center(
                    child: InkWell(
                        onTap: () {
                          // Get.to(() => SupermarketCategoryPage());
                        },
                        child: Image.asset(continueWithoutImg,
                            height: SizeConfig.height * 3))),
                SizedBox(
                  height: SizeConfig.height * 5,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container credinentalSection() {
    return Container(
      width: SizeConfig.width * 100,
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.width * 8, vertical: SizeConfig.width * 5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.09),
                offset: Offset(0, 3),
                blurRadius: 25)
          ]),
      child: Form(
        key: controller.form,
        child: Column(
          children: [
            UserTextField(
              color: HexColor('#FF9797'),
              hint: 'username',
              textController: controller.username,
            ),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            UserTextFieldPassWord(
              color: HexColor('#869CA8'),
              hint: 'Password',
            ),
            SizedBox(
              height: SizeConfig.height * 1.5,
            ),
            Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () {
                    Get.to(() => ForgotPasswordPage());
                  },
                  child: Text(
                    'Forgot Password?',
                    style: TextStyle(
                        color: HexColor('#54C1FB'), fontFamily: poppinsExtra),
                  ),
                )),
            SizedBox(
              height: SizeConfig.height * 3,
            ),
            GestureDetector(
              onTap: () {
                final valid = controller.form.currentState!.validate();
                if (valid) {
                  Get.to(() => HomeView());
                  controller.loginCheck();
                  return;
                }
                controller.form.currentState!.save();
              },
              child: Container(
                height: SizeConfig.height * 5.5,
                width: SizeConfig.width * 100,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: HexColor('#002449'),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(84, 193, 251, 0.29),
                          offset: Offset(0, 10),
                          blurRadius: 13)
                    ]),
                child: Text(
                  'Sign In',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: poppinsReg,
                      fontSize: SizeConfig.height * 2.1),
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.height * 3,
            ),
            Image.asset(
              orImage,
              width: SizeConfig.width * 100,
            ),
            SizedBox(
              height: SizeConfig.height * 3,
            ),
            RegisterWithGoogle()
          ],
        ),
      ),
    );
  }
}
