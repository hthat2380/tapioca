import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/verifyNumberPage.dart';
import 'package:ayush_buddy/ui/widgets/user_text_field.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig.width * 2,
                horizontal: SizeConfig.width * 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: SizeConfig.width * 7,
                ),
                Padding(
                  padding:  EdgeInsets.symmetric(vertical: SizeConfig.width * 9),
                  child: Center(
                    child: Image.asset(
                      forgotPasswordImage,
                      width: SizeConfig.width*50,
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Forgot password',
                    style: TextStyle(
                        fontFamily: poppinsReg,
                        fontSize: SizeConfig.width * 5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 3,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 10),
                  child: Text(
                    'Entre your mobile/email for the verification process, we will sent 4 digits code to mobile/email.',
                    style: TextStyle(
                        fontFamily: poppinsLight,
                        color: Colors.black.withOpacity(0.6),
                        fontSize: SizeConfig.width * 3.5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Padding(
                  padding: EdgeInsets.only(left: SizeConfig.width * 14),
                  child: Text(
                    'Enter Mobile/Email',
                    style: TextStyle(
                        color: HexColor('#707070'),
                        fontFamily: poppinsLight,
                        fontSize: SizeConfig.width * 3.5),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 11),
                  child: UserTextField(color: HexColor('#869CA8'), hint: ''),
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() => VerifyNumberPage());
                    },
                    child: Container(
                      height: SizeConfig.height * 5.5,
                      width: SizeConfig.width * 75,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: HexColor('#002449'),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(84, 193, 251, 0.29),
                                offset: Offset(0, 10),
                                blurRadius: 13)
                          ]),
                      child: Text(
                        'Continue',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: poppinsReg,
                            fontSize: SizeConfig.height * 2.1),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
