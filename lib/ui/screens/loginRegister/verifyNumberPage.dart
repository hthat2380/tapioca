import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/resetPasswordPage.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class VerifyNumberPage extends StatelessWidget {
  const VerifyNumberPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: SizeConfig.width * 2,
                horizontal: SizeConfig.width * 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: SizeConfig.width * 7,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.width * 9),
                  child: Center(
                    child: Image.asset(
                      verifyNumberImage,
                      width: SizeConfig.width * 50,
                    ),
                  ),
                ),
                Center(
                  child: Text(
                    'Verify Mobile Number',
                    style: TextStyle(
                        fontFamily: poppinsReg, fontSize: SizeConfig.width * 5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 3,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 10),
                  child: Text(
                    'OTP has been sent to you on your mobile Number +91****68 please enter',
                    style: TextStyle(
                        fontFamily: poppinsLight,
                        color: Colors.black.withOpacity(0.6),
                        fontSize: SizeConfig.width * 3.5),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: SizeConfig.width * 5),
                  child: OTPTextField(
                    length: 4,
                    width: SizeConfig.width*100,
                    fieldWidth: SizeConfig.width*12,
                    style: TextStyle(
                        fontSize: 17
                    ),
                    textFieldAlignment: MainAxisAlignment.spaceAround,
                    fieldStyle: FieldStyle.underline,
                    onCompleted: (pin) {
                      print("Completed: " + pin);
                    },
                  ),
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Don't Receive the OTP?",
                      style: TextStyle(
                          color: HexColor('#99999A'),
                          fontFamily: poppinsReg,
                          fontSize: SizeConfig.width * 3.5),
                    ),
                    Text(
                      ' Resend OTP',
                      style: TextStyle(
                          color: HexColor('#6443EB'),
                          fontFamily: poppinsReg,
                          fontSize: SizeConfig.width * 3.5),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.width * 10,
                ),
                Center(
                  child: GestureDetector(
                    onTap: () {
                      Get.to(() => ResetPasswordPage());
                    },
                    child: Container(
                      height: SizeConfig.height * 5.5,
                      width: SizeConfig.width * 75,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: HexColor('#002449'),
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(84, 193, 251, 0.29),
                                offset: Offset(0, 10),
                                blurRadius: 13)
                          ]),
                      child: Text(
                        'Continue',
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: poppinsReg,
                            fontSize: SizeConfig.height * 2.1),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
