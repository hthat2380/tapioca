import 'package:ayush_buddy/resource/font.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_controller.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/login_page.dart';
import 'package:ayush_buddy/ui/widgets/register_with_google.dart';
import 'package:ayush_buddy/ui/widgets/user_text_field.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../landing_page/view/landing_page.dart';

class RegisterPage extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            // height: SizeConfig.height*100,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: SizeConfig.height * 5),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.width * 3),
                    child: Text('Register',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: SizeConfig.height * 4.5,
                            fontFamily: robotoBold)),
                  ),
                  SizedBox(
                    height: SizeConfig.height * 0.5,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: SizeConfig.width * 3),
                    child: Text('Create your new account',
                        style: TextStyle(
                            color: HexColor('#858484'),
                            fontSize: SizeConfig.height * 1.8,
                            fontFamily: robotoReg)),
                  ),
                  SizedBox(
                    height: SizeConfig.height * 2.5,
                  ),
                  credinentalSection(),
                  SizedBox(
                    height: SizeConfig.height * 3.5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account? ? ',
                        style: TextStyle(
                            color: HexColor('#7A858B'),
                            fontFamily: robotoReg,
                            fontSize: SizeConfig.height * 1.9),
                      ),
                      GestureDetector(
                          onTap: () => Get.to(() => LoginPage()),
                          child: Text('Login',
                              style: TextStyle(
                                  color: HexColor('#54C1FB'),
                                  fontFamily: robotoReg,
                                  fontSize: SizeConfig.height * 1.9))),
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.height * 3.5,
                  ),
                  // Spacer(),
                  Center(
                      child: Image.asset(continueWithoutImg,
                          height: SizeConfig.height * 2.5)),
                  SizedBox(
                    height: SizeConfig.height * 5,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Container credinentalSection() {
    return Container(
      width: SizeConfig.width * 100,
      padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.width * 8, vertical: SizeConfig.width * 5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                color: Colors.black.withOpacity(0.09),
                offset: Offset(0, 3),
                blurRadius: 25)
          ]),
      child: Form(
        key: controller.form,
        child: Column(
          children: [
            UserTextField(
              color: HexColor('#869CA8'),
              hint: 'Full name',
              textController: controller.fullName,
            ),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            UserTextField(
                color: HexColor('#869CA8'),
                hint: 'Your email address',
                textController: controller.email),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            UserTextField(
                color: HexColor('#869CA8'),
                hint: 'Mobile Number',
                textController: controller.mobileNo),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            UserTextField(
              color: HexColor('#869CA8'),
              hint: 'password',
              textController: controller.password,
            ),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            UserTextField(
              color: HexColor('#869CA8'),
              hint: 'Confirm password',
              textController: controller.cPassword,
            ),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            SizedBox(
              height: SizeConfig.height * 1,
            ),
            GestureDetector(
              onTap: () {
                final valid = controller.form.currentState!.validate();
                if (valid) {
                  Get.to(() => HomeView());
                  controller.loginCheck();
                  return;
                }
                controller.form.currentState!.save();
              },
              child: Container(
                height: SizeConfig.height * 5.5,
                width: SizeConfig.width * 100,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: HexColor('#002449'),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(84, 193, 251, 0.29),
                          offset: Offset(0, 10),
                          blurRadius: 13)
                    ]),
                child: Text(
                  'Register',
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: poppinsReg,
                      fontSize: SizeConfig.height * 2.1),
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig.height * 2.5,
            ),
            Image.asset(
              orImage,
              width: SizeConfig.width * 100,
            ),
            SizedBox(
              height: SizeConfig.height * 2,
            ),
            RegisterWithGoogle()
          ],
        ),
      ),
    );
  }
}
