import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_controller.dart';
import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_controller.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../shared/provider/local_auth_provider.dart';
import '../../whish_list_screen/controller/whish_controller.dart';

class LoginBinding extends Bindings {
  void dependencies() {
    Get.lazyPut(() => LoginController());
    Get.lazyPut<LocalAuthProvider>(() => LocalAuthProvider(Get.find()));

    Get.lazyPut(() => GetStorage());
  }
}
