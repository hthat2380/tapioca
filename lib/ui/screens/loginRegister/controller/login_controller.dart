import 'package:flutter/cupertino.dart';
import 'package:get/state_manager.dart';

import '../../../../resource/api.dart';
import '../../../../shared/repository/local_auth_repository.dart';

class LoginController extends GetxController {
  // Form
  final GlobalKey<FormState> form = GlobalKey<FormState>();
  final TextEditingController username = TextEditingController();
  final TextEditingController confirmPass = TextEditingController();
  final TextEditingController fullName = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController mobileNo = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController cPassword = TextEditingController();

  void loginCheck() {
    LocalAuthRepository().writeSession(SECURE_STORAGE_WHERE_LOGIN, 'login');
  }
}
