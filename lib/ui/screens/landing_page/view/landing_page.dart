import 'package:ayush_buddy/helper/size_helper.dart';
import 'package:ayush_buddy/resource/shadows.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/top_category.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/amazing_deals.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/app_bar.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/imageSlider.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/navigation_bar.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/navigation_item.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/near_me.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/near_me_foot.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/shop_by_brands.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/super_market.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:get/get.dart';

import 'widget/restaurantCategory.dart';

class HomeView extends StatelessWidget {
  final _homeController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double notification = MediaQuery.of(context).padding.top;

    return GetBuilder<HomePageController>(builder: (controller) {
      return AdvancedDrawer(
        backdropColor: Colors.white.withOpacity(.95),
        controller: _homeController.advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        openRatio: 6 / 10,
        disabledGestures: false,
        childDecoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 2.0,
              spreadRadius: 3,
              offset: Offset(
                10.0, // Move to right 10  horizontally
                4.0, // Move to bottom 10 Vertically
              ),
            ),
          ],
          color: Colors.white10,
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        child: Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white, boxShadow: listBoxShadowLight3),
                child: Column(
                  children: [
                    HomeAppBar(),
                    MainSlider(),
                    TopCategory(),
                    titleSection("Restaurants Near me"),
                    RestCategory(),
                    titleSection("Supermarket Near me"),
                    SuperMarket(),
                    ad1(width),
                    titleSection("Near me"),
                    NearMe(),
                    ad1(width),
                    titleCenter("Shop by Brands"),
                    ShopByBrands(),
                    titleSection2("Near me footwear"),
                    NearMeFoot(),
                    ads2(),
                    titleSection2("Amazing Deals"),
                    AmazindDeals()
                  ],
                ),
              ),
            ),
          ),
          bottomNavigationBar: CustomBottomNavigationBar(
            currentIndex: controller.tabIndex,
            backgroundColor: Colors.white,
            onChange: (index) {
              controller.changeTabIndex(index);
            },
            children: [
              CustomBottomNavigationItem(
                  icon: "assets/images/ic_bottom_home.svg",
                  label: 'Home',
                  color: Colors.grey),
              CustomBottomNavigationItem(
                  icon: "assets/images/ic_bottom_search.svg",
                  label: 'Search',
                  color: Colors.grey),
              CustomBottomNavigationItem(
                  icon: "assets/images/ic_bottom_bag.svg",
                  label: 'Cart',
                  color: Colors.grey,
                  routeName: "cartRoute"),
              CustomBottomNavigationItem(
                  icon: "assets/images/ic_bottom_profile.svg",
                  label: 'Account',
                  color: Colors.grey),
            ],
          ),
        ),
        drawer: SafeArea(
          child: drawerItem(),
        ),
      );
    });
  }

  NavigationItem drawerItem() {
    return NavigationItem();
  }

  Container ad1(double width) {
    return Container(
      child: GestureDetector(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.only(
                left: SizeConfig.size10, right: SizeConfig.size10),
            child: Container(
              width: width,
              height: SizeConfig.blockSizeHorizontal * 34.3,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  image: DecorationImage(
                      image: new ExactAssetImage('assets/images/bg_ad1.png'),
                      fit: BoxFit.fill)),
            ),
          )),
    );
  }

  Container titleCenter(String titleText) {
    return Container(
      margin: EdgeInsets.only(right: 10, left: 10, bottom: 10, top: 10),
      alignment: Alignment.topLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              height: 2,
              width: double.infinity,
              color: Colors.grey[300],
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Text(
            titleText,
            style: TextStyle(
                fontFamily: "PoppinsReg", fontSize: 14, color: Colors.black38),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: Container(
              height: 2,
              width: double.infinity,
              color: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }

  Container titleSection(String titleText) {
    return Container(
      margin: EdgeInsets.only(right: 10, left: 10, bottom: 10, top: 10),
      alignment: Alignment.topLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            titleText,
            style: TextStyle(
                fontFamily: "PoppinsReg", fontSize: 14, color: Colors.black38),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: Container(
              height: 2,
              width: double.infinity,
              color: Colors.grey[300],
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Text(
            "View All",
            style: TextStyle(
                fontFamily: "PoppinsReg",
                fontSize: 14,
                color: Colors.blueAccent),
          ),
        ],
      ),
    );
  }

  Container ads2() {
    return Container(
        child: GestureDetector(
            onTap: () {},
            child: Image(
                fit: BoxFit.contain,
                image: AssetImage("assets/images/bg_ad2.png"))));
  }

  Container titleSection2(String titleText) {
    return Container(
      margin: EdgeInsets.only(right: 10, left: 10, bottom: 10, top: 10),
      alignment: Alignment.topLeft,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            titleText,
            style: TextStyle(
                fontFamily: "PoppinsReg", fontSize: 14, color: Colors.black38),
          ),
          SizedBox(
            width: 2,
          ),
          Expanded(
            child: Container(
              height: 2,
              width: double.infinity,
              color: Colors.grey[300],
            ),
          ),
        ],
      ),
    );
  }
}
