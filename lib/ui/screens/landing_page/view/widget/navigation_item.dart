import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/settings/controller/settings_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../../helper/DalogHeleper.dart';
import '../../../../../helper/curve_painet.dart';
import '../../../../../resource/api.dart';
import '../../../../../resource/validation.dart';

class NavigationItem extends GetView<HomePageController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListTileTheme(
        textColor: Colors.black.withOpacity(.6),
        iconColor: Colors.black.withOpacity(.6),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            ClipPath(
              clipper: BottomClipper(),
              child: Container(
                height: 200,
                width: double.infinity,
                color: appColor,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ClipOval(
                        child: Image.asset(
                          "assets/images/ic_profile_avathar.png",
                          fit: BoxFit.cover,
                          width: 90.0,
                          height: 90.0,
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "Robert R. Walker",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            fontFamily: "PoppinsLight"),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        "rayjo@gmail.com",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 10,
                            fontWeight: FontWeight.w500,
                            fontFamily: "PoppinsLight"),
                      ),
                    ],
                  ),
                ),
              ),
            ),

            // Container(
            //       width: 110.0,
            //       height: 110.0,
            //       margin: const EdgeInsets.only(
            //         top: 20.0,
            //         bottom: 30.0,
            //       ),
            //       clipBehavior: Clip.antiAlias,
            //       decoration: BoxDecoration(
            //         color: Colors.black26,
            //         shape: BoxShape.circle,
            //       ),
            //       child: Image.asset(
            //         'assets/images/flutter_logo.png',
            //         width: 110.0,
            //         height: 110.0,
            //       ),
            //     ),
            ListTile(
              onTap: () {},
              leading: new SvgPicture.asset("assets/images/ic_home.svg"),
              title: Text('Home'),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(wishListRoute);
              },
              leading: new SvgPicture.asset("assets/images/ic_whishlist.svg"),
              title: Text('Wishlist'),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(cartRoute);
              },
              leading: new SvgPicture.asset("assets/images/ic_cart.svg"),
              title: Text('My Cart'),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(myOrdersRoute);
              },
              leading: new SvgPicture.asset("assets/images/ic_my_order.svg"),
              title: Text('My Orders'),
            ),
            ListTile(
              onTap: () {},
              leading: new SvgPicture.asset("assets/images/ic_bell.svg"),
              title: Text('Notifications'),
            ),
            ListTile(
              onTap: () {},
              leading: new SvgPicture.asset("assets/images/ic_refer.svg"),
              title: Text('Refer & Earn'),
            ),
            ListTile(
              onTap: () {},
              leading: new SvgPicture.asset("assets/images/ic_rateus.svg"),
              title: Text('Rate us on playstore'),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(helpAndSupport);
              },
              leading: new SvgPicture.asset("assets/images/ic_help.svg"),
              title: Text('Help & Support'),
            ),
            ListTile(
              onTap: () {
                Get.toNamed(settingsRoute);
              },
              leading: new SvgPicture.asset("assets/images/ic_settings.svg"),
              title: Text('Settings'),
            ),
            ListTile(
              onTap: () {
                var user_type = controller.userType.value.toString();

                print("usertype:" + controller.userType.value.toString());
                // controller.callLogout();
                if (user_type != SECURE_STORAGE_WHERE_LOGIN) {
                  Get.offAllNamed(loginMainRoute);
                } else {
                  var dialog = CustomAlertDialog(
                      title: "Logout",
                      message: logoutMessage,
                      onPostivePressed: () {
                        controller.callLogout();
                      },
                      positiveBtnText: 'Yes',
                      negativeBtnText: 'No');
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => dialog);
                }
              },
              leading: new SvgPicture.asset("assets/images/ic_logout.svg"),
              title: Text('Log out'),
            ),
          ],
        ),
      ),
    );
  }
}
