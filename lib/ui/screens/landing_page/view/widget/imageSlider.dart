import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainSlider extends StatelessWidget {
  int _current = 0;

  final _homeController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.width * .49,
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: CarouselSlider(
              items: imageSliders,
              carouselController: HomePageController.to.sliderController,
              options: CarouselOptions(
                height: 240.0,
                autoPlay: true,
                aspectRatio: 16 / 9,
                autoPlayCurve: Curves.fastOutSlowIn,
                enableInfiniteScroll: true,
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                viewportFraction: 0.8,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children:
                HomePageController.to.imgList.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => HomePageController.to.sliderController
                    .animateToPage(entry.key),
                child: Container(
                  width: 6.0,
                  height: 6.0,
                  margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 4.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: (Theme.of(context).brightness == Brightness.dark
                              ? Colors.white
                              : Colors.black)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                ),
              );
            }).toList(),
          ),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }

  final List<Widget> imageSliders = HomePageController.to.imgList
      .map((item) => Container(
            child: Container(
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(2, 0, 2, 8),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.asset(
                        item,
                        fit: BoxFit.fill,
                        height: 230,
                        width: double.infinity,
                      ),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ))
      .toList();
}
