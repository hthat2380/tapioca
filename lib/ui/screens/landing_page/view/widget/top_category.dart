import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/landing_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TopCategory extends StatelessWidget {
  final _homeController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _homeController.topList.isNotEmpty
            ? Container(
                height: Get.width * .22,
                width: Get.width,
                margin: EdgeInsets.only(top: 10, bottom: 15, left: 0, right: 0),
                child: Center(
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: _homeController.topList.length,
                    itemBuilder: (context, index) {
                      var _data = _homeController.topList[index];
                      return Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (index == 0) {
                                Get.toNamed(storeRoute);
                              } else if (index == 1) {
                                Get.toNamed(restRoute);
                              } else if (index == 2) {
                                Get.toNamed(bakeryRoute);
                              } else if (index == 3) {
                                Get.toNamed(redimadesRoute);
                              } else if (index == 4) {
                                Get.toNamed(footwearRoute);
                              }
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: Get.width * 0.02),
                              child: Align(
                                alignment: Alignment.center,
                                child: Container(
                                  height: Get.width * .16,
                                  width: Get.width * .16,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: AssetImage(_data.image),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            _data.name,
                            style:
                                TextStyle(fontSize: 10, fontFamily: "Poppins"),
                          )
                        ],
                      );
                    },
                  ),
                ))
            : Container(),
      ],
    );
  }
}
