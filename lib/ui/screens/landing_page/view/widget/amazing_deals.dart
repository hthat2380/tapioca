import 'package:ayush_buddy/helper/size_helper.dart';
import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/shadows.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/home_page_controller.dart';

class AmazindDeals extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _homeController = Get.put(HomePageController());
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 10.0,
              right: 10.0,
            ),
            child: Container(
              child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: _homeController.amazingDealslist.length,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 3, vertical: 0),
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 10,
                    childAspectRatio: width / (width * 1.1),
                  ),
                  itemBuilder: (context, index) {
                    var _data = _homeController.amazingDealslist[index];
                    return GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 5),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            boxShadow: listBoxShadowLight2,
                            border: Border.all(color: HexColor("#CBD7E9")),
                            //borderRadius: BorderRadius.circular(10.0),

                            color: Colors.white,
                          ),
                          margin: EdgeInsets.only(left: 5.0, right: 5.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 5,
                              ),
                              Image(
                                image: AssetImage(_data.image),
                                height: 80,
                                width: 80,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Container(
                                      width: 10,
                                      height: 10,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              color: HexColor("#00DDFF")))),
                                  Container(
                                    height: width < 380 ? 18 : 20,
                                    width: width < 380 ? 82 : 82,
                                    child: Center(
                                      child: SizedBox(
                                        width:
                                            SizeConfig.safeBlockHorizontal * 20,
                                        child: Text(
                                          _data.name,
                                          overflow: TextOverflow.ellipsis,
                                          softWrap: false,
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: width < 380 ? 8 : 8,
                                              fontFamily: "Myriad Pro"),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
