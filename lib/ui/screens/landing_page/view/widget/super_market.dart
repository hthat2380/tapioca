import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/landing_page.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/list/sliderMenu1.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SuperMarket extends StatelessWidget {
  final _homeController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _homeController.topList.isNotEmpty
            ? Container(
                height: 160,
                width: Get.width,
                margin: EdgeInsets.only(top: 5, bottom: 0, left: 0, right: 0),
                child: Center(
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                    shrinkWrap: true,
                    itemCount: _homeController.superMarketList.length,
                    itemBuilder: (context, index) {
                      var data = _homeController.superMarketList[index];
                      return SliderCardView(data: data);
                    },
                  ),
                ))
            : Container(),
      ],
    );
  }
}
