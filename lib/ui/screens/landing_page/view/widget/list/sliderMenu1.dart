import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/landing_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SliderCardView extends StatelessWidget {
  var data;

  SliderCardView({
    this.data,
  });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: Get.width * 0.02),
            child: Container(
              height: Get.width * .33,
              width: Get.width * .295,
              decoration: BoxDecoration(
                  color: HexColor(data.bgColor),
                  borderRadius: BorderRadius.circular(6.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.12),
                      blurRadius: 6, // soften the shadow
                      offset: Offset(2.5, 2.5),
                    ),
                  ]),
              child: Column(children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(6.0),
                  child: Image.asset(
                    data.image,
                    height: Get.width * .21,
                    fit: BoxFit.cover,
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        data.name,
                        style: TextStyle(fontSize: 10, fontFamily: "Poppins"),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        data.subTitle,
                        style: TextStyle(fontSize: 8, fontFamily: "Poppins"),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                )
              ]),
            ),
          ),
        ),
      ],
    );
  }
}
