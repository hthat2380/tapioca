import 'package:ayush_buddy/helper/size_helper.dart';
import 'package:ayush_buddy/resource/shadows.dart';
import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ShopByBrands extends StatelessWidget {
  final _homeController = Get.put(HomePageController());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(height: SizeConfig.size10),
          Container(
              width: SizeConfig.safeBlockHorizontal * 100,
              child: Stack(
                children: [
                  CarouselSlider(
                    options: CarouselOptions(
                        height: SizeConfig.safeBlockHorizontal * 45,
                        viewportFraction: 1,
                        initialPage: 0,
                        onPageChanged: (index, reason) {
                          _homeController.onChangeBrandSlider(index);
                          print("brandindex" + index.toString());
                        }),
                    carouselController: _homeController.carouselController,
                    items: _homeController.shopByBarndlist.map((imgurl) {
                      return Builder(builder: (BuildContext context) {
                        return GestureDetector(
                          onTap: () {},
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.asset(
                              imgurl.image,
                              height: Get.width * .1,
                              fit: BoxFit.contain,
                            ),
                          ),
                          // Container(
                          //     child: Image(
                          //   image: CachedNetworkImageProvider(
                          //       imgurl.image ?? ''),
                          //   height: 100,
                          //   width: 200,
                          // )),
                        );
                      });
                    }).toList(),
                  ),
                  Positioned.fill(
                      left: SizeConfig.size18,
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: GestureDetector(
                          onTap: () {
                            _homeController.carouselController.previousPage();
                          },
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: listBoxShadowLight2,
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 6.0),
                                  child: Center(
                                    child: Icon(
                                      Icons.arrow_back_ios,
                                      size: 16,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      )),
                  Positioned.fill(
                    right: SizeConfig.size18,
                    // bottom: 80.0,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () {
                          _homeController.carouselController.nextPage();
                        },
                        child: Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            boxShadow: listBoxShadowLight2,
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 1.0),
                                child: Center(
                                    child: Icon(
                                  Icons.arrow_forward_ios,
                                  size: 16,
                                  color: Colors.grey,
                                )),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )),
          SizedBox(height: SizeConfig.blockSizeHorizontal * 4),
          Container(
            height: SizeConfig.safeBlockHorizontal * 28,
            margin: EdgeInsets.only(left: 0),
            child: ListView.builder(
                itemCount: _homeController.shopByBarndlist.length,
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var brandProduct = _homeController.shopByBarndlist[index];
                  print("three" +
                      _homeController.brandSliderPos.value.toString());
                  return Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: GestureDetector(
                      onTap: () {},
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Container(
                          height: 20,
                          width: 100,
                          child: Column(
                            children: [
                              Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    boxShadow: listBoxShadowLight2),
                                child: Center(
                                    child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset(
                                    brandProduct.image,
                                    height: Get.width * .10,
                                    fit: BoxFit.contain,
                                  ),
                                )),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              SizedBox(
                                width: SizeConfig.safeBlockHorizontal * 30,
                                child: Text(
                                  brandProduct.name,
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: false,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "PoppinsReg",
                                      fontSize: 10,
                                      color: Colors.black),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
