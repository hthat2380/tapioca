import 'package:carousel_slider/carousel_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/extension_instance.dart';
import 'package:get/state_manager.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../model/home_screen_model.dart';
import '../../../../resource/api.dart';
import '../../../../routes/route.dart';
import '../../../../shared/provider/local_auth_provider.dart';
import '../../../../shared/repository/local_auth_repository.dart';

class HomePageController extends GetxController {
  static HomePageController get to => Get.find();
  final CarouselController sliderController = CarouselController();
  final advancedDrawerController = AdvancedDrawerController();
  PageController pageController =
      PageController(viewportFraction: 1, keepPage: true);
  final _storage = GetStorage();

  final userType = "".obs;

  var tabIndex = 0;
  RxInt brandSliderPos = 0.obs;
  CarouselController carouselController = new CarouselController();

  var shopByBrand = <ShopByBrandModel>[].obs;
  var shopByBrandProduct = <ShopByBrandModel>[].obs;

  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }

  @override
  void onInit() {
    pageController = PageController();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (pageController.hasClients) {
        pageController.animateToPage(tabIndex,
            duration: Duration(milliseconds: 1), curve: Curves.easeInOut);
      }
    });
    userTypeData();
    super.onInit();
  }

  void userTypeData() async {
    final box = GetStorage();
    userType.value = box.read(SECURE_STORAGE_WHERE_LOGIN);
    // print("typeTEst" + box.read(SECURE_STORAGE_IS_LOGIN));
  }

  callLogout() async {
    // showLoaderDialog(Get.overlayContext);

    await Future.delayed(const Duration(seconds: 1), () {
      print("logout");
    });
    clearSession();
    // ProfileViewController.to.clearData();
    // signOutWithGoogle();
    // logoutFacebook();
    // await FirebaseMessaging.instance.deleteToken();

    Get.offAllNamed(loginRoute);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  int _current = 0;
  List imgList = [
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png',
    'assets/images/ic_slider_img1.png'
  ];
  List<TopCategory> topList = [
    TopCategory(
        id: "1", name: "Supermarkets", image: 'assets/images/ic_top_cat1.png'),
    TopCategory(
        id: "1", name: "Restaurants", image: 'assets/images/ic_top_cat2.png'),
    TopCategory(
        id: "1", name: "Bakeries", image: 'assets/images/ic_top_cat3.png'),
    TopCategory(
        id: "1", name: "Redimades", image: 'assets/images/ic_top_cat4.png'),
    TopCategory(
        id: "1", name: "Footwear", image: 'assets/images/ic_top_cat5.png'),
  ];

  List<RestCategory> restCategoryList = [
    RestCategory(
        id: "1",
        name: "Mcdonalds",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant.png',
        bgColor: "##FFFFFF"),
    RestCategory(
        id: "1",
        name: "Farasa",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant2.png',
        bgColor: "##FFFFB5"),
    RestCategory(
        id: "1",
        name: "Roasters",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant3.png',
        bgColor: "##ACE9FF"),
    RestCategory(
        id: "1",
        name: "Mcdonalds",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant.png',
        bgColor: "##FFFFFF"),
    RestCategory(
        id: "1",
        name: "Farasa",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant2.png',
        bgColor: "##FFFFB5"),
    RestCategory(
        id: "1",
        name: "Roasters",
        subTitle: "Restaurants",
        image: 'assets/images/ic_restaurant3.png',
        bgColor: "##ACE9FF"),
  ];

  List<RestCategory> superMarketList = [
    RestCategory(
        id: "1",
        name: "PRIME",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket1.png',
        bgColor: "##FFFFFF"),
    RestCategory(
        id: "1",
        name: "LIVE LIFE",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket2.png',
        bgColor: "##FFFFB5"),
    RestCategory(
        id: "1",
        name: "MUSTI JA MASTI",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket3.png',
        bgColor: "##ACE9FF"),
    RestCategory(
        id: "1",
        name: "LIVE LIFE",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket1.png',
        bgColor: "##FFFFFF"),
    RestCategory(
        id: "1",
        name: "Supermarkets",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket2.png',
        bgColor: "##FFFFB5"),
    RestCategory(
        id: "1",
        name: "MUSTI JA MASTI",
        subTitle: "Supermarkets",
        image: 'assets/images/ic_supermarket3.png',
        bgColor: "##ACE9FF"),
  ];

  List<ShopByBrandModel> shopByBarndlist = [
    ShopByBrandModel(
      id: "1",
      name: "Nike",
      image: 'assets/images/ic_brand_nike.png',
    ),
    ShopByBrandModel(
      id: "1",
      name: "Coconut oil",
      image: 'assets/images/ic_brand_sketchers.png',
    ),
    ShopByBrandModel(
      id: "1",
      name: "Coconut oil",
      image: 'assets/images/ic_brand_addidas.png',
    ),
    ShopByBrandModel(
      id: "1",
      name: "Coconut oil",
      image: 'assets/images/ic_brand_wood.png',
    ),
  ];

  List<NearMeModel> nearList = [
    NearMeModel(
        id: "1",
        name: "LIFESTYLE",
        subTitle: "Fashion",
        image: 'assets/images/ic_near_me1.png',
        bgColor: "##FFFFFF"),
    NearMeModel(
        id: "1",
        name: "FAMILY",
        subTitle: "Electronics",
        image: 'assets/images/ic_near_me2.png',
        bgColor: "##FFFFB5"),
    NearMeModel(
        id: "1",
        name: "ZEENATH",
        subTitle: "Furniture",
        image: 'assets/images/ic_near_me3.png',
        bgColor: "##ACE9FF"),
    NearMeModel(
        id: "1",
        name: "LIFESTYLE",
        subTitle: "Fashion",
        image: 'assets/images/ic_near_me1.png',
        bgColor: "##FFFFFF"),
    NearMeModel(
        id: "1",
        name: "FAMILY",
        subTitle: "Wedding Plaza",
        image: 'assets/images/ic_near_me2.png',
        bgColor: "##FFFFB5"),
    NearMeModel(
        id: "1",
        name: "ZEENATH",
        subTitle: "Furniture",
        image: 'assets/images/ic_near_me3.png',
        bgColor: "##ACE9FF"),
  ];

  List<NearMeFootModel> nearMeFootList = [
    NearMeFootModel(
        id: "1",
        name: "Lifestyle Stores",
        subTitle: "Fashion",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##FFFFFF"),
    NearMeFootModel(
        id: "1",
        name: "My G",
        subTitle: "Electronics",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##FFFFB5"),
    NearMeFootModel(
        id: "1",
        name: "Classy Furniture",
        subTitle: "Furniture",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##ACE9FF"),
    NearMeFootModel(
        id: "1",
        name: "Lifestyle Stores",
        subTitle: "Fashion",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##FFFFFF"),
    NearMeFootModel(
        id: "1",
        name: "My G",
        subTitle: "Electronics",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##FFFFB5"),
    NearMeFootModel(
        id: "1",
        name: "Classy Furniture",
        subTitle: "Furniture",
        image: 'assets/images/ic_foot_brand.png',
        bgColor: "##ACE9FF"),
  ];

  List<AmazingDealsModel> amazingDealslist = [
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz1.png',
    ),
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz2.png',
    ),
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz3.png',
    ),
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz1.png',
    ),
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz2.png',
    ),
    AmazingDealsModel(
      id: "1",
      name: "REDEED CODE",
      image: 'assets/images/ic_amazdealz3.png',
    ),
  ];

  void onChangeBrandSlider(int index) {
    brandSliderPos.value = index;
    //List view value updated while moving carosel slider
    // brandProducts.value = shopByBrand.value[index].name!;
  }

  void handleMenuButtonPressed() {
    // NOTICE: Manage Advanced Drawer state through the Controller.
    // _advancedDrawerController.value = AdvancedDrawerValue.visible();
    advancedDrawerController.showDrawer();
  }

  clearSession() {
    _storage.remove(SECURE_STORAGE_USERNAME);
    _storage.remove(SECURE_STORAGE_EMAIL);
    _storage.remove(SECURE_STORAGE_MOBILE);
    _storage.remove(SECURE_STORAGE_PROFILE_URL);
    _storage.remove(SECURE_STORAGE_USER_ID);
    _storage.remove(SECURE_STORAGE_TOKEN);
    _storage.remove(SECURE_STORAGE_WHERE_LOGIN);
    // _storage.remove(SECURE_STORAGE_FCM_ID);
    // _storage.remove(SECURE_STORAGE_IS_LOGIN);
    update();
  }
}
