import 'package:get/get.dart';

import '../../../../model/product_list_model.dart';
import '../../../../resource/images.dart';

class WhishListController extends GetxController
    with SingleGetTickerProviderMixin {
  static WhishListController get to => Get.find();

  var products = <Product>[].obs;

  @override
  void onInit() {
    getCartData();
    super.onInit();
  }

  getCartData() async {
    products.value = [
      Product(
          productId: "2",
          quantity: 1,
          productName: "Green Broccoli",
          brandName: "Vegetable",
          type: "",
          description: "test",
          image: ic_fruits,
          price: "100",
          specialPrice: 120,
          uomValue: "2kg",
          discountAmount: "100",
          discountInPercentage: "4",
          outOfStock: false),
      Product(
          productId: "2",
          quantity: 1,
          productName: "test",
          brandName: "test",
          type: "",
          description: "test",
          image: ic_fruits,
          price: "100",
          specialPrice: 120,
          uomValue: "2kg",
          discountAmount: "100",
          discountInPercentage: "4",
          outOfStock: false),
    ];
  }
}
