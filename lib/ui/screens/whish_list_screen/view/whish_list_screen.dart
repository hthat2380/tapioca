import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/cart/views/widget/cart_item.dart';
import 'package:ayush_buddy/ui/screens/whish_list_screen/view/widget/cart_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../helper/size_helper.dart';
import '../../../../resource/colors.dart';
import '../../../../resource/hex_color.dart';
import '../../../../resource/images.dart';
import '../../../../resource/shadows.dart';
import '../../../widgets/productAppbar.dart';
import '../controller/whish_controller.dart';

class WhishListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    double notification = MediaQuery.of(context).padding.top;
    print(notification);
    return Scaffold(
      appBar: ProductAppbar(title: "Whish List"),
      body: Stack(
        children: [
          SingleChildScrollView(
              child: Column(
            children: [
              WhishListItem(),
              SizedBox(
                height: 40,
              )
            ],
          )),
        ],
      ),
    );
  }
}
