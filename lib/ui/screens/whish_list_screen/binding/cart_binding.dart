import 'package:ayush_buddy/ui/screens/cart/controller/cart_controller.dart';
import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:get/get.dart';

import '../controller/whish_controller.dart';

class WhishListBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => WhishListController());
}
