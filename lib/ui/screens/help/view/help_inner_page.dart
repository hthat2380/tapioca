import 'package:ayush_buddy/ui/screens/help/view/controller/help_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import 'help_Inner_page2.dart';

class HelpInnerPage extends GetView<HelpController> {
  String? title;
  int? helpIndex;
  HelpInnerPage({this.title, this.helpIndex});

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title!),
        ),
        body: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(.3),
                      blurRadius: 2.0, // soften the shadow
                      spreadRadius: .1, //extend the shadow
                      offset: Offset(
                        .5, // Move to right 10  horizontally
                        .5, // Move to bottom 10 Vertically
                      ),
                    ),
                  ]),
              child: Column(
                children: controller.Consultation.map((e) => GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HelpDetails(
                                      title: title,
                                      heading: e['val'],
                                    )));
                      },
                      child: Container(
                        height: 50,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: .05, color: Colors.grey))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Text(
                                e["val"],
                                style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: "PoppinsReg",
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios_outlined,
                              color: Colors.grey,
                              size: 19,
                            ),
                            SizedBox(
                              width: 15,
                            ),
                          ],
                        ),
                      ),
                    )).toList(),
              ),
            ),
          ],
        ));
  }
}
