import 'package:get/get_state_manager/get_state_manager.dart';

class HelpController extends GetxController {
  List help = [
    {
      "val": "Orders",
    },
    {
      "val": "Payment",
    },
    {
      "val": "Return",
    },
    {
      "val": "Referral",
    },
    {
      "val": "Wallet",
    },
    {
      "val": "Business",
    },
    {
      "val": "General Enquiry",
    },
  ];

  List Consultation = [
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
    {
      "val": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit?",
    },
  ];
  List howto = [
    {
      "val": "Step 1",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
    {
      "val": "Step 2",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
    {
      "val": "Step 3",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
    {
      "val": "Step 4",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
    {
      "val": "Step 5",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
    {
      "val": "Step 6",
      "val2":
          "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna 1",
    },
  ];
}
