import 'package:get/instance_manager.dart';

import 'help_controller.dart';

class HelpBinding extends Bindings {
  @override
  void dependencies() => Get.lazyPut(() => HelpController());
}
