import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import 'controller/help_controller.dart';

class HelpDetails extends GetView<HelpController> {
  String? title;
  String? heading;
  int? helpIndex;
  HelpDetails({this.title, this.helpIndex, this.heading});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(title!),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 10, top: 10),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
          ),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        heading!,
                        style:
                            TextStyle(fontSize: 20, fontFamily: "PoppinsReg"),
                      ),
                      Expanded(
                        child: Container(
                          height: .5,
                          color: Colors.grey[400],
                        ),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: controller.howto
                        .map((e) => Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  e['val'],
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontFamily: "PoppinsReg",
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  e['val2'],
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: "PoppinsReg",
                                      color: Colors.grey),
                                ),
                              ],
                            ))
                        .toList(),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      _bottomSheet(context);
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 30),
                      alignment: Alignment.center,
                      height: 50,
                      width: 250,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.grey[400],
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(.3),
                              blurRadius: 2.0, // soften the shadow
                              spreadRadius: .1, //extend the shadow
                              offset: Offset(
                                .5, // Move to right 10  horizontally
                                .5, // Move to bottom 10 Vertically
                              ),
                            ),
                          ]),
                      child: Text(
                        "Still need help",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "PoppinsReg",
                            fontSize: 19),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_bottomSheet(context) {
  showModalBottomSheet(
      backgroundColor: Colors.transparent,
      context: context,
      builder: (context) {
        List contact = [
          {
            "clr": Color.fromRGBO(18, 196, 18, 1),
            "icon": "images/chat.png",
            'label': "Chat"
          },
          {
            "clr": Color.fromRGBO(51, 138, 254, 1),
            "icon": "images/phone.png",
            'label': "Call"
          },
          {
            "clr": Color.fromRGBO(255, 206, 0, 1),
            "icon": "images/mail.png",
            'label': "Mail"
          }
        ];
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          height: 167,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(40),
                topLeft: Radius.circular(40),
              )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: contact
                .map((e) => Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(22),
                          height: 75,
                          width: 75,
                          decoration: BoxDecoration(
                            color: e['clr'],
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset(e['icon']),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          e['label'],
                          style:
                              TextStyle(fontSize: 15, fontFamily: "PoppinsReg"),
                        )
                      ],
                    ))
                .toList(),
          ),
        );
      });
}
