import 'dart:developer';

import 'package:ayush_buddy/resource/api.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get_storage/get_storage.dart';

import '../../../shared/repository/local_auth_repository.dart';

class SplashController extends GetxController
    with SingleGetTickerProviderMixin {
  static SplashController get to => Get.find();
  // var _localAuthRepository = Get.find<LocalAuthRepository>();
  final loginCheck = GetStorage();

  @override
  void onInit() {
    super.onInit();
    // _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    // offsetAnimation = Tween<Offset>(begin: Offset.zero, end: const Offset(0.0, -0.8)).animate(CurvedAnimation(parent: _controller, curve: Curves.decelerate));
    // _controller.forward().then((value) => _controller.reverse());
  }

  @override
  void onReady() {
    super.onReady();
    _launchPage();
  }

  _launchPage() async {
    await Future.delayed(const Duration(seconds: 3), () {
      print("object.............");
    });

    var value = loginCheck.read(SECURE_STORAGE_WHERE_LOGIN);
    if (value != null) {
      Get.offNamed(homeRoute);
    } else {
      Get.offNamed(onBoardingRoute);
    }

    // Get.offNamed(onBoardingRoute);
    // ? _whereLogin
    //     ? homeRoute
    //     : loginRoute
    // : onBoardingRoute);
  }

  @override
  void dispose() {
    super.dispose();
  }
}
