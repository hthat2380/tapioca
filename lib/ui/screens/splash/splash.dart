import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  final _splashController = Get.put(SplashController());

  @override
  build(BuildContext context) => Scaffold(
        body: Container(
          height: SizeConfig.height*100,
          width: SizeConfig.width*100,
          color: HexColor('#002449'),
          alignment: Alignment.center,
          child: Image.asset(
            splashImage,
            width: 100.w,
          ),
        ),
      );
}
