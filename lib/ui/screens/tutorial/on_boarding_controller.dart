import 'package:ayush_buddy/model/onboarding_response.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/login_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../../../routes/route.dart';

class OnBoardingController extends GetxController {
  static OnBoardingController get to => Get.find();
  var selectedPage = 0.obs;

  var pageController = PageController();

  bool get isLastPage =>
      selectedPage.value == onBoardingResponseList.length - 1;

  forwardAction() {
    if (isLastPage) {
      // _localAuthRepository.writeSession(
      //     SECURE_STORAGE_ON_BOARDING, ON_BOARDING);
      Get.toNamed(loginRoute);
      // Get.offAll(()=>LoginPage());
      // Get.offNamedUntil(firstLaunchRoute, (_) => false);
    } else {
      pageController.nextPage(duration: 300.milliseconds, curve: Curves.ease);
    }
  }

  List<OnBoardingResponse> onBoardingResponseList = [
    OnBoardingResponse(
        imageAssets: 'assets/images/bg_onboard1.png',
        title: 'TAPIOCA 24/7',
        description:
            'Have a virtual doctor consultation at your convenience using your smart phone, tablet or computer.'),
    OnBoardingResponse(
        imageAssets: 'assets/images/bg_onboard2.png',
        title: 'Order Online Best store around you',
        description:
            'Have a virtual doctor consultation at yourconvenience using your smart phone, tablet or computer.'),
    OnBoardingResponse(
        imageAssets: 'assets/images/bg_onboard3.png',
        title: 'Better for you',
        description:
            'Quality health care comes only from qualified doctors Find experienced doctors across all specialties')
  ];
}
