import 'package:ayush_buddy/resource/colors.dart';
import 'package:ayush_buddy/resource/images.dart';
import 'package:ayush_buddy/resource/shadows.dart';
import 'package:ayush_buddy/resource/style.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import 'on_boarding_controller.dart';

class OnBoardingPage extends StatelessWidget {
  Widget build(BuildContext context) => Scaffold(
          body: Stack(children: [
        PageView.builder(
            onPageChanged: OnBoardingController.to.selectedPage,
            controller: OnBoardingController.to.pageController,
            itemCount: OnBoardingController.to.onBoardingResponseList.length,
            physics: BouncingScrollPhysics(),
            itemBuilder: (context, index) {
              return Column(
                children: [
                  SizedBox(height: SizeConfig.width * 30),
                  Image.asset(
                    OnBoardingController
                        .to.onBoardingResponseList[index].imageAssets!,
                    fit: BoxFit.fitWidth,
                  ),
                  // Container(
                  //   width: SizeConfig.width*100,
                  //   height: SizeConfig.width*93,
                  //   decoration: new BoxDecoration(
                  //     color: Colors.yellow,
                  //     image: new DecorationImage(
                  //       image: new ExactAssetImage(OnBoardingController
                  //           .to.onBoardingResponseList[index].imageAssets),
                  //       fit: BoxFit.fitWidth,
                  //     ),
                  //   ),
                  // ),
                  SizedBox(height: SizeConfig.width * 5),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: SizeConfig.width * 24),
                    child: Text(
                        OnBoardingController
                            .to.onBoardingResponseList[index].title!,
                        textAlign: TextAlign.center,
                        style: onBoardingTitleStyle),
                  ),
                  SizedBox(height: SizeConfig.width * 3),
                  Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: SizeConfig.width * 18),
                    child: Text(
                        OnBoardingController
                            .to.onBoardingResponseList[index].description!,
                        textAlign: TextAlign.center,
                        style: onBoardingMessageStyle),
                  ),
                ],
              );
            }),
        Positioned.fill(
            bottom: SizeConfig.width * 15,
            // left: 150,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                        OnBoardingController.to.onBoardingResponseList.length,
                        (index) => Obx(() {
                              return Container(
                                margin: EdgeInsets.all(SizeConfig.width * 1.7),
                                width: SizeConfig.width * 3.1,
                                height: SizeConfig.width * 3.1,
                                decoration: BoxDecoration(
                                  color: OnBoardingController
                                              .to.selectedPage.value ==
                                          index
                                      ? onBoardIndicatorColor
                                      : Colors.white,
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 1.0,
                                    // assign the color to the border color
                                    color: onBoardIndicatorColor,
                                  ),
                                ),
                              );
                            }))),
              ),
            )),
        Positioned(
            right: SizeConfig.width * 10,
            bottom: SizeConfig.width * 13,
            child: GestureDetector(
              onTap: () {
                OnBoardingController.to.forwardAction();
              },
              child: Container(
                height: SizeConfig.width * 11,
                width: SizeConfig.width * 11,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                    boxShadow: [onBoardButtonShadow]
                    // borderRadius: BorderRadius.circular(4),
                    ),
                child: Image.asset(
                  onBoardArrowImg,
                  height: SizeConfig.width * 2.5,
                ),
              ),
            ))
      ]));
}
