import 'package:ayush_buddy/ui/screens/cart/controller/cart_controller.dart';
import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:get/get.dart';

import '../ui/screens/settings/view/settings.dart';

class CartBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => CartController());
}
