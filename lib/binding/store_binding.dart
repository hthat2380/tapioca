import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:ayush_buddy/ui/screens/store_screen.dart/controller/store_controller.dart';
import 'package:ayush_buddy/ui/screens/store_screen.dart/view/store_home_screen.dart';
import 'package:get/get.dart';

import '../ui/screens/settings/view/settings.dart';

class StoreBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => StoreController());
}
