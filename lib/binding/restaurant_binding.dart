import 'package:ayush_buddy/ui/screens/categories/restaurants/controller/rest_cat_controller.dart';
import 'package:get/get.dart';

class RestaurantBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => RestCatController());
}