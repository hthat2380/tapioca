import 'package:ayush_buddy/ui/screens/categories/redimades/controller/redimades_controller.dart';
import 'package:get/get.dart';

class ReadyMadeBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => ReadyMadeController());
}