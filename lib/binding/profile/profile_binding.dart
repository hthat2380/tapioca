import 'package:ayush_buddy/ui/screens/cart/controller/cart_controller.dart';
import 'package:ayush_buddy/ui/screens/profile/controller/cart_binding.dart';
import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:get/get.dart';

class ProfileBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => ProfileController());
}
