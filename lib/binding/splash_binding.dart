import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:get/get.dart';

class SplashBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => SplashController());
}
