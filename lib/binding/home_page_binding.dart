import 'package:ayush_buddy/ui/screens/landing_page/controller/home_page_controller.dart';
import 'package:ayush_buddy/ui/screens/tutorial/on_boarding_controller.dart';
import 'package:get/get.dart';

class HomePageBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => HomePageController());
}
