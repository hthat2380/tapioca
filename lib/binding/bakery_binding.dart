import 'package:ayush_buddy/ui/screens/categories/Bakeries/controller/bakaries_controller.dart';
import 'package:get/get.dart';

class BakeryBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => BakeriesController());
}