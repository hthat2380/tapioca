import 'package:ayush_buddy/ui/screens/tutorial/on_boarding_controller.dart';
import 'package:get/get.dart';

class OnBoardingBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => OnBoardingController());
}
