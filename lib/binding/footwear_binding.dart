import 'package:ayush_buddy/ui/screens/categories/footwear/controller/footwear_controller.dart';
import 'package:get/get.dart';

class FootwearBinding extends Bindings {
  void dependencies() => Get.lazyPut(() => FootwearController());
}