import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_controller.dart';
import 'package:ayush_buddy/ui/screens/settings/controller/settings_controller.dart';
import 'package:ayush_buddy/ui/screens/splash/splash_controller.dart';
import 'package:get/get.dart';

import '../ui/screens/settings/view/settings.dart';

class SettingsBinding extends Bindings {
  void dependencies() {
    Get.lazyPut(() => SettingsController());
    Get.lazyPut(() => LoginController());
  }
}
