import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'hex_color.dart';

const primarySwatchColor = Colors.deepOrange;
const accentColor = Colors.deepOrange;
const Color notificationBackgroundColor = Color(0xFFFF8A65);
Color primaryColor = HexColor('#FFFFFF');
Color onBoardIndicatorColor = HexColor('#0C2239');
Color onBoardTitleColor = HexColor('#146131');
Color appColor = HexColor('#001C39');
Color textGrey = HexColor('#858484');
Color profileTitleColor = HexColor('#F14D4D').withOpacity(.5);
Color profileTitleGreen = HexColor('#4CB050');
Color profileGenderText = HexColor('#7C8494');
Color profileGenderBgColor = HexColor('#EFF2F3');
Color appBarBackArrowColor = HexColor('#037FF2');
Color imageBakgroundGrey = HexColor('#D8D8D8');
