const String appName = 'Food Cafe';

const labelSignUp = "Don\u0027t have an account?";
const labelSignUpContent =
    'Please enter below details to register your account on $appName.';
const labelForgotPassword =
    'Forgot password success sent to email, waiting for admin approval.';
const labelStart = 'START';
const labelNext = 'NEXT';
