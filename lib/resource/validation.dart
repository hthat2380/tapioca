import 'package:get/get.dart';

const String appName = 'Food Cafe';


const linkSignUp = 'Sign Up';
const linkForgotPassword = 'Forgot password';

//HINTS
const hintPassword = 'Password';
const hintConfirmPassword = 'Confirm password';
const hintNewPassword = 'New password';
const hintNewConfirmPassword = 'New confirm password';
const hintCurrentPassword = 'Current password';
const hintEmail = 'Email';
const hintUserName = 'Name';
const hintMobile = 'Mobile';
const hintPinCode = 'Pin code';
const hintAddress = 'Address';
const hintMessage = 'Message';

//DATA NOT FOUND MESSAGE
const dataNotMessage = 'Data not available';
const dataNotPendingMessage = 'No pending list found!';
const dataNotAcceptedMessage = 'No accepted list found!';
const dataNotReadyMessage = 'No ready list found!';
const dataNotDispatchedMessage = 'No dispatched list found!';
const dataNotNewOrderMessage = 'No new order list found!';
const dataNotNotificationMessage = 'Notification not found!';
const dataNotKDSViewMessage = 'No KDS view list found!';
const dataNotAllMessage = 'No all find list found!';
const dataNotTurnOfOrdering = 'Order not off';
const dataNotPauseMenu = 'No pause menu list found!';
const dataNotBirthday = 'No birthday list found!';


//NETWORK ERROR MESSAGE
const networkErrorMessage = 'Failed to get network connection';
const networkErrorTitleMessage = 'Network error';

//LOGOUT MESSAGE
const logoutMessage = 'Are you sure want to exit?';

const netWorkConnectionWIFI = 1;
const netWorkConnectionMobile = 2;
const netWorkConnectionError = 0;

const mobileMaxLength = 10;
const pinCodeMaxLength = 6;
const passwordMaxLength = 6;
const messageMaxLength = 250;