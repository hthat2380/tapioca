const String robotoMedium = 'RobotoMedium';
const String robotoBold = 'RobotoBold';
const String robotoReg = 'RobotoReg';

const String poppinsExtra = 'PoppinsExtra';
const String poppinsReg = 'PopinsReg';
const String poppinsMed = 'PopinsMedium';
const String poppinsLight = 'PopinsLight';
const String poppinsSemiBold = 'PopinsSemiBold';



const String mediumFont = 'Baloo Tamma 2 Medium';
const String semiBoldFont = 'Baloo Tamma 2 SemiBold';
const String regularFont = 'Baloo Tamma 2 Regular';
