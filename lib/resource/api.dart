//USE FOR API URL
const BASE_URL = '';
const LOGIN_URL = '$BASE_URL//';
const USER_SIGNUP_URL = '$BASE_URL//';
const USER_OTP_URL = '$BASE_URL//';
const FORGOT_PASSWORD_URL = '$BASE_URL//';
const CHANGE_PASSWORD_URL = '$BASE_URL//';


//USE FOR FIREBASE TABLE NAME
const tableNameNotificationCounter = 'notificationcounter';

//USE FOR LOCAL STORAGE
const SECURE_STORAGE_USERNAME = "username";
const SECURE_STORAGE_EMAIL = "email";
const SECURE_STORAGE_MOBILE = "mobile";
const SECURE_STORAGE_PROFILE_URL = "profileURL";
const SECURE_STORAGE_USER_ID = "userId";
const SECURE_STORAGE_TOKEN = "token";
const SECURE_STORAGE_PINCODE = "pinCode";
const SECURE_STORAGE_ADDRESS = "address";
const SECURE_STORAGE_WHERE_LOGIN = "login";
const SECURE_STORAGE_ON_BOARDING = "onBoarding";
const SECURE_STORAGE_FIREBASE_TOKEN = "firebaseToken";

//USE FOR WHICH TYPE OF LOGIN
const WHERE_LOGIN = "login";
const WHERE_FACEBOOK_LOGIN = "facebook";
const WHERE_GOOGLE_LOGIN = "google";

const ON_BOARDING = "true";

//USE FOR ORDER STATUS
const whereToReachOrderPending = 1;
const whereToReachOrderAccepted = 2;
const whereToReachOrderReady = 3;
const whereToReachOrderDispatched = 4;


