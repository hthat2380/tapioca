import 'package:ayush_buddy/resource/hex_color.dart';
import 'package:flutter/material.dart';

BoxShadow onBoardButtonShadow = BoxShadow(
    color: Color.fromRGBO(0, 0, 0, 0.16), offset: Offset(0, 3), blurRadius: 6);

var listBoxShadowLight2 = [
  BoxShadow(
    color: HexColor("#EEEEEE"),
    blurRadius: 2.2, // soften the shadow
    spreadRadius: 1, //extend the shadow
    offset: Offset(
      2.0, // Move to right 10  horizontally
      2.0, // Move to bottom 10 Vertically
    ),
  ),
];
var listBoxShadowLight3 = [
  BoxShadow(
    color: Colors.grey,
    blurRadius: 2.2, // soften the shadow
    spreadRadius: 1, //extend the shadow
    offset: Offset(
      4.0, // Move to right 10  horizontally
      4.0, // Move to bottom 10 Vertically
    ),
  ),
];
var listBoxShadowLight = [
  BoxShadow(
    color: HexColor('#E0E0E0'),
    blurRadius: 2.0, // soften the shadow
    spreadRadius: 1, //extend the shadow
    offset: Offset(
      2.0, // Move to right 10  horizontally
      2.0, // Move to bottom 10 Vertically
    ),
  ),
];
var buttonCartShadowLight = [
  BoxShadow(
    color: Color.fromRGBO(0, 0, 0, .2),
    blurRadius: 3,
    offset: Offset(
      1.50,
      1.50,
    ),
  ),
];

var cartShadowLight = [
  BoxShadow(
    color: Color.fromRGBO(0, 0, 0, .2),
    blurRadius: 5,
    spreadRadius: 2,
    offset: Offset(
      3,
      3,
    ),
  ),
];
var listBoxShadow = [
  BoxShadow(
    color: (Colors.grey[350])!,
    blurRadius: 3.0, // soften the shadow
    spreadRadius: 1, //extend the shadow
    offset: Offset(
      0.0, // Move to right 10  horizontally
      3.0, // Move to bottom 10 Vertically
    ),
  ),
];
