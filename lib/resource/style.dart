import 'package:ayush_buddy/utills/screenUtil.dart';
import 'package:ayush_buddy/utills/sizeConfig.dart';
import 'package:flutter/material.dart';
import 'colors.dart';
import 'font.dart';

// //TITLE STYLE
// var splashTitleStyle =
//     TextStyle(fontSize: 70.sp, color: Colors.black, fontFamily: mediumFont);
// var appBarTitleStyle =
//     TextStyle(fontSize: 36.sp, color: Colors.black, fontFamily: mediumFont);
// var appBarSubTitleStyle =
//     TextStyle(fontSize: 30.sp, color: Colors.grey, fontFamily: mediumFont);
// var appBarAddressStyle =
//     TextStyle(fontSize: 24.sp, color: Colors.black, fontFamily: mediumFont);
// var descriptionStyle =
//     TextStyle(fontSize: 32.sp, color: Colors.black, fontFamily: regularFont);

// //NOT FOUND
// var dataNotAvailableStyle =
//     TextStyle(fontSize: 18.h, color: Colors.black, fontFamily: semiBoldFont);

// var notificationTitleStyle =
//     TextStyle(fontSize: 40.sp, color: Colors.white, fontFamily: mediumFont);
// var totalNotificationStyle =
//     TextStyle(fontSize: 50.sp, color: Colors.white, fontFamily: mediumFont);
// var getNotificationTitleStyle =
//     TextStyle(fontSize: 36.sp, color: Colors.black, fontFamily: mediumFont);
// var getNotificationDescriptionStyle =
//     TextStyle(fontSize: 30.sp, color: Colors.black, fontFamily: regularFont);

// //BUTTON STYLE
// var btnOkStyle =
//     TextStyle(fontSize: 30.sp, color: Colors.black, fontFamily: mediumFont);
// var btnCloseStyle =
//     TextStyle(fontSize: 30.sp, color: Colors.red, fontFamily: mediumFont);
// var btnFindStyle =
//     TextStyle(fontSize: 25.sp, color: Colors.black, fontFamily: mediumFont);
// var btnOnBoardingStyle =
//     TextStyle(fontSize: 30.sp, color: accentColor, fontFamily: mediumFont);

// //TOAST STYLE
// flushBarMessageStyle(Color color) =>
//     TextStyle(fontSize: 24.sp, color: color, fontFamily: mediumFont);
// flushBarTitleStyle(Color color) =>
//     TextStyle(fontSize: 28.sp, color: color, fontFamily: semiBoldFont);

//ON BOARDING STYLE
var onBoardingTitleStyle = TextStyle(
    fontSize: SizeConfig.width*5.2,
    color: Colors.black,
    fontFamily: poppinsMed,
);
var onBoardingMessageStyle =
TextStyle(fontSize: SizeConfig.width*3.3,
    color: Colors.black.withOpacity(0.66),letterSpacing: 0.25,
    fontFamily: poppinsReg);

// //LINK STYLE
// doNotHaveAccountStyle(Color color) =>
//     TextStyle(fontSize: 30.sp, color: color, fontFamily: regularFont);
// loginLinkStyle(Color color) =>
//     TextStyle(fontSize: 30.sp, fontFamily: mediumFont, color: color);

//TAB STYLE
var tabSelectStyle = TextStyle(fontSize: 14, fontFamily: mediumFont);
var tabUnSelectStyle = TextStyle(fontSize: 12, fontFamily: mediumFont);

// //DRAWER STYLE
// var drawerMenuStyle =
//     TextStyle(fontSize: 30.sp, color: Colors.black, fontFamily: regularFont);
// var drawerUserNameStyle =
//     TextStyle(fontSize: 32.sp, color: Colors.black, fontFamily: semiBoldFont);
// var drawerEmailStyle =
//     TextStyle(fontSize: 24.sp, color: Colors.black, fontFamily: semiBoldFont);
