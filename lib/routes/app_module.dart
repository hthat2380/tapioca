import 'package:ayush_buddy/binding/bakery_binding.dart';
import 'package:ayush_buddy/binding/cart_binding.dart';
import 'package:ayush_buddy/binding/footwear_binding.dart';
import 'package:ayush_buddy/binding/on_boarding_binding.dart';
import 'package:ayush_buddy/binding/readymades_binding.dart';
import 'package:ayush_buddy/binding/restaurant_binding.dart';
import 'package:ayush_buddy/binding/settings_binding.dart';
import 'package:ayush_buddy/binding/splash_binding.dart';
import 'package:ayush_buddy/binding/store_binding.dart';
import 'package:ayush_buddy/routes/route.dart';
import 'package:ayush_buddy/ui/screens/cart/controller/cart_controller.dart';
import 'package:ayush_buddy/ui/screens/categories/Bakeries/controller/bakaries_controller.dart';
import 'package:ayush_buddy/ui/screens/categories/Bakeries/view/bakery_categoty.dart';
import 'package:ayush_buddy/ui/screens/categories/footwear/view/footwear_category.dart';
import 'package:ayush_buddy/ui/screens/categories/redimades/view/redimade_category.dart';
import 'package:ayush_buddy/ui/screens/categories/restaurants/view/restaurant_category.dart';
import 'package:ayush_buddy/ui/screens/help/view/help_page.dart';
import 'package:ayush_buddy/ui/screens/landing_page/view/widget/restaurantCategory.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/controller/login_binding.dart';
import 'package:ayush_buddy/ui/screens/loginRegister/login_page.dart';
import 'package:ayush_buddy/ui/screens/my_order_details/controller/my_orders_details_binding.dart';
import 'package:ayush_buddy/ui/screens/payment/view/payment_screen.dart';
import 'package:ayush_buddy/ui/screens/settings/add_address/add_address.dart';
import 'package:ayush_buddy/ui/screens/splash/splash.dart';
import 'package:ayush_buddy/ui/screens/store_screen.dart/view/store_home_screen.dart';
import 'package:ayush_buddy/ui/screens/tutorial/onBoarding_page.dart';
import 'package:ayush_buddy/ui/screens/whish_list_screen/binding/cart_binding.dart';
import 'package:ayush_buddy/ui/screens/whish_list_screen/view/whish_list_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

import '../binding/home_page_binding.dart';
import '../ui/screens/cart/views/cart_screen.dart';
import '../ui/screens/help/view/controller/help_binding.dart';
import '../ui/screens/landing_page/view/landing_page.dart';
import '../ui/screens/my_order_details/view/my_order_detail_page.dart';
import '../ui/screens/my_orders/controller/my_orders_binding.dart';
import '../ui/screens/my_orders/view/profile_screen.dart';
import '../ui/screens/profile/view/profile_screen.dart';
import '../ui/screens/settings/add_address/select_address.dart';
import '../ui/screens/settings/view/settings.dart';

abstract class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: firstLaunchRoute,
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: onBoardingRoute,
        page: () => OnBoardingPage(),
        binding: OnBoardingBinding()),

    GetPage(
        name: homeRoute, page: () => HomeView(), binding: HomePageBinding()),

    GetPage(
        name: settingsRoute,
        page: () => SettingsPage(),
        binding: SettingsBinding()),
    GetPage(
      name: SelectAddressRoute,
      page: () => SelectAddress(),
    ),

    GetPage(name: cartRoute, page: () => CartScreen(), binding: CartBinding()),
    GetPage(
        name: storeRoute,
        page: () => StoreHomeScreen(),
        binding: StoreBinding()),
    GetPage(
        name: restRoute,
        page: () => RestaurantCategory(),
        binding: RestaurantBinding()),
    GetPage(
        name: bakeryRoute,
        page: () => BakeryCategory(),
        binding: BakeryBinding()),
    GetPage(
        name: footwearRoute,
        page: () => FootwearCategory(),
        binding: FootwearBinding()),
    GetPage(
        name: redimadesRoute,
        page: () => ReadyMadeCategory(),
        binding: ReadyMadeBinding()),
    GetPage(
        name: profileRoute,
        page: () => ProfileScreen(),
        binding: StoreBinding()),
    GetPage(name: payment, page: () => PaymentMethod()),
    GetPage(
        name: wishListRoute,
        page: () => WhishListScreen(),
        binding: WhishListBinding()),
    GetPage(
        name: myOrdersRoute,
        page: () => MyOrders(),
        binding: MyOrdersBinding()),
    GetPage(
        name: myOrdersDetails,
        page: () => OrderDetails(),
        binding: MyOrdersDetailsBinding()),
    GetPage(name: loginRoute, page: () => LoginPage(), binding: LoginBinding()),
    GetPage(
        name: helpAndSupport, page: () => HelpPage(), binding: HelpBinding()),

    // GetPage(
    //     name: profileEdit, page: () => ProfileEdit(), binding: ProfileEditB()),
    // GetPage(
    // name: addFamilyMemberRoute,
    // page: () => MemberProfile(),
    // binding: AddFamilyMemberProfileBinding()),

    // GetPage(
    //     name: ReferAndEarnRoute,
    //     page: () => ReferAndEarn(),
    //     binding: MedCoinControllerBinding()),

    // GetPage(
    //     name: cartRoute, page: () => MediCart(), binding: SettingsBinding()),
    // GetPage(
    //     name: selectDelTimeRoute,
    //     page: () => SelectDelTime(),
    //     binding: SelectDeliveryControllerBinding()),
    // GetPage(
    //     name: orderReviewRoute,
    //     page: () => OrderReviewPage(),
    //     binding: OrderReviewBinding()),
    // GetPage(
    //     name: AddAddressRoute,
    //     page: () => AddAddress2(),
    //     binding: AddAddressBinding()),
    // GetPage(
    //     name: SelectAddressRoute,
    //     page: () => SelectAddressPage(),
    //     binding: SelectAddressControllerBinding()),
    // GetPage(
    //     name: mapPageRoute,
    //     page: () => MapPage(),
    //     binding: MapBindingControllerBinding()),
    // GetPage(
    //     name: mapPageRoute,
    //     page: () => MapPage(),
    //     binding: MapBindingControllerBinding()),
    // GetPage(
    //   name: VouchersRoute,
    //   page: () => Vouchers(),
    //   //TODO
    // ),
    // GetPage(
    //     name: myOrdersRoute, page: () => MyOrders(), binding: MyOrderBinding()),
    // GetPage(
    //     name: myOrderDetailRoute,
    //     page: () => OrderDetails(),
    //     binding: MyOrderBinding()),
    // GetPage(
    //   name: RecentPurchaseRoute,
    //   page: () => RecentPurchase(),
    //   //TODO
    // ),
    // GetPage(
    //   name: SubscriptionRoute,
    //   page: () => Subscription(),
    //     binding: MedFillSubscriptionBinding()

    // ),
    // GetPage(
    //     name: SuperGoldSaving2Route,
    //     page: () => SuperGoldSaving2(),
    //     binding: MedPrideMemberShipBinding()),
    // GetPage(
    //   name: MemberShipPageRoute,
    //   page: () => MemberShipPage(),
    //   //TODO
    // ),
    // GetPage(
    //     name: ReferPageRoute,
    //     page: () => ReferPage(),
    //     binding: MedCoinControllerBinding()),
    // GetPage(
    //   name: HelpPageRoute,
    //   page: () => HelpPage(),
    //   //TODO
    // ),
    // GetPage(
    //   name: AboutUsRoute,
    //   page: () => AboutUs(),
    //   //TODO
    // ),
    // GetPage(
    //   name: LegalPageRoute,
    //   page: () => LegalPage(),
    //   //TODO
    // ),
    // GetPage(
    //     name: productDetailsRoute,
    //     page: () => LegalPage(),
    //     binding: ProductDetailsBinding()
    //     //TODO
    //     ),
    // GetPage(
    //     name: prideMemberShip,
    //     page: () => MemberShipPage(),
    //     binding: MemberShipBinding()),
    // GetPage(
    //     name: subReviewRoute,
    //     page: () => SubScriptionReview(),
    //     binding: SubReviewBinding()),
    // GetPage(
    //     name: OrderMedicineRoute,
    //     page: () => OrderMedicinePage(),
    //     binding: OrderMedicineBinding()),
  ];
}
