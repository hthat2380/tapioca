const String firstLaunchRoute = '/';
const String languageRoute = '/language';
const String onBoardingRoute = '/onBoarding';
const String loginMainRoute = '/loginMAin';
const String loginRoute = '/login';
const String loginSignup = '/signup';
const String forgotPasswordRoute = '/forgotPassword';
const String signUpRoute = '/signUp';
const String homeRoute = '/home';
const String settingsRoute = '/settings';
const String cartRoute = '/cart';
const String storeRoute = '/store';
const String restRoute = '/restaurant';
const String profileRoute = '/profile';
const String payment = '/payment';
const String wishListRoute = '/whishListRoute';
const String myOrdersRoute = '/myOrdersRoute';
const String myOrdersDetails = '/myOrdersDetailsRoute';
const String helpAndSupport = '/helpandsupport';
const String bakeryRoute = '/bakery';
const String redimadesRoute = '/redimades';
const String footwearRoute = '/footwear';

// FolioFit
const String healthReminderRoute = '/healthReminder';

const String yogaWhishListView = '/yogaWhishListView';

//Profile
const String ProfileViewRoute = '/ProfileViewRoute';
const String profileEdit = '/ProfileEdit';
const String addFamilyMemberRoute = '/addFamilyMemberRoute';
const String ReasonPageRoute = '/ReasonPageRoute';
const String HealthDataRoute = '/HealthDataRoute';
const String UploadHealthDataRoute = '/UploadHealthDataRoute';
const String ReferAndEarnRoute = '/ReferAndEarnRoute';
const String settingRoute = '/settingRoute';

const String addAddressRoute = '/AddAddressRoute';
const String SelectAddressRoute = '/SelectAddressRoute';
const String VouchersRoute = '/VouchersRoute';
const String myOrderDetailRoute = '/MyOrderDetailasRoute';
const String RecentPurchaseRoute = '/RecentPurchaseRoute';
const String SubscriptionRoute = '/SubscriptionRoute';
const String SuperGoldSaving2Route = '/SuperGoldSaving2Route';
const String MemberShipPageRoute = '/MemberShipPageRoute';
const String ReferPageRoute = '/ReferPageRoute';
const String HelpPageRoute = '/HelpPageRoute';
const String AboutUsRoute = '/AboutUsRoute';
const String LegalPageRoute = '/LegalPageRoute';
const String mapPageRoute = '/mapPageRoute';
const String productDetailsRoute = '/productDetails';
const String faqReferAndEranRoute = '/faqReferAndEranRoute';
