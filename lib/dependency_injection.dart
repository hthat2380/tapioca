import 'package:ayush_buddy/shared/provider/local_auth_provider.dart';
import 'package:ayush_buddy/shared/repository/local_auth_repository.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DependencyInjection {
  static void init() {
    //SESSION BINDING
    Get.lazyPut<GetStorage>(() => GetStorage(), fenix: true);

    Get.lazyPut<LocalAuthProvider>(() => LocalAuthProvider(Get.find()),
        fenix: true);
    Get.lazyPut<LocalAuthRepository>(() => LocalAuthRepository(), fenix: true);
  }
}
